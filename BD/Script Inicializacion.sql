--use solds321_88dsrhplus
use [RH Plus]

insert into Roles(IdRol,Nombre)
values(1,'Super Administrador'),
	  (2,'Administrador'),
	  (3,'Editor'),
	  (4,'Lector');

insert into GrupoEmpresarial(Nombre,Descripcion,Activo)
values ('Grupo Empresarial','Grupo',1);

insert into Organizacion(NombreEmpresa,GrupoEmpresarial,Activa,CantidadUsuario,Logo,Observaciones)
values('Soluciones DS',1,1,5,null,null);

insert into Secciones(Nombre,Activo,IdOrganizacion)
values ('RH',1,1);

insert into Usuarios(IdUsuario,Nombre,Apellido1,Apellido2,Clave,Correo,Estado,IdRol,IdSeccion,IdOrganizacion)
values('admin','Admin','Admin','Admin','4QrcOUm6Wau+VuBX8g+IPg==','soporte@soporte.com',1,1,1,1);
--la clave esta en el formato encriptado. para ingresar es 123456

insert into Estados (IdEstado,Nombre)
values(0,'Inicial'),(1,'Aprobado'),(2,'Obsoleto');