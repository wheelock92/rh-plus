USE [RH Plus]
GO
/****** Object:  StoredProcedure [dbo].[Archivo_Nuevo]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Archivo_Nuevo]
@Archivo image,
@Nombre varchar(50),
@Extension varchar(50),
@IdArchivo int OUTPUT

as
begin

insert into Archivo
values(@Archivo,@Nombre,@Extension);

select @IdArchivo=SCOPE_IDENTITY();

end


GO
/****** Object:  StoredProcedure [dbo].[Cambio_Clave]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Cambio_Clave]
@IdUsuario int,
@Clave varchar(50)

as
begin
update Usuarios
set Clave=@Clave
where Id=@IdUsuario

end
GO
/****** Object:  StoredProcedure [dbo].[Categoria_Consulta]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Categoria_Consulta]
@IdOrganizacion int
as
begin
select distinct C.IdCategoria,C.Nombre,O.IdOrganizacion,O.NombreEmpresa,C.Activo
from Categoria C INNER JOIN Organizacion O on C.IdOrganizacion = O.IdOrganizacion
				 INNER JOIN Organizacion on o.IdOrganizacion=@IdOrganizacion

end
GO
/****** Object:  StoredProcedure [dbo].[Categoria_Editar]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[Categoria_Editar]
@IdCategoria int,
@Nombre varchar(50),
@IdOrganizacion int,
@Activo bit

as
begin
update Categoria
set Nombre=@Nombre,
	IdOrganizacion=@IdOrganizacion,
	Activo=@Activo
where IdCategoria=@IdCategoria

end
GO
/****** Object:  StoredProcedure [dbo].[Categoria_Eliminar]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Categoria_Eliminar]
@IdCategoria int
as
begin
delete Categoria
where IdCategoria=@IdCategoria
end
GO
/****** Object:  StoredProcedure [dbo].[Categoria_Nuevo]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Categoria_Nuevo]
@Nombre varchar(50),
@IdOrganizacion int,
@Activo bit
as
begin
insert into Categoria
values(@Nombre,@IdOrganizacion,@Activo)
end
GO
/****** Object:  StoredProcedure [dbo].[Colaboradores_Consulta]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Colaboradores_Consulta]
@IdOrganizacion int

as
begin

select distinct C.Identificacion,C.Nombre,C.Apellido1,C.Apellido2,C.Puesto,P.NombrePuesto,C.FechaContratacion,C.FechaNacimiento,C.FechaVinculacion,C.Codigo,C.Estado,
				C.IdOrganizacion,O.NombreEmpresa

from Colaboradores C INNER JOIN PerfilesPuesto P on C.Puesto=P.IdPerfil
					 INNER JOIN Organizacion O on C.IdOrganizacion=O.IdOrganizacion
					 INNER JOIN Organizacion on O.IdOrganizacion=@IdOrganizacion

end
GO
/****** Object:  StoredProcedure [dbo].[Colaboradores_Editar]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Colaboradores_Editar]

@Identificacion varchar(50),
@Nombre varchar(50),
@Apellido1 varchar(50),
@Apellido2 varchar(50),
@Puesto int,
@Codigo int,
@FechaContratacion date,
@FechaNacimiento date,
@FechaVinculacion date,
@Estado bit,
@IdOrganizacion int

as
begin

update Colaboradores
set Nombre=@Nombre,
	Apellido1=@Apellido1,
	Apellido2=@Apellido2,
	Puesto=@Puesto,
	Codigo=@Codigo,
	FechaContratacion=@FechaContratacion,
	FechaNacimiento=@FechaNacimiento,
	FechaVinculacion=@FechaVinculacion,
	Estado=@Estado,
	IdOrganizacion=@IdOrganizacion
where Identificacion=@Identificacion

end
GO
/****** Object:  StoredProcedure [dbo].[Colaboradores_Eliminar]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Colaboradores_Eliminar]
@Identificacion varchar(50)

as
begin

delete Colaboradores
where Identificacion=@Identificacion

end
GO
/****** Object:  StoredProcedure [dbo].[Colaboradores_Nuevo]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[Colaboradores_Nuevo]
@Identificacion varchar(50),
@Nombre varchar(50),
@Apellido1 varchar(50),
@Apellido2 varchar(50),
@Puesto int,
@Codigo int,
@FechaContratacion date,
@FechaNacimiento date,
@FechaVinculacion date,
@Estado bit,
@IdOrganizacion int

as
begin

insert into Colaboradores
values(@Identificacion,@Nombre,@Apellido1,@Apellido2,@Puesto,@Codigo,@FechaContratacion,@FechaNacimiento,@FechaVinculacion,@Estado,@IdOrganizacion)

end
GO
/****** Object:  StoredProcedure [dbo].[ConsultaColaboradores]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[ConsultaColaboradores]

@IdOrganizacion int

as
begin

select C.Identificacion,C.Nombre+' '+C.Apellido1 as Nombre,C.Puesto,P.NombrePuesto,C.FechaContratacion,C.FechaNacimiento,C.FechaVinculacion,C.Estado,
	   C.IdOrganizacion,O.NombreEmpresa

from Colaboradores C INNER JOIN PerfilesPuesto P on C.Puesto=P.IdPerfil
					 INNER JOIN Organizacion O on C.IdOrganizacion=O.IdOrganizacion
					 INNER JOIN Organizacion on O.IdOrganizacion=@IdOrganizacion

end
GO
/****** Object:  StoredProcedure [dbo].[Correo_Consulta]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Correo_Consulta]
@IdOrganizacion int
as
begin
select distinct C.IdCorreo,C.ServidorSMTP,C.POP3,C.CorreoNotificaciones,C.ContrasenaCorreo,C.Autenticacion,C.Usuario,C.SSL,C.Puerto,C.Observaciones,C.IdOrganizacion
from Correo C inner join Organizacion O on C.IdOrganizacion=O.IdOrganizacion
			  inner join Organizacion on O.IdOrganizacion=@IdOrganizacion
end
GO
/****** Object:  StoredProcedure [dbo].[Correo_Editar]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Correo_Editar]
@IdCorreo int,
@ServidorSMTP varchar(50),
@CorreoNotificaciones varchar(50),
@ContrasenaCorreo varchar(50),
@SSL varchar(50),
@Puerto int,
@Observaciones varchar(100),
@IdOrganizacion int,
@POP3 varchar(50),
@Autenticacion bit,
@Usuario varchar(50)

as
begin
update Correo
set ServidorSMTP=@ServidorSMTP,
	CorreoNotificaciones=@CorreoNotificaciones,
	ContrasenaCorreo=@ContrasenaCorreo,
	SSL=@SSL,
	Puerto=@Puerto,
	Observaciones=@Observaciones,
	IdOrganizacion=@IdOrganizacion,
	POP3=@POP3,
	Autenticacion=@Autenticacion,
	Usuario=@Usuario
where IdCorreo=@IdCorreo
end
GO
/****** Object:  StoredProcedure [dbo].[Correo_Eliminar]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Correo_Eliminar]
@IdCorreo int

as
begin
delete Correo
where IdCorreo=@IdCorreo
end
GO
/****** Object:  StoredProcedure [dbo].[Correo_Nuevo]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Correo_Nuevo]
@ServidorSMTP varchar(50),
@CorreoNotificaciones varchar(50),
@ContrasenaCorreo varchar(50),
@SSL bit,
@Puerto int,
@Observaciones varchar(100),
@IdOrganizacion int,
@POP3 varchar(50),
@Autenticacion bit,
@Usuario varchar(50)
as
begin
insert into Correo
values(@ServidorSMTP,@CorreoNotificaciones,@ContrasenaCorreo,@SSL,@Puerto,@Observaciones,@IdOrganizacion,@POP3,@Autenticacion,@Usuario)
end
GO
/****** Object:  StoredProcedure [dbo].[Editar_Seccion]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Editar_Seccion]
@IdSeccion int,
@Nombre varchar(50),
@Activo bit
as
begin

update Secciones
set Nombre=@Nombre,
	Activo=@Activo
where IdSeccion=@IdSeccion

end
GO
/****** Object:  StoredProcedure [dbo].[GrupoEmpresarial_Consulta]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[GrupoEmpresarial_Consulta]
as
begin
select * from GrupoEmpresarial
end
GO
/****** Object:  StoredProcedure [dbo].[GrupoEmpresarial_Editar]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[GrupoEmpresarial_Editar]
@IdGrupo int,
@Nombre varchar(50),
@Descripcion varchar(100),
@Activo bit

as
begin

update GrupoEmpresarial
set Nombre=@Nombre,
	Descripcion=@Descripcion,
	Activo=@Activo
where IdGrupo=@IdGrupo

end
GO
/****** Object:  StoredProcedure [dbo].[GrupoEmpresarial_Eliminar]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[GrupoEmpresarial_Eliminar]
@IdGrupo int
as
begin
delete GrupoEmpresarial
where IdGrupo=@IdGrupo
end
GO
/****** Object:  StoredProcedure [dbo].[GrupoEmpresarial_Nuevo]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[GrupoEmpresarial_Nuevo]
@Nombre varchar(50),
@Descripcion varchar(100),
@Activo bit

as
begin

insert into GrupoEmpresarial
values (@Nombre,@Descripcion,@Activo);

end
GO
/****** Object:  StoredProcedure [dbo].[ImagenOrganizacion]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[ImagenOrganizacion]

as
begin
select IdOrganizacion,Logo
from Organizacion
end

GO
/****** Object:  StoredProcedure [dbo].[Login]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Login]


AS
BEGIN

select U.IdUsuario, U.Clave,U.IdRol,U.Nombre+' '+U.Apellido1 as Nombre, U.IdOrganizacion, U.Estado, O.Activa,U.Id
from Usuarios U inner join Roles R on U.IdRol=R.IdRol
				inner join Organizacion O on O.IdOrganizacion=U.IdOrganizacion

END
GO
/****** Object:  StoredProcedure [dbo].[Organizacion_Consulta]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[Organizacion_Consulta]
As
Begin
select O.IdOrganizacion,O.NombreEmpresa,G.IdGrupo,G.Nombre as GrupoEmpresarial,O.Activa,O.CantidadUsuario,O.Observaciones,U.Id,U.Nombre + ' ' + U.Apellido1 as Usuario, U.IdUsuario
from Organizacion O left outer join GrupoEmpresarial G on (O.GrupoEmpresarial = G.IdGrupo or O.GrupoEmpresarial = null)
					inner join Usuarios U on U.Id = O.IdUsuarioAdmin

End
GO
/****** Object:  StoredProcedure [dbo].[Organizacion_Editar]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Organizacion_Editar]
@IdOrganizacion int,
@NombreEmpresa varchar(50),
@GrupoEmpresarial int,
@Activa bit,
@CantidadUsuarios int,
@Observaciones varchar(100)

as
begin
update Organizacion
set NombreEmpresa=@NombreEmpresa,
	GrupoEmpresarial=@GrupoEmpresarial,
	Activa=@Activa,
	CantidadUsuario=@CantidadUsuarios,
	Observaciones=@Observaciones
where IdOrganizacion=@IdOrganizacion

end
GO
/****** Object:  StoredProcedure [dbo].[Organizacion_Elimina]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Organizacion_Elimina]
@IdOrganizacion int

as
begin
delete Organizacion
where IdOrganizacion=@IdOrganizacion
end
GO
/****** Object:  StoredProcedure [dbo].[Organizacion_Nuevo]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[Organizacion_Nuevo]
--Usuarios

@Login varchar(100),
@Nombre varchar(50),
@Apellido1 varchar(50),
@Apellido2 varchar(50),
@Clave varchar(50),
@Correo varchar(100),
@Estado bit,
@IdRol int,
--Organizacion
@IdOrganizacion int,
@NombreEmpresa varchar(50),
@GrupoEmpresarial bit,
@Activa bit,
@CantidadUsuario int,
@Logo image,
@Observaciones varchar(100),
@IdUsuarioAdministrador int,

@IdSeccion int

as
begin

--Inserta tabla Usuarios
insert into Usuarios (IdUsuario,Nombre,Apellido1,Apellido2,Clave,Correo,Estado,IdRol)
values (@Login,@Nombre,@Apellido1,@Apellido2,@Clave,@Correo,@Estado,@IdRol)

--Selecciona Id Adminstrador nuevo
select @IdUsuarioAdministrador=SCOPE_IDENTITY();

--Inserta en organizacion
insert into Organizacion
values(@NombreEmpresa,@GrupoEmpresarial,@Activa,@CantidadUsuario,@Logo,@Observaciones,@IdUsuarioAdministrador)

--selecciona idOrganizacion
select @IdOrganizacion=SCOPE_IDENTITY();

update Usuarios
set IdOrganizacion=@IdOrganizacion
where Id=@IdUsuarioAdministrador


end
GO
/****** Object:  StoredProcedure [dbo].[Organizacion_NuevoAdministrador]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[Organizacion_NuevoAdministrador]
@IdOrganizacion int,
@IdUsuarioAdministrador int,

@Login varchar(100),
@Nombre varchar(50),
@Apellido1 varchar(50),
@Apellido2 varchar(50),
@Clave varchar(50),
@Correo varchar(100),
@Estado bit,
@IdRol int

as
begin

insert into Usuarios (IdUsuario,Nombre,Apellido1,Apellido2,Clave,Correo,Estado,IdRol,IdSeccion)
values (@Login,@Nombre,@Apellido1,@Apellido2,@Clave,@Correo,@Estado,@IdRol,(select IDENT_CURRENT('Secciones')));

select @IdUsuarioAdministrador=SCOPE_IDENTITY();

update Organizacion
set IdUsuarioAdmin=@IdUsuarioAdministrador
where IdOrganizacion=@IdOrganizacion;

End
GO
/****** Object:  StoredProcedure [dbo].[Perfiles_Elimina]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Perfiles_Elimina]
@IdPerfil int
as
begin
delete PerfilesPuesto
where IdPerfil=@IdPerfil
end
GO
/****** Object:  StoredProcedure [dbo].[PerfilesPuesto_Aprobar]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[PerfilesPuesto_Aprobar]
@IdPerfilPuesto int
as
begin
update PerfilesPuesto
set IdEstado=1
where IdPerfil=@IdPerfilPuesto
end
GO
/****** Object:  StoredProcedure [dbo].[PerfilesPuesto_Consulta]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[PerfilesPuesto_Consulta]
@IdOrganizacion int
as
begin

select distinct
	   P.IdPerfil,P.Codigo,P.Version,P.NombrePuesto,P.Descripcion,P.FechaModificación,
	   U.Id,U.Nombre+' '+U.Apellido1 as Administrador,
	   Res.Id as IdResp,Res.Nombre+' '+Res.Apellido1 as Responsable,
	   S.IdSeccion,S.Nombre as Seccion,
	   SC.IdSubcategoria,SC.Nombre as SubCategoria,
	   A.IdArchivo,A.Nombre+'.'+A.Extension as Archivo,
	   E.IdEstado,E.Nombre as Estado,
	   P.IdCategoria, C.Nombre as Categoria

from PerfilesPuesto P LEFT JOIN Usuarios U on P.Administrador = U.Id
					  LEFT JOIN Usuarios Res on P.Responsable = Res.Id
					  INNER JOIN Estados E on P.IdEstado = E.IdEstado
					  INNER JOIN Secciones S on P.Seccion = S.IdSeccion
					  INNER JOIN Archivo A on A.IdArchivo = P.IdArchivo
					  INNER JOIN SubCategoria SC on SC.IdSubcategoria = P.SubCategoria
					  INNER JOIN Organizacion O on S.IdOrganizacion=O.IdOrganizacion
					  INNER JOIN Organizacion on O.IdOrganizacion=@IdOrganizacion
					  INNER JOIN Categoria C on P.IdCategoria=C.IdCategoria

end
GO
/****** Object:  StoredProcedure [dbo].[PerfilesPuesto_Nuevo]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[PerfilesPuesto_Nuevo]
--parametros para la tabla Perfiles Puesto
@Codigo varchar(50),
@Version int,
@Titulo varchar(50),
@Descripcion varchar(100),
@Administrador int,
@Responsable int,
@FechaVigencia date,
@Seccion int,
@SubCategoria int,
@IdCategoria int,
--@IdArchivoPP int,
@IdEstado int,
--parametros para tabla Archivo
@Archivo image,
@Nombre varchar(50),
@Extension varchar(50),
@IdArchivo int OUTPUT

as
begin

--insertar a tabla archivo
insert into Archivo
values(@Archivo,@Nombre,@Extension);

--selecciona el nuevo ID
select @IdArchivo = SCOPE_IDENTITY();

--inserta a tabla PerfilesPuesto
insert into PerfilesPuesto(Codigo,Version,NombrePuesto,Descripcion,Administrador,Responsable,FechaModificación,Seccion,
			Subcategoria,IdArchivo,IdEstado,IdCategoria)
values (@Codigo,@Version,@Titulo,@Descripcion,@Administrador,@Responsable,@FechaVigencia,@Seccion,@SubCategoria,
		@IdArchivo,@IdEstado,@IdCategoria);

end
GO
/****** Object:  StoredProcedure [dbo].[PerfilesPuesto_Obsoleto]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[PerfilesPuesto_Obsoleto]
@IdPerfilPuesto int
as
begin
update PerfilesPuesto
set IdEstado=2
where IdPerfil=@IdPerfilPuesto
end
GO
/****** Object:  StoredProcedure [dbo].[PerfilPuesto_Editar]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[PerfilPuesto_Editar]
@IdPerfil int,
@Codigo varchar(50),
@Version int,
@Nombre varchar(50),
@Descripcion varchar(100),
@Administrador int,
@Responsable int,
@FechaVigencia date,
@Seccion int,
@SubCategoria int,
@IdCategoria int

as
begin
update PerfilesPuesto
set Codigo=@Codigo,
	Version=@Version,
	NombrePuesto=@Nombre,
	Descripcion=@Descripcion,
	Administrador=@Administrador,
	Responsable=@Responsable,
	FechaModificación=@FechaVigencia,
	Seccion=@Seccion,
	Subcategoria=@SubCategoria,
	IdCategoria=@IdCategoria
where IdPerfil=@IdPerfil

end
GO
/****** Object:  StoredProcedure [dbo].[Roles_Consulta]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[Roles_Consulta]
as
begin
select * from Roles
end
GO
/****** Object:  StoredProcedure [dbo].[Seccion_Eliminar]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Seccion_Eliminar]
@IdSeccion int
as
begin
delete Secciones
where IdSeccion=@IdSeccion
end
GO
/****** Object:  StoredProcedure [dbo].[Secciones_Consulta]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Secciones_Consulta]
@IdOrganizacion int
as
begin
select distinct S.IdSeccion,S.Nombre,S.Activo,O.IdOrganizacion,O.NombreEmpresa
from Secciones S inner join Organizacion O on O.IdOrganizacion=S.IdOrganizacion
				 inner join Organizacion on O.IdOrganizacion=@IdOrganizacion
end
GO
/****** Object:  StoredProcedure [dbo].[Secciones_Nuevo]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Secciones_Nuevo]
@Nombre varchar(50),
@Activo bit,
@IdOrganizacion int
as
begin
insert into Secciones
values(@Nombre,@Activo,@IdOrganizacion)
end
GO
/****** Object:  StoredProcedure [dbo].[SubCategoria_Consulta]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SubCategoria_Consulta]
@IdOrganizacion int
as
begin
select distinct S.IdSubcategoria,S.Nombre,O.IdOrganizacion,S.Activo
from SubCategoria S INNER JOIN Organizacion O on S.IdOrganizacion=O.IdOrganizacion
					INNER JOIN Organizacion on O.IdOrganizacion=@IdOrganizacion
end
GO
/****** Object:  StoredProcedure [dbo].[SubCategoria_Editar]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SubCategoria_Editar]
@IdSubcategoria int,
@Nombre varchar(50),
@IdOrganizacion int,
@Activo bit

as
begin
update SubCategoria
set Nombre=@Nombre,
	IdOrganizacion=@IdOrganizacion,
	Activo=@Activo
where IdSubcategoria=@IdSubcategoria

end
GO
/****** Object:  StoredProcedure [dbo].[SubCategoria_Eliminar]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SubCategoria_Eliminar]
@IdSubcategoria int

as
begin
delete SubCategoria
where IdSubcategoria=@IdSubcategoria
end
GO
/****** Object:  StoredProcedure [dbo].[SubCategoria_Nuevo]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SubCategoria_Nuevo]
@Nombre varchar(50),
@IdOrganizacion int,
@Activo bit
as
begin
insert into SubCategoria
values(@Nombre,@IdOrganizacion,@Activo)
end
GO
/****** Object:  StoredProcedure [dbo].[Usuario_Eliminar]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[Usuario_Eliminar]
@IdUsuario int
as
begin
delete Usuarios
where Id=@IdUsuario
end
GO
/****** Object:  StoredProcedure [dbo].[Usuarios_Consulta]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[Usuarios_Consulta]
@IdOrganizacion int
As
Begin
select distinct U.Id,U.IdUsuario,U.Nombre,U.Apellido1,U.Apellido2,U.Correo,U.Estado,U.IdOrganizacion,
				R.IdRol,R.Nombre as Rol,
				S.IdSeccion,S.Nombre as Seccion
from Usuarios U LEFT JOIN Roles R on U.IdRol = R.IdRol
				Left JOIN Secciones S on U.IdSeccion = S.IdSeccion
				--RIGHT JOIN Secciones on S.IdOrganizacion=2--@IdOrganizacion
				RIGHT JOIN Organizacion O on U.IdOrganizacion=O.IdOrganizacion

order by U.Nombre asc

END
GO
/****** Object:  StoredProcedure [dbo].[Usuarios_Editar]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Usuarios_Editar]
@Id int,
@IdUsuario varchar(100),
@Nombre varchar(50),
@Apellido1 varchar(50),
@Apellido2 varchar(50),
@Correo varchar(100),
@Estado bit,
@IdRol int,
@IdSeccion int

as
begin
update Usuarios
set IdUsuario=@IdUsuario,
	Nombre=@Nombre,
	Apellido1=@Apellido1,
	Apellido2=@Apellido2,
	Correo=@Correo,
	Estado=@Estado,
	IdRol=@IdRol,
	IdSeccion=@IdSeccion
where Id=@Id

end
GO
/****** Object:  StoredProcedure [dbo].[Usuarios_Nuevo]    Script Date: 07/09/2018 05:41:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[Usuarios_Nuevo]
@IdUsuario varchar(100),
@Nombre varchar(50),
@Apellido1 varchar(50),
@Apellido2 varchar(50),
@Clave varchar(50),
@Correo varchar(100),
@Estado bit,
@IdRol int,
@IdSeccion int,
@IdOrganizacion int

As
BEGIN

insert into Usuarios
values (@IdUsuario,@Nombre,@Apellido1,@Apellido2,@Clave,@Correo,@Estado,@IdRol,@IdSeccion,@IdOrganizacion)

END
GO
