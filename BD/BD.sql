USE [RH Plus]
GO
/****** Object:  Table [dbo].[Archivo]    Script Date: 07/09/2018 05:39:40 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Archivo](
	[IdArchivo] [int] IDENTITY(1,1) NOT NULL,
	[Archivo] [image] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Extension] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Archivo] PRIMARY KEY CLUSTERED 
(
	[IdArchivo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Categoria]    Script Date: 07/09/2018 05:39:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Categoria](
	[IdCategoria] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[IdOrganizacion] [int] NOT NULL,
	[Activo] [bit] NOT NULL,
 CONSTRAINT [PK_Categoria] PRIMARY KEY CLUSTERED 
(
	[IdCategoria] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Colaboradores]    Script Date: 07/09/2018 05:39:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Colaboradores](
	[Identificacion] [varchar](50) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Apellido1] [varchar](50) NOT NULL,
	[Apellido2] [varchar](50) NOT NULL,
	[Puesto] [int] NOT NULL,
	[Codigo] [int] NOT NULL,
	[FechaContratacion] [date] NOT NULL,
	[FechaNacimiento] [date] NOT NULL,
	[FechaVinculacion] [date] NOT NULL,
	[Estado] [bit] NOT NULL,
	[IdOrganizacion] [int] NOT NULL,
 CONSTRAINT [PK_Colaboradores] PRIMARY KEY CLUSTERED 
(
	[Identificacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Correo]    Script Date: 07/09/2018 05:39:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Correo](
	[IdCorreo] [int] IDENTITY(1,1) NOT NULL,
	[ServidorSMTP] [varchar](50) NULL,
	[CorreoNotificaciones] [varchar](50) NULL,
	[ContrasenaCorreo] [varchar](50) NULL,
	[SSL] [bit] NULL,
	[Puerto] [int] NULL,
	[Observaciones] [varchar](100) NULL,
	[IdOrganizacion] [int] NOT NULL,
	[POP3] [varchar](50) NULL,
	[Autenticacion] [bit] NULL,
	[Usuario] [varchar](50) NULL,
 CONSTRAINT [PK_Correo] PRIMARY KEY CLUSTERED 
(
	[IdCorreo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Estados]    Script Date: 07/09/2018 05:39:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Estados](
	[IdEstado] [int] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Estados] PRIMARY KEY CLUSTERED 
(
	[IdEstado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GrupoEmpresarial]    Script Date: 07/09/2018 05:39:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GrupoEmpresarial](
	[IdGrupo] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Descripcion] [varchar](100) NOT NULL,
	[Activo] [bit] NOT NULL,
 CONSTRAINT [PK_GrupoEmpresarial] PRIMARY KEY CLUSTERED 
(
	[IdGrupo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Organizacion]    Script Date: 07/09/2018 05:39:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Organizacion](
	[IdOrganizacion] [int] IDENTITY(1,1) NOT NULL,
	[NombreEmpresa] [varchar](50) NOT NULL,
	[GrupoEmpresarial] [int] NULL,
	[Activa] [bit] NOT NULL,
	[CantidadUsuario] [int] NOT NULL,
	[Logo] [image] NULL,
	[Observaciones] [varchar](100) NULL,
	[IdUsuarioAdmin] [int] NULL,
 CONSTRAINT [PK_Organizacion] PRIMARY KEY CLUSTERED 
(
	[IdOrganizacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PerfilesPuesto]    Script Date: 07/09/2018 05:39:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PerfilesPuesto](
	[IdPerfil] [int] IDENTITY(1,1) NOT NULL,
	[Codigo] [varchar](50) NOT NULL,
	[Version] [int] NOT NULL,
	[NombrePuesto] [varchar](50) NOT NULL,
	[Descripcion] [varchar](100) NOT NULL,
	[Administrador] [int] NOT NULL,
	[Responsable] [int] NOT NULL,
	[FechaModificación] [date] NOT NULL,
	[Seccion] [int] NOT NULL,
	[Subcategoria] [int] NOT NULL,
	[IdArchivo] [int] NOT NULL,
	[IdEstado] [int] NOT NULL,
	[IdCategoria] [int] NOT NULL,
 CONSTRAINT [PK_PerfilesPuesto] PRIMARY KEY CLUSTERED 
(
	[IdPerfil] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 07/09/2018 05:39:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Roles](
	[IdRol] [int] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[IdRol] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Secciones]    Script Date: 07/09/2018 05:39:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Secciones](
	[IdSeccion] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Activo] [bit] NOT NULL,
	[IdOrganizacion] [int] NOT NULL,
 CONSTRAINT [PK_Secciones] PRIMARY KEY CLUSTERED 
(
	[IdSeccion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SubCategoria]    Script Date: 07/09/2018 05:39:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SubCategoria](
	[IdSubcategoria] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[IdOrganizacion] [int] NOT NULL,
	[Activo] [bit] NOT NULL,
 CONSTRAINT [PK_SubCategoria] PRIMARY KEY CLUSTERED 
(
	[IdSubcategoria] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 07/09/2018 05:39:42 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuarios](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdUsuario] [varchar](100) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Apellido1] [varchar](50) NOT NULL,
	[Apellido2] [varchar](50) NOT NULL,
	[Clave] [varchar](50) NOT NULL,
	[Correo] [varchar](100) NOT NULL,
	[Estado] [bit] NOT NULL,
	[IdRol] [int] NOT NULL,
	[IdSeccion] [int] NULL,
	[IdOrganizacion] [int] NULL,
 CONSTRAINT [PK_Usuarios] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_Usuarios] UNIQUE NONCLUSTERED 
(
	[IdUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Categoria]  WITH CHECK ADD  CONSTRAINT [FK_Categoria_Organizacion] FOREIGN KEY([IdOrganizacion])
REFERENCES [dbo].[Organizacion] ([IdOrganizacion])
GO
ALTER TABLE [dbo].[Categoria] CHECK CONSTRAINT [FK_Categoria_Organizacion]
GO
ALTER TABLE [dbo].[Colaboradores]  WITH CHECK ADD  CONSTRAINT [FK_Colaboradores_Organizacion] FOREIGN KEY([IdOrganizacion])
REFERENCES [dbo].[Organizacion] ([IdOrganizacion])
GO
ALTER TABLE [dbo].[Colaboradores] CHECK CONSTRAINT [FK_Colaboradores_Organizacion]
GO
ALTER TABLE [dbo].[Colaboradores]  WITH CHECK ADD  CONSTRAINT [FK_Colaboradores_PerfilesPuesto] FOREIGN KEY([Puesto])
REFERENCES [dbo].[PerfilesPuesto] ([IdPerfil])
GO
ALTER TABLE [dbo].[Colaboradores] CHECK CONSTRAINT [FK_Colaboradores_PerfilesPuesto]
GO
ALTER TABLE [dbo].[Correo]  WITH CHECK ADD  CONSTRAINT [FK_Correo_Organizacion] FOREIGN KEY([IdOrganizacion])
REFERENCES [dbo].[Organizacion] ([IdOrganizacion])
GO
ALTER TABLE [dbo].[Correo] CHECK CONSTRAINT [FK_Correo_Organizacion]
GO
ALTER TABLE [dbo].[Organizacion]  WITH CHECK ADD  CONSTRAINT [FK_Organizacion_GrupoEmpresarial] FOREIGN KEY([GrupoEmpresarial])
REFERENCES [dbo].[GrupoEmpresarial] ([IdGrupo])
GO
ALTER TABLE [dbo].[Organizacion] CHECK CONSTRAINT [FK_Organizacion_GrupoEmpresarial]
GO
ALTER TABLE [dbo].[Organizacion]  WITH CHECK ADD  CONSTRAINT [FK_Organizacion_Usuarios] FOREIGN KEY([IdUsuarioAdmin])
REFERENCES [dbo].[Usuarios] ([Id])
GO
ALTER TABLE [dbo].[Organizacion] CHECK CONSTRAINT [FK_Organizacion_Usuarios]
GO
ALTER TABLE [dbo].[PerfilesPuesto]  WITH CHECK ADD  CONSTRAINT [FK_PerfilesPuesto_Archivo] FOREIGN KEY([IdArchivo])
REFERENCES [dbo].[Archivo] ([IdArchivo])
GO
ALTER TABLE [dbo].[PerfilesPuesto] CHECK CONSTRAINT [FK_PerfilesPuesto_Archivo]
GO
ALTER TABLE [dbo].[PerfilesPuesto]  WITH CHECK ADD  CONSTRAINT [FK_PerfilesPuesto_Estados] FOREIGN KEY([IdEstado])
REFERENCES [dbo].[Estados] ([IdEstado])
GO
ALTER TABLE [dbo].[PerfilesPuesto] CHECK CONSTRAINT [FK_PerfilesPuesto_Estados]
GO
ALTER TABLE [dbo].[PerfilesPuesto]  WITH CHECK ADD  CONSTRAINT [FK_PerfilesPuesto_Secciones] FOREIGN KEY([Seccion])
REFERENCES [dbo].[Secciones] ([IdSeccion])
GO
ALTER TABLE [dbo].[PerfilesPuesto] CHECK CONSTRAINT [FK_PerfilesPuesto_Secciones]
GO
ALTER TABLE [dbo].[PerfilesPuesto]  WITH CHECK ADD  CONSTRAINT [FK_PerfilesPuesto_SubCategoria] FOREIGN KEY([Subcategoria])
REFERENCES [dbo].[SubCategoria] ([IdSubcategoria])
GO
ALTER TABLE [dbo].[PerfilesPuesto] CHECK CONSTRAINT [FK_PerfilesPuesto_SubCategoria]
GO
ALTER TABLE [dbo].[PerfilesPuesto]  WITH CHECK ADD  CONSTRAINT [FK_PerfilesPuesto_Usuarios] FOREIGN KEY([Administrador])
REFERENCES [dbo].[Usuarios] ([Id])
GO
ALTER TABLE [dbo].[PerfilesPuesto] CHECK CONSTRAINT [FK_PerfilesPuesto_Usuarios]
GO
ALTER TABLE [dbo].[PerfilesPuesto]  WITH CHECK ADD  CONSTRAINT [FK_PerfilesPuesto_Usuarios1] FOREIGN KEY([Responsable])
REFERENCES [dbo].[Usuarios] ([Id])
GO
ALTER TABLE [dbo].[PerfilesPuesto] CHECK CONSTRAINT [FK_PerfilesPuesto_Usuarios1]
GO
ALTER TABLE [dbo].[Secciones]  WITH CHECK ADD  CONSTRAINT [FK_Secciones_Organizacion] FOREIGN KEY([IdOrganizacion])
REFERENCES [dbo].[Organizacion] ([IdOrganizacion])
GO
ALTER TABLE [dbo].[Secciones] CHECK CONSTRAINT [FK_Secciones_Organizacion]
GO
ALTER TABLE [dbo].[SubCategoria]  WITH CHECK ADD  CONSTRAINT [FK_SubCategoria_Organizacion] FOREIGN KEY([IdOrganizacion])
REFERENCES [dbo].[Organizacion] ([IdOrganizacion])
GO
ALTER TABLE [dbo].[SubCategoria] CHECK CONSTRAINT [FK_SubCategoria_Organizacion]
GO
ALTER TABLE [dbo].[Usuarios]  WITH CHECK ADD  CONSTRAINT [FK_Usuarios_Organizacion] FOREIGN KEY([IdOrganizacion])
REFERENCES [dbo].[Organizacion] ([IdOrganizacion])
GO
ALTER TABLE [dbo].[Usuarios] CHECK CONSTRAINT [FK_Usuarios_Organizacion]
GO
ALTER TABLE [dbo].[Usuarios]  WITH CHECK ADD  CONSTRAINT [FK_Usuarios_Roles] FOREIGN KEY([IdRol])
REFERENCES [dbo].[Roles] ([IdRol])
GO
ALTER TABLE [dbo].[Usuarios] CHECK CONSTRAINT [FK_Usuarios_Roles]
GO
ALTER TABLE [dbo].[Usuarios]  WITH CHECK ADD  CONSTRAINT [FK_Usuarios_Secciones] FOREIGN KEY([IdSeccion])
REFERENCES [dbo].[Secciones] ([IdSeccion])
GO
ALTER TABLE [dbo].[Usuarios] CHECK CONSTRAINT [FK_Usuarios_Secciones]
GO
