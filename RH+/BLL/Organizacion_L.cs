﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace BLL
{
    public class Organizacion_L
    {
        #region propiedades
        //Variables de organizacion
        private int _IdOrganizacion;
        public int IdOrganizacion
        {
            get { return _IdOrganizacion; }
            set { _IdOrganizacion = value; }
        }

        private string _NombreEmpresa;
        public string NombreEmpresa
        {
            get { return _NombreEmpresa; }
            set { _NombreEmpresa = value; }
        }

        private int _GrupoEmpresarial;
        public int GrupoEmpresarial
        {
            get { return _GrupoEmpresarial; }
            set { _GrupoEmpresarial = value; }
        }

        private bool _Activa;
        public bool Activa
        {
            get { return _Activa; }
            set { _Activa = value; }
        }

        private string _URL;
        public string URL
        {
            get { return _URL; }
            set { _URL = value; }
        }

        private int _CantidadUsuarios;
        public int CantidadUsuarios
        {
            get { return _CantidadUsuarios; }
            set { _CantidadUsuarios = value; }
        }

        private byte[] _Logo;
        public byte[] Logo
        {
            get { return _Logo; }
            set { _Logo = value; }
        }

        private string _Observaciones;
        public string Observaciones
        {
            get { return _Observaciones; }
            set { _Observaciones = value; }
        }

        private int _IdUsarioAdmin;
        public int IdUsuarioAdmin
        {
            get { return _IdUsarioAdmin; }
            set { _IdUsarioAdmin = value; }
        }

        //variables de usuarios
        private string _Login;
        public string Login
        {
            get { return _Login; }
            set { _Login = value; }
        }

        private string _Nombre;
        public string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }

        private string _Apellido1;
        public string Apellido1
        {
            get { return _Apellido1; }
            set { _Apellido1 = value; }
        }

        private string _Apellido2;
        public string Apellido2
        {
            get { return _Apellido2; }
            set { _Apellido2 = value; }
        }

        private string _Clave;
        public string Clave
        {
            get { return _Clave; }
            set { _Clave = value; }
        }

        private string _Correo;
        public string Correo
        {
            get { return _Correo; }
            set { _Correo = value; }
        }

        private bool _Estado;
        public bool Estado
        {
            get { return _Estado; }
            set { _Estado = value; }
        }

        private int _IdRol;
        public int IdRol
        {
            get { return _IdRol; }
            set { _IdRol = value; }
        }

        private int _IdSeccion;
        public int IdSeccion
        {
            get { return _IdSeccion; }
            set { _IdSeccion = value; }
        }

        #endregion

        #region variables Privadas

        SqlConnection conexion;
        string mensaje_error;
        int numero_error;
        string sql;
        DataSet ds;

        #endregion

        #region Metodos

        public DataSet Carga_Lista_Organizacion()
        {
            try
            {
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref numero_error);
                if (conexion == null)
                {
                    string error = "Ha ocurrido un error en la conexión con la BD. Favor contacte al administrador";
                    HttpContext.Current.Response.Redirect("~/ErroresSU.aspx?Error=" + error, false);
                    return null;
                }
                else
                {
                    sql = "RHPLUS_Organizacion_Consulta";
                    ds = cls_DAL.ejecuta_dataset_consulta(conexion, sql, true, ref mensaje_error, ref numero_error);

                    if (numero_error != 0)
                    {
                        string error = "Ha ocurrido un error al consultar con la BD. Favor contacte al administrador.";
                        HttpContext.Current.Response.Redirect("ErroresSU.aspx?Error=" + error, false);
                        return null;
                    }
                    else
                    {
                        return ds;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Agregar_Organizacion()
        {
            try
            {
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref numero_error);

                if (conexion == null)
                {
                    string error = "Ha ocurrido un error en la conexión con la BD. Favor contacte al administrador";
                    HttpContext.Current.Response.Redirect("~/ErroresSU.aspx?Error=" + error, false);
                    return false;
                }
                else
                {
                    sql = "RHPLUS_Organizacion_Nuevo";
                    paramStruct[] parametros = new paramStruct[17];
                    //parametros Organizacion
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 0, "@NombreEmpresa", SqlDbType.VarChar, _NombreEmpresa);
                    if (_GrupoEmpresarial == 0)
                    {
                        cls_DAL.agregar_datos_estructura_parametros(ref parametros, 1, "@GrupoEmpresarial", SqlDbType.Int, DBNull.Value);
                    }
                    else
                    {
                        cls_DAL.agregar_datos_estructura_parametros(ref parametros, 1, "@GrupoEmpresarial", SqlDbType.Int, _GrupoEmpresarial);
                    }
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 2, "@Activa", SqlDbType.Bit, _Activa);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 3, "@CantidadUsuario", SqlDbType.Int, _CantidadUsuarios);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 4, "@Logo", SqlDbType.Image, _Logo);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 5, "@Observaciones", SqlDbType.VarChar, _Observaciones);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 6, "@IdUsuarioAdministrador", SqlDbType.Int, _IdUsarioAdmin);

                    //parametros Usuario
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 7, "@Login", SqlDbType.VarChar, _Login);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 8, "@Nombre", SqlDbType.VarChar, _Nombre);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 9, "@Apellido1", SqlDbType.VarChar, _Apellido1);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 10, "@Apellido2", SqlDbType.VarChar, _Apellido2);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 11, "@Clave", SqlDbType.VarChar, Encriptar_Clave.generateHashDigest(_Clave, Encriptar_Clave.HashMethod.MD5));
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 12, "@Correo", SqlDbType.VarChar, _Correo);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 13, "@Estado", SqlDbType.Bit, _Estado);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 14, "@IdRol", SqlDbType.Int, _IdRol);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 15, "@IdOrganizacion", SqlDbType.Int, 0);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 16, "@IdSeccion", SqlDbType.Int, 0);

                    cls_DAL.conectar(conexion, ref mensaje_error, ref numero_error);
                    cls_DAL.ejecuta_sqlcommand_editar_agregar_imagen(conexion, sql, true, parametros, ref mensaje_error, ref numero_error);

                    if (numero_error != 0)
                    {
                        string error = "Ha ocurrido un error al insertar en la BD. Favor contacte al administrador.";
                        HttpContext.Current.Response.Redirect("~/ErroresSU.aspx?Error=" + error, false);
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return false;
                    }
                    else
                    {
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Editar_Organizacion()
        {
            try
            {
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref numero_error);
                if (conexion == null)
                {
                    string error = "Ha ocurrido un error en la conexión con la BD. Favor contacte al administrador";
                    HttpContext.Current.Response.Redirect("~/ErroresSU.aspx=Error" + error, false);
                    return false;
                }
                else
                {
                    sql = "RHPLUS_Organizacion_Editar";
                    paramStruct[] parametros = new paramStruct[6];
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 0, "@IdOrganizacion", SqlDbType.Int, _IdOrganizacion);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 1, "@NombreEmpresa", SqlDbType.VarChar, _NombreEmpresa);
                    if (_GrupoEmpresarial == 0)
                    {
                        cls_DAL.agregar_datos_estructura_parametros(ref parametros, 2, "@GrupoEmpresarial", SqlDbType.Int, DBNull.Value);
                    }
                    else
                    {
                        cls_DAL.agregar_datos_estructura_parametros(ref parametros, 2, "@GrupoEmpresarial", SqlDbType.Int, _GrupoEmpresarial);
                    }
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 3, "@Activa", SqlDbType.Int, _Activa);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 4, "@CantidadUsuarios", SqlDbType.Int, _CantidadUsuarios);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 5, "@Observaciones", SqlDbType.VarChar, _Observaciones);
                    cls_DAL.conectar(conexion, ref mensaje_error, ref numero_error);
                    cls_DAL.ejecuta_sqlcommand_editar_agregar_imagen(conexion, sql, true, parametros, ref mensaje_error, ref numero_error);
                    if (numero_error != 0)
                    {
                        string error = "Ha ocurrido un error al editar en la BD. Favor contacte al administrador.";
                        HttpContext.Current.Response.Redirect("ErroresSU.aspx?Error" + error, false);
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return false;
                    }
                    else
                    {
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Eliminar_Organizacion(int IdOrganizacion)
        {
            try
            {
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref numero_error);
                if (conexion == null)
                {
                    string error = "Ha ocurrido un error en la conexión con la BD. Favor contacte al administrador";
                    HttpContext.Current.Response.Redirect("~/ErroresSU.aspx?Error=" + error, false);
                    return false;
                }
                else
                {
                    sql = "RHPLUS_Organizacion_Elimina";
                    paramStruct[] parametros = new paramStruct[1];
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 0, "@IdOrganizacion", SqlDbType.Int, IdOrganizacion);
                    cls_DAL.conectar(conexion, ref mensaje_error, ref numero_error);
                    cls_DAL.ejecuta_sqlcommand_editar_agregar(conexion, sql, true, parametros, ref mensaje_error, ref numero_error);
                    if (numero_error != 0)
                    {
                        string error = "Ha ocurrido un error al eliminar en la BD. Favor contacte al administrador.";
                        HttpContext.Current.Response.Redirect("~/ErroresSU.aspx?Error=" + error, false);
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return false;
                    }
                    else
                    {
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Editar_UsuarioAdmin_Organizacion()
        {
            try
            {
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref numero_error);
                if (conexion == null)
                {
                    string error = "Ha ocurrido un error en la conexión con la BD. Favor contacte al administrador";
                    HttpContext.Current.Response.Redirect("~/ErroresSU.aspx=Error" + error, false);
                    return false;
                }
                else
                {
                    sql = "RHPLUS_Organizacion_EditaUsuario";
                    paramStruct[] parametros = new paramStruct[6];
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 0, "@Id", SqlDbType.Int, _IdUsarioAdmin);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 1, "@Login", SqlDbType.VarChar, _Login);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 2, "@Nombre", SqlDbType.VarChar, _Nombre);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 3, "@Apellido1", SqlDbType.VarChar, _Apellido1);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 4, "@Apellido2", SqlDbType.VarChar, _Apellido2);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 5, "@Correo", SqlDbType.VarChar, _Correo);

                    cls_DAL.conectar(conexion, ref mensaje_error, ref numero_error);
                    cls_DAL.ejecuta_sqlcommand_editar_agregar(conexion, sql, true, parametros, ref mensaje_error, ref numero_error);

                    if (numero_error != 0)
                    {
                        string error = "Ha ocurrido un error al insertar en la BD. Favor contacte al administrador.";
                        HttpContext.Current.Response.Redirect("~/ErroresSU.aspx?Error=" + error, false);
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return false;
                    }
                    else
                    {
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Reset_Clave_Administrador()
        {
            try
            {
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref numero_error);
                if (conexion == null)
                {
                    string error = "Ha ocurrido un error en la conexión con la BD. Favor contacte al administrador";
                    HttpContext.Current.Response.Redirect("`/ErroresSU.aspx=Error" + error, false);
                    return false;
                }
                else
                {
                    sql = "RHPLUS_Cambio_Clave";
                    paramStruct[] parametros = new paramStruct[2];
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 0, "@IdUsuario", SqlDbType.Int, _IdUsarioAdmin);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 1, "@Clave", SqlDbType.VarChar, Encriptar_Clave.generateHashDigest(_Clave, Encriptar_Clave.HashMethod.MD5));

                    cls_DAL.conectar(conexion, ref mensaje_error, ref numero_error);
                    cls_DAL.ejecuta_sqlcommand_editar_agregar(conexion, sql, true, parametros, ref mensaje_error, ref numero_error);

                    if (numero_error != 0)
                    {
                        string error = "Ha ocurrido un error al cambiar la contraseña. Favor contacte al administrador.";
                        HttpContext.Current.Response.Redirect("~/ErroresSU.aspx?Error=" + error, false);
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return false;
                    }
                    else
                    {
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool ResetClave()
        {
            try
            {
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref numero_error);
                if (conexion == null)
                {
                    string error = "Ha ocurrido un error en la conexión con la BD. Favor contacte al administrador";
                    HttpContext.Current.Response.Redirect("`/ErroresSU.aspx=Error" + error, false);
                    return false;
                }
                else
                {
                    sql = "RHPLUS_Cambio_Clave";
                    paramStruct[] parametros = new paramStruct[2];
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 0, "@IdUsuario", SqlDbType.Int, _IdUsarioAdmin);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 1, "@Clave", SqlDbType.VarChar, Encriptar_Clave.generateHashDigest(_Clave, Encriptar_Clave.HashMethod.MD5));

                    cls_DAL.conectar(conexion, ref mensaje_error, ref numero_error);
                    cls_DAL.ejecuta_sqlcommand_editar_agregar(conexion, sql, true, parametros, ref mensaje_error, ref numero_error);

                    if (numero_error != 0)
                    {
                        string error = "Ha ocurrido un error al cambiar la contraseña. Favor contacte al administrador.";
                        HttpContext.Current.Response.Redirect("~/ErroresSU.aspx?Error=" + error, false);
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return false;
                    }
                    else
                    {
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }
}
