﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.IO;

namespace BLL
{
    public class Encriptar_Clave
    {
        public enum HashMethod
        {
            MD5,
            SHA1,
            SHA384
        }

        public static String generateHashDigest(String source, HashMethod algorithm)
        {
            try
            {
                HashAlgorithm hashAlgor;
                switch (algorithm)
                {
                    case HashMethod.MD5:
                        hashAlgor = new MD5CryptoServiceProvider();
                        break;
                    case HashMethod.SHA1:
                        hashAlgor = new SHA1CryptoServiceProvider();
                        break;
                    case HashMethod.SHA384:
                        hashAlgor = new SHA384CryptoServiceProvider();
                        break;
                    default:
                        return null;
                }

                byte[] byteValue=Encoding.UTF8.GetBytes(source);
                byte[] hashValue=hashAlgor.ComputeHash(byteValue);
                return Convert.ToBase64String(hashValue);

            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
