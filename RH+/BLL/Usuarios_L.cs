﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using System.Web;
using System.IO;
using System.Security.Cryptography;

namespace BLL
{
    public class Usuarios_L
    {
        /// <summary>
        /// variables globales del módulo de usuarios.
        /// </summary>
        #region Propiedades

        private int _Id;
        public int Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private string _IdUsuario;

        public string IdUsuario
        {
            get { return _IdUsuario; }
            set { _IdUsuario = value; }
        }

        private string _Nombre;

        public string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }

        private string _Apellido1;

        public string Apellido1
        {
            get { return _Apellido1; }
            set { _Apellido1 = value; }
        }

        private string _Apellido2;

        public string Apellido2
        {
            get { return _Apellido2; }
            set { _Apellido2 = value; }
        }

        private string _Clave;

        public string Clave
        {
            get { return _Clave; }
            set { _Clave = value; }
        }

        private string _Correo;

        public string Correo
        {
            get { return _Correo; }
            set { _Correo = value; }
        }

        private bool _Estado;

        public bool Estado
        {
            get { return _Estado; }
            set { _Estado = value; }
        }

        private int _IdRol;

        public int IdRol
        {
            get { return _IdRol; }
            set { _IdRol = value; }
        }

        private int _IdSeccion;

        public int IdSeccion
        {
            get { return _IdSeccion; }
            set { _IdSeccion = value; }
        }

        private int _IdOrganizacion;
        public int IdOrganizacion
        {
            get { return _IdOrganizacion; }
            set { _IdOrganizacion = value; }
        }

        #endregion

        /// <summary>
        /// variables para la comunicación con la BD en la clase DAL.
        /// </summary>
        #region Varibles Priadas

        SqlConnection conexion;
        string mensaje_error;
        int num_error;
        string sql;
        DataSet ds;

        #endregion

        #region Metodos

        /// <summary>
        /// Realiza la consulta de los usuarios en la BD
        /// </summary>
        /// <returns>
        /// Retorna un DataSet con la información de la BD.
        /// </returns>
        public DataSet carga_lista_usuarios()
        {
            try
            {
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref num_error);
                if (conexion == null)
                {
                    string error = "Ha ocurrido un error en la conexión con la BD. Favor contacte al administrador";
                    HttpContext.Current.Response.Redirect("Errores.aspx?Error" + error, false);
                    return null;
                }
                else
                {
                    sql = "RHPLUS_Usuarios_Consulta";
                    ds = cls_DAL.ejecuta_dataset_consulta(conexion, sql, true, ref mensaje_error, ref num_error);
                    if (num_error != 0)
                    {
                        string error = "Ha ocurrido un error al consultar con la BD. Favor contacte al administrador.";
                        HttpContext.Current.Response.Redirect("Errores.aspx?Error" + error, false);
                        return null;
                    }
                    else
                    {
                        return ds;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Agrega un nuevo usuario en la BD.
        /// </summary>
        /// <returns> Retorna un valor bool dependiendo del éxito de la operación.</returns>
        public bool Nuevo()
        {
            try
            {
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref num_error);
                if (conexion == null)
                {
                    string error = "Ha ocurrido un error en la conexión con la BD. Favor contacte al administrador";
                    HttpContext.Current.Response.Redirect("Errores.aspx?Error" + error, false);
                    return false;
                }
                else
                {
                    sql = "RHPLUS_Usuarios_Nuevo";
                    //aquí se genera una estructura de parámetros para enviar a la BD.
                    paramStruct[] parametros = new paramStruct[10];
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 0, "@IdUsuario", SqlDbType.VarChar, _IdUsuario);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 1, "@Nombre", SqlDbType.VarChar, _Nombre);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 2, "@Apellido1", SqlDbType.VarChar, _Apellido1);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 3, "@Apellido2", SqlDbType.VarChar, _Apellido2);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 4, "@Clave", SqlDbType.VarChar,Encriptar_Clave.generateHashDigest(_Clave, Encriptar_Clave.HashMethod.MD5));
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 5, "@Correo", SqlDbType.VarChar, _Correo);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 6, "@Estado", SqlDbType.Bit, _Estado);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 7, "@IdRol", SqlDbType.Int, _IdRol);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 8, "@IdSeccion", SqlDbType.Int, _IdSeccion);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 9, "@IdOrganizacion", SqlDbType.Int, _IdOrganizacion);
                    cls_DAL.conectar(conexion, ref mensaje_error, ref num_error);

                    cls_DAL.ejecuta_sqlcommand_editar_agregar(conexion, sql, true, parametros, ref mensaje_error, ref num_error);
                    if (num_error != 0)
                    {
                        string error = "Ha ocurrido un error al insertar en la BD. Favor contacte al administrador.";
                        HttpContext.Current.Response.Redirect("Errores.aspx?Error" + error, false);
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref num_error);
                        return false;
                    }
                    else
                    {
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref num_error);
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Edita en la BD un usuario existente.
        /// </summary>
        /// <returns></returns>
        public bool Editar_Usuario()
        {
            try
            {
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref num_error);
                if (conexion == null)
                {
                    string error = "Ha ocurrido un error en la conexión con la BD. Favor contacte al administrador";
                    HttpContext.Current.Response.Redirect("Errores.aspx?Error=" + error, false);
                    return false;
                }
                else
                {
                    sql = "RHPLUS_Usuarios_Editar";
                    paramStruct[] parametros = new paramStruct[9];
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 0, "@Id", SqlDbType.Int, _Id);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 1, "@IdUsuario", SqlDbType.VarChar, _IdUsuario);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 2, "@Nombre", SqlDbType.VarChar, _Nombre);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 3, "@Apellido1", SqlDbType.VarChar, _Apellido1);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 4, "@Apellido2", SqlDbType.VarChar, _Apellido2);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 5, "@Correo", SqlDbType.VarChar, _Correo);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 6, "@Estado", SqlDbType.Bit, _Estado);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 7, "@IdRol", SqlDbType.Int, _IdRol);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 8, "@IdSeccion", SqlDbType.Int, _IdSeccion);
                    cls_DAL.conectar(conexion, ref mensaje_error, ref num_error);
                    cls_DAL.ejecuta_sqlcommand_editar_agregar(conexion, sql, true, parametros, ref mensaje_error, ref num_error);
                    if (num_error != 0)
                    {
                        string error = "Ha ocurrido un error al eliminar en la BD. Favor contacte al administrador.";
                        HttpContext.Current.Response.Redirect("Errores.aspx?Error=" + error, false);
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref num_error);
                        return false;
                    }
                    else
                    {
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref num_error);
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Elimina un usuario de la BD
        /// </summary>
        /// <param name="IdUsuario"></param>
        /// <returns></returns>
        public bool Eliminar_Usuario(int IdUsuario)
        {
            try
            {
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref num_error);
                if (conexion == null)
                {
                    string error = "Ha ocurrido un error en la conexión con la BD. Favor contacte al administrador";
                    HttpContext.Current.Response.Redirect("Errores.aspx?Error=" + error, false);
                    return false;
                }
                else
                {
                    sql = "RHPLUS_Usuario_Eliminar";
                    paramStruct[] parametros = new paramStruct[1];
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 0, "@IdUsuario", SqlDbType.Int, IdUsuario);
                    cls_DAL.conectar(conexion, ref mensaje_error, ref num_error);
                    cls_DAL.ejecuta_sqlcommand_editar_agregar(conexion, sql, true, parametros, ref mensaje_error, ref num_error);
                    if (num_error != 0)
                    {
                        string error = "Ha ocurrido un error al eliminar en la BD. Favor contacte al administrador.";
                        HttpContext.Current.Response.Redirect("Errores.aspx?Error=" + error, false);
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref num_error);
                        return false;
                    }
                    else
                    {
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref num_error);
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// realiza la consulta de los usuarios de la organización que está logueada en el sistema.
        /// </summary>
        /// <returns></returns>
        public DataSet Usuarios_Organizacion()
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref num_error);
                cmd = new SqlCommand("RHPLUS_Usuarios_Consulta", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@IdOrganizacion", SqlDbType.Int).Value = _IdOrganizacion;
                //paramStruct[] parametros = new paramStruct[1];
                //cls_DAL.agregar_datos_estructura_parametros(ref parametros, 0, "@IdOrganizacion", SqlDbType.Int, _IdOrganizacion);
                cls_DAL.conectar(conexion, ref mensaje_error, ref num_error);

                da.SelectCommand = cmd;
                da.Fill(ds);
                da.Dispose();
                cmd.Dispose();
                cls_DAL.desconectar(conexion, ref mensaje_error, ref num_error);

                return ds;
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// permite al usuario cambiar su clave.
        /// también se utiliza para el reset de clave en el módulo de superadmin; para el usuario administrador de una organización.
        /// </summary>
        /// <returns></returns>
        public bool Cambio_Clave()
        {
            try
            {
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref num_error);
                if (conexion == null)
                {
                    string error = "Ha ocurrido un error en la conexión con la BD. Favor contacte al administrador";
                    HttpContext.Current.Response.Redirect("~/Errores.aspx?Error=" + error, false);
                    return false;
                }
                else
                {
                    sql = "RHPLUS_Cambio_Clave";
                    paramStruct[] parametros = new paramStruct[2];
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 0, "@IdUsuario", SqlDbType.Int, _Id);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 1, "@Clave", SqlDbType.VarChar, Encriptar_Clave.generateHashDigest(_Clave, Encriptar_Clave.HashMethod.MD5));

                    cls_DAL.conectar(conexion, ref mensaje_error, ref num_error);
                    cls_DAL.ejecuta_sqlcommand_editar_agregar(conexion, sql, true, parametros, ref mensaje_error, ref num_error);

                    if (num_error != 0)
                    {
                        string error = "Error al cambiar la contraseña- Favor contacte al administrador";
                        HttpContext.Current.Response.Redirect("~/Errores.aspx?Error=" + error, false);
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref num_error);
                        return false;
                    }
                    else
                    {
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref num_error);
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }
}
