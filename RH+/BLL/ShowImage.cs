﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Configuration;

namespace BLL
{
    public class ShowImage : IHttpHandler
    {
        /// <summary>
        /// Debe configurar este controlador en el archivo Web.config de su 
        ///  web y registrarlo en IIS para poder usarlo. Para obtener más información
        /// consulte el vínculo siguiente: https://go.microsoft.com/?linkid=8101007
        /// </summary>
        /// 
        #region IHttpHandler Members

        public bool IsReusable
        {
            // Devuelve false si el controlador administrado no se puede reutilizar para otra solicitud.
            // Suele ser false si hay información de estado reservada por solicitud.
            get { return false; }
        }

        public void ProcessRequest(HttpContext context)
        {
            //Escriba aquí la implementación del controlador.
            Int32 empno;
            if (context.Request.QueryString["IdOrganizacion"] != null)
                empno = Convert.ToInt32(context.Request.QueryString["IdOrganizacion"]);
            else
                throw new ArgumentException("No parameter specified");

            context.Response.ContentType = "image/jpeg";
            Stream strm = ShowEmpImage(empno);
            byte[] buffer = new byte[4096];
            int byteSeq = strm.Read(buffer, 0, 4096);

            while (byteSeq > 0)
            {
                context.Response.OutputStream.Write(buffer, 0, byteSeq);
                byteSeq = strm.Read(buffer, 0, 4096);
            }
            //context.Response.BinaryWrite(buffer);
        }

        public Stream ShowEmpImage(int empno)
        {
            string conn = ConfigurationManager.ConnectionStrings["EmployeeConnString"].ConnectionString;
            SqlConnection connection = new SqlConnection(conn);
            string sql = "select Logo from Organizacion where IdOrganizacion = @IdOrganizacion";
            SqlCommand cmd = new SqlCommand(sql, connection);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@IdOrganizacion", empno);
            connection.Open();
            object img = cmd.ExecuteScalar();
            try
            {
                return new MemoryStream((byte[])img);
            }
            catch
            {
                return null;
            }
            finally
            {
                connection.Close();
            }
        }

        #endregion
    }
}
