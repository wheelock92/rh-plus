﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace BLL
{
    public class Colaboradores_L
    {
        /// <summary>
        /// variables globales del modulo de colaboradores
        /// </summary>
        #region propiedades

        private string _Identificacion;
        public string Identificacion
        {
            get { return _Identificacion; }
            set { _Identificacion = value; }
        }

        private string _Nombre;
        public string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }

        private string _Apellido1;
        public string Apellido1
        {
            get { return _Apellido1; }
            set { _Apellido1 = value; }
        }

        private string _Apellido2;
        public string Apellido2
        {
            get { return _Apellido2; }
            set { _Apellido2 = value; }
        }

        private int _Puesto;
        public int Puesto
        {
            get { return _Puesto; }
            set { _Puesto = value; }
        }

        private int _Codigo;
        public int Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        private DateTime _FechaContratacion;
        public DateTime FechaContratacion
        {
            get { return _FechaContratacion; }
            set { _FechaContratacion = value; }
        }

        private DateTime _FechaNacimiento;
        public DateTime FechaNacimiento
        {
            get { return _FechaNacimiento; }
            set { _FechaNacimiento = value; }
        }

        private DateTime _FechaVinculacion;
        public DateTime FechaVinculacion
        {
            get { return _FechaVinculacion; }
            set { _FechaVinculacion = value; }
        }

        private bool _Estado;
        public bool Estado
        {
            get { return _Estado; }
            set { _Estado = value; }
        }

        private int _IdOrganizacion;
        public int IdOrganizacion
        {
            get { return _IdOrganizacion; }
            set { _IdOrganizacion = value; }
        }

        #endregion

        /// <summary>
        /// variables para la comunicacion con la BD
        /// </summary>
        #region Variables privadas

        SqlConnection conexion;
        string mensaje_error;
        int numero_error;
        string sql;
        DataSet ds;

        #endregion

        #region Metodos
        /// <summary>
        /// consulta los colaboradores de la organizacion logueada.
        /// </summary>
        /// <returns></returns>
        public DataSet Consulta_Colaboradores_Organizacion()
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref numero_error);
                cmd = new SqlCommand("RHPLUS_Colaboradores_Consulta", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@IdOrganizacion", SqlDbType.Int).Value = _IdOrganizacion;
                cls_DAL.conectar(conexion, ref mensaje_error, ref numero_error);

                da.SelectCommand = cmd;
                da.Fill(ds);
                da.Dispose();
                cmd.Dispose();
                cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);

                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// inserta un nuevo colaborador en la BD
        /// </summary>
        /// <returns></returns>
        public bool Colaboradores_Agregar()
        {
            try
            {
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref numero_error);
                if (conexion == null)
                {
                    string error = "Error al conectarse con la base de datos. Favor contacte al administrador";
                    HttpContext.Current.Response.Redirect("~/Errores.aspx?Error" + error, false);
                    return false;
                }
                else
                {
                    sql = "RHPLUS_Colaboradores_Nuevo";

                    paramStruct[] parametros = new paramStruct[11];
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 0, "@Identificacion", SqlDbType.VarChar, _Identificacion);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 1, "@Nombre", SqlDbType.VarChar, _Nombre);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 2, "@Apellido1", SqlDbType.VarChar, _Apellido1);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 3, "@Apellido2", SqlDbType.VarChar, _Apellido2);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 4, "@Puesto", SqlDbType.Int, _Puesto);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 5, "@Codigo", SqlDbType.Int, _Codigo);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 6, "@FechaContratacion", SqlDbType.Date, _FechaContratacion);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 7, "@FechaNacimiento", SqlDbType.Date, _FechaNacimiento);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 8, "@FechaVinculacion", SqlDbType.Date, _FechaVinculacion);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 9, "@Estado", SqlDbType.Bit, _Estado);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 10, "@IdOrganizacion", SqlDbType.Int, _IdOrganizacion);

                    cls_DAL.conectar(conexion, ref mensaje_error, ref numero_error);
                    cls_DAL.ejecuta_sqlcommand_editar_agregar(conexion, sql, true, parametros, ref mensaje_error, ref numero_error);

                    if (numero_error != 0)
                    {
                        string error = "Ha ocurrido un error al insertar en la BD. Favor contacte al administrador.";
                        HttpContext.Current.Response.Redirect("Errores.aspx?Error" + error, false);
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return false;
                    }
                    else
                    {
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// edita un colaborador en la BD
        /// </summary>
        /// <returns></returns>
        public bool Colaboradores_Editar()
        {
            try
            {
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref numero_error);

                if (conexion == null)
                {
                    string error = "Ocurrio un error al conectar con la base de datos. Favor contacte al administrador";
                    HttpContext.Current.Response.Redirect("~/Errores.aspx?Error" + error, false);
                    return false;
                }
                else
                {
                    sql = "RHPLUS_Colaboradores_Editar";

                    paramStruct[] parametros = new paramStruct[11];
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 0, "@Identificacion", SqlDbType.VarChar, _Identificacion);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 1, "@Nombre", SqlDbType.VarChar, _Nombre);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 2, "@Apellido1", SqlDbType.VarChar, _Apellido1);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 3, "@Apellido2", SqlDbType.VarChar, _Apellido2);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 4, "@Puesto", SqlDbType.Int, _Puesto);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 5, "@Codigo", SqlDbType.Int, _Codigo);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 6, "@FechaContratacion", SqlDbType.Date, _FechaContratacion);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 7, "@FechaNacimiento", SqlDbType.Date, _FechaNacimiento);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 8, "@FechaVinculacion", SqlDbType.Date, _FechaVinculacion);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 9, "@Estado", SqlDbType.Bit, _Estado);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 10, "@IdOrganizacion", SqlDbType.Int, _IdOrganizacion);

                    cls_DAL.conectar(conexion, ref mensaje_error, ref numero_error);
                    cls_DAL.ejecuta_sqlcommand_editar_agregar(conexion, sql, true, parametros, ref mensaje_error, ref numero_error);

                    if (numero_error != 0)
                    {
                        string error = "Ha ocurrido un error al insertar en la BD. Favor contacte al administrador.";
                        HttpContext.Current.Response.Redirect("Errores.aspx?Error" + error, false);
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return false;
                    }
                    else
                    {
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// elimina un colaborador de la BD
        /// </summary>
        /// <param name="Identificacion">id del colaborador para eliminar unicamente el seleccionado</param>
        /// <returns></returns>
        public bool Colaboradores_Eliminar()
        {
            try
            {
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref numero_error);

                if (conexion == null)
                {
                    string error = "Ha ocurrido un error en la conexión con la BD. Favor contacte al administrador";
                    HttpContext.Current.Response.Redirect("Errores.aspx?Error" + error, false);
                    return false;
                }
                else
                {
                    sql = "RHPLUS_Colaboradores_Eliminar";

                    paramStruct[] parametros = new paramStruct[1];
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 0, "@Identificacion", SqlDbType.VarChar, _Identificacion);
                    cls_DAL.conectar(conexion, ref mensaje_error, ref numero_error);
                    cls_DAL.ejecuta_sqlcommand_editar_agregar(conexion, sql, true, parametros, ref mensaje_error, ref numero_error);

                    if (numero_error != 0)
                    {
                        string error = "Ha ocurrido un error al eliminar en la BD. Favor contacte al administrador.";
                        HttpContext.Current.Response.Redirect("Errores.aspx?Error" + error, false);
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return false;
                    }
                    else
                    {
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Eliminar(string Id)
        {
            try
            {
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref numero_error);

                if (conexion == null)
                {
                    string error = "Ha ocurrido un error en la conexión con la BD. Favor contacte al administrador";
                    HttpContext.Current.Response.Redirect("Errores.aspx?Error" + error, false);
                    return false;
                }
                else
                {
                    sql = "RHPLUS_Colaboradores_Eliminar";

                    paramStruct[] parametros = new paramStruct[1];
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 0, "@Identificacion", SqlDbType.Int, _Identificacion);
                    cls_DAL.conectar(conexion, ref mensaje_error, ref numero_error);
                    cls_DAL.ejecuta_sqlcommand_editar_agregar(conexion, sql, true, parametros, ref mensaje_error, ref numero_error);

                    if (numero_error != 0)
                    {
                        string error = "Ha ocurrido un error al eliminar en la BD. Favor contacte al administrador.";
                        HttpContext.Current.Response.Redirect("Errores.aspx?Error" + error, false);
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return false;
                    }
                    else
                    {
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }
}
