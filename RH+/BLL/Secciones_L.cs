﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using System.Web;

namespace BLL
{
    public class Secciones_L
    {
        #region variables privadas

        private int _IdSeccion;
        public int IdSeccion
        {
            get { return _IdSeccion; }
            set { _IdSeccion = value; }
        }

        private string _Nombre;
        public string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }

        private bool _Activo;
        public bool Activo
        {
            get { return _Activo; }
            set { _Activo = value; }
        }

        private int _IdOrganizacion;
        public int IdOrganizacion
        {
            get { return _IdOrganizacion; }
            set { _IdOrganizacion = value; }
        }

        #endregion

        #region variables Privadas

        SqlConnection conexion;
        string mensaje_error;
        int numero_error;
        string sql;
        DataSet ds;

        #endregion

        #region Metodos

        public DataSet carga_lista_secciones()
        {
            try
            {
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref numero_error);

                if (conexion == null)
                {
                    string error = "Ha ocurrido un error en la conexión con la BD. Favor contacte al administrador";
                    HttpContext.Current.Response.Redirect("Errores.aspx?Error" + error, false);
                    return null;
                }
                else
                {
                    sql = "RHPLUS_Secciones_Consulta";
                    ds = cls_DAL.ejecuta_dataset_consulta(conexion, sql, true, ref mensaje_error, ref numero_error);
                    if (numero_error != 0)
                    {
                        string error = "Ha ocurrido un error al consultar con la BD. Favor contacte al administrador.";
                        HttpContext.Current.Response.Redirect("Errores.aspx?Error" + error, false);
                        return null;
                    }
                    else
                    {
                        return ds;
                    }
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public bool agrega_seccion()
        {
            try
            {
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref numero_error);

                if (conexion == null)
                {
                    string error = "Ha ocurrido un error en la conexión con la BD. Favor contacte al administrador";
                    HttpContext.Current.Response.Redirect("Errores.aspx?Error" + error, false);
                    return false;
                }
                else
                {
                    sql = "RHPLUS_Secciones_Nuevo";

                    paramStruct[] parametros = new paramStruct[3];
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 0, "@Nombre", SqlDbType.VarChar, _Nombre);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 1, "@Activo", SqlDbType.Bit, _Activo);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 2, "@IdOrganizacion", SqlDbType.Int, _IdOrganizacion);

                    cls_DAL.conectar(conexion, ref mensaje_error, ref numero_error);
                    cls_DAL.ejecuta_sqlcommand_editar_agregar(conexion, sql, true, parametros, ref mensaje_error, ref numero_error);

                    if (numero_error != 0)
                    {
                        string error = "Ha ocurrido un error al insertar en la BD. Favor contacte al administrador.";
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        HttpContext.Current.Response.Redirect("Errores.aspx?Error" + error, false);
                        return false;
                    }
                    else
                    {
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Editar_Seccion()
        {
            try
            {
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref numero_error);
                if (conexion == null)
                {
                    string error = "Ha ocurrido un error en la conexión con la BD. Favor contacte al administrador";
                    HttpContext.Current.Response.Redirect("Errores.aspx?Error=" + error, false);
                    return false;
                }
                else
                {
                    sql = "RHPLUS_Editar_Seccion";
                    paramStruct[] parametros = new paramStruct[3];
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 0, "@IdSeccion", SqlDbType.Int, _IdSeccion);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 1, "@Nombre", SqlDbType.VarChar, _Nombre);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 2, "@Activo", SqlDbType.Bit, _Activo);
                    //cls_DAL.agregar_datos_estructura_parametros(ref parametros, 3, "@IdOrganizacion", SqlDbType.Int, _IdOrganizacion);
                    cls_DAL.conectar(conexion, ref mensaje_error, ref numero_error);
                    cls_DAL.ejecuta_sqlcommand_editar_agregar(conexion, sql, true, parametros, ref mensaje_error, ref numero_error);
                    if (numero_error != 0)
                    {
                        string error = "Ha ocurrido un error al editar en la BD. Favor contacte al administrador.";
                        HttpContext.Current.Response.Redirect("Errores.aspx?Error=" + error, false);
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return false;
                    }
                    else
                    {
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return true;
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Eliminar_Seccion(int IdSeccion)
        {
            try
            {
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref numero_error);
                if (conexion == null)
                {
                    string error = "Ha ocurrido un error en la conexión con la BD. Favor contacte al administrador";
                    HttpContext.Current.Response.Redirect("Errores.aspx?Error=" + error, false);
                    return false;
                }
                else
                {
                    sql = "RHPLUS_Seccion_Eliminar";
                    paramStruct[] parametros = new paramStruct[1];
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 0, "@IdSeccion", SqlDbType.Int, IdSeccion);
                    cls_DAL.conectar(conexion, ref mensaje_error, ref numero_error);
                    cls_DAL.ejecuta_sqlcommand_editar_agregar(conexion, sql, true, parametros, ref mensaje_error, ref numero_error);
                    if (numero_error != 0)
                    {
                        string error = "Ha ocurrido un error al eliminar en la BD. Favor contacte al administrador.";
                        HttpContext.Current.Response.Redirect("Errores.aspx?Error=" + error, false);
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return false;
                    }
                    else
                    {
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataSet Carga_Secciones_Organizacion()
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref numero_error);
                cmd = new SqlCommand("RHPLUS_Secciones_Consulta", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@IdOrganizacion", SqlDbType.Int).Value = _IdOrganizacion;
                //paramStruct[] parametros = new paramStruct[1];
                //cls_DAL.agregar_datos_estructura_parametros(ref parametros, 0, "@IdOrganizacion", SqlDbType.Int, _IdOrganizacion);
                cls_DAL.conectar(conexion, ref mensaje_error, ref numero_error);

                da.SelectCommand = cmd;
                da.Fill(ds);
                da.Dispose();
                cmd.Dispose();
                cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);

                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }
}