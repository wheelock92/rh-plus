﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using System.Web;

namespace BLL
{
    public class GrupoEmpresarial_L
    {
        #region Propiedades

        private int _IdGrupo;
        public int IdGrupo
        {
            get { return _IdGrupo; }
            set { _IdGrupo = value; }
        }

        private string _Nombre;
        public string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }

        private string _Descripcion;
        public string Descripcion
        {
            get { return _Descripcion; }
            set { _Descripcion = value; }
        }

        private bool _Activo;
        public bool Activo
        {
            get { return _Activo; }
            set { _Activo = value; }
        }

        #endregion

        #region Variables privadas

        SqlConnection conexion;
        string mensaje_error;
        int numero_error;
        string sql;
        DataSet ds;

        #endregion

        #region Metodos

        public DataSet Carga_Lista_Grupos()
        {
            try
            {
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref numero_error);
                if (conexion == null)
                {
                    string error = "Ha ocurrido un error en la conexión con la BD. Favor contacte al administrador";
                    HttpContext.Current.Response.Redirect("`/ErroresSU.aspx?Error" + error, false);
                    return null;
                }
                else
                {
                    sql = "RHPLUS_GrupoEmpresarial_Consulta";
                    ds = cls_DAL.ejecuta_dataset_consulta(conexion, sql, true, ref mensaje_error, ref numero_error);

                    if (numero_error != 0)
                    {
                        string error = "Ha ocurrido un error al consultar con la BD. Favor contacte al administrador.";
                        HttpContext.Current.Response.Redirect("~/ErroresSU.aspx?Error" + error, false);
                        return null;
                    }
                    else
                    {
                        return ds;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Agregar_Grupo()
        {
            try
            {
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref numero_error);

                if (conexion == null)
                {
                    string error = "Ha ocurrido un error en la conexión con la BD. Favor contacte al administrador";
                    HttpContext.Current.Response.Redirect("~/ErroresSU.aspx?Error" + error, false);
                    return false;
                }
                else
                {
                    sql = "RHPLUS_GrupoEmpresarial_Nuevo";
                    paramStruct[] parametros = new paramStruct[3];
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 0, "@Nombre", SqlDbType.VarChar, _Nombre);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 1, "@Descripcion", SqlDbType.VarChar, _Descripcion);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 2, "@Activo", SqlDbType.Bit, _Activo);
                    cls_DAL.conectar(conexion, ref mensaje_error, ref numero_error);
                    cls_DAL.ejecuta_sqlcommand_editar_agregar(conexion, sql, true, parametros, ref mensaje_error, ref numero_error);

                    if (numero_error != 0)
                    {
                        string error = "Ha ocurrido un error al insertar en la BD. Favor contacte al administrador.";
                        HttpContext.Current.Response.Redirect("~/ErroresSU.aspx?Error" + error, false);
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return false;
                    }
                    else
                    {
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Editar_Grupo()
        {
            try
            {
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref numero_error);
                if (conexion == null)
                {
                    string error = "Ha ocurrido un error en la conexión con la BD. Favor contacte al administrador";
                    HttpContext.Current.Response.Redirect("~/ErroresSU.aspx?Error=" + error, false);
                    return false;
                }
                else
                {
                    sql = "RHPLUS_GrupoEmpresarial_Editar";
                    paramStruct[] parametros = new paramStruct[4];
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 0, "@IdGrupo", SqlDbType.Int, _IdGrupo);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 1, "@Nombre", SqlDbType.VarChar, _Nombre);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 2, "@Descripcion", SqlDbType.VarChar, _Descripcion);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 3, "@Activo", SqlDbType.Bit, _Activo);
                    cls_DAL.conectar(conexion, ref mensaje_error, ref numero_error);
                    cls_DAL.ejecuta_sqlcommand_editar_agregar(conexion, sql, true, parametros, ref mensaje_error, ref numero_error);
                    if (numero_error != 0)
                    {
                        string error = "Ha ocurrido un error al editar en la BD. Favor contacte al administrador.";
                        HttpContext.Current.Response.Redirect("~/ErroresSU.aspx?Error=" + error, false);
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return false;
                    }
                    else
                    {
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Eliminar_Grupo(int IdGrupo)
        {
            try
            {
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref numero_error);
                if (conexion == null)
                {
                    string error = "Ha ocurrido un error en la conexión con la BD. Favor contacte al administrador";
                    HttpContext.Current.Response.Redirect("~/ErroresSU.aspx?Error" + error, false);
                    return false;
                }
                else
                {
                    sql = "RHPLUS_GrupoEmpresarial_Eliminar";
                    paramStruct[] parametros = new paramStruct[1];
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 0, "@IdGrupo", SqlDbType.Int, IdGrupo);
                    cls_DAL.conectar(conexion, ref mensaje_error, ref numero_error);
                    cls_DAL.ejecuta_sqlcommand_editar_agregar(conexion, sql, true, parametros, ref mensaje_error, ref numero_error);
                    if (numero_error != 0)
                    {
                        string error = "Ha ocurrido un error al eliminar en la BD. Favor contacte al administrador.";
                        HttpContext.Current.Response.Redirect("~/ErroresSU.aspx?Error" + error, false);
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return false;
                    }
                    else
                    {
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        #endregion
    }
}
