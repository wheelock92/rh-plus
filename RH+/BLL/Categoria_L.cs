﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using System.Web;

namespace BLL
{
    public class Categoria_L
    {
        #region propiedades

        private int _IdCategoria;
        public int IdCategoria
        {
            get { return _IdCategoria; }
            set { _IdCategoria = value; }
        }

        private string _Nombre;
        public string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }

        private int _IdOrganizacion;
        public int IdOrganizacion
        {
            get { return _IdOrganizacion; }
            set { _IdOrganizacion = value; }
        }

        private bool _Activa;
        public bool Activa
        {
            get { return _Activa; }
            set { _Activa = value; }
        }

        #endregion

        #region Variables privadas

        SqlConnection conexion;
        string mensaje_error;
        int numero_error;
        string sql;
        DataSet ds;

        #endregion

        #region Metodos

        /// <summary>
        /// carga lista general de categorías
        /// </summary>
        /// <returns></returns>
        public DataSet Carga_Lista_Categorias()
        {
            try
            {
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref numero_error);

                if (conexion == null)
                {
                    string error = "Ha ocurrido un error en la conexión con la BD. Favor contacte al administrador";
                    HttpContext.Current.Response.Redirect("~/Errores.aspx?Error" + error, false);
                    return null;
                }
                else
                {
                    sql = "RHPLUS_Categoria_Consulta";
                    ds = cls_DAL.ejecuta_dataset_consulta(conexion, sql, true, ref mensaje_error, ref numero_error);

                    if (numero_error != 0)
                    {
                        string error = "Ha ocurrido un error al consultar con la BD. Favor contacte al administrador.";
                        HttpContext.Current.Response.Redirect("~/Errores.aspx?Error" + error, false);
                        return null;
                    }
                    else
                    {
                        return ds;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// agrega una nueva categoria de la organizacion logueada
        /// </summary>
        /// <returns></returns>
        public bool Agregar_Categoria()
        {
            try
            {
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref numero_error);

                if (conexion == null)
                {
                    string error = "Ha ocurrido un error en la conexión con la BD. Favor contacte al administrador";
                    HttpContext.Current.Response.Redirect("~/Errores.aspx?Error" + error, false);
                    return false;
                }
                else
                {
                    sql = "RHPLUS_Categoria_Nuevo";
                    paramStruct[] parametros = new paramStruct[3];
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 0, "@Nombre", SqlDbType.VarChar, _Nombre);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 1, "@IdOrganizacion", SqlDbType.Int, _IdOrganizacion);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 2, "@Activo", SqlDbType.Bit, _Activa);

                    cls_DAL.conectar(conexion, ref mensaje_error, ref numero_error);
                    cls_DAL.ejecuta_sqlcommand_editar_agregar(conexion, sql, true, parametros, ref mensaje_error, ref numero_error);

                    if (numero_error != 0)
                    {
                        string error = "Ha ocurrido un error al insertar en la BD. Favor contacte al administrador.";
                        HttpContext.Current.Response.Redirect("~/Errores.aspx?Error" + error, false);
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return false;
                    }
                    else
                    {
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        /// <summary>
        /// edita una categoria de la organizacion logueda
        /// </summary>
        /// <returns></returns>
        public bool Editar_Categoria()
        {
            try
            {
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref numero_error);
                if (conexion == null)
                {
                    string error = "Ha ocurrido un error en la conexión con la BD. Favor contacte al administrador";
                    HttpContext.Current.Response.Redirect("~/Errores.aspx=Error" + error, false);
                    return false;
                }
                else
                {
                    sql = "RHPLUS_Categoria_Editar";
                    paramStruct[] parametros = new paramStruct[4];
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 0, "@IdCategoria", SqlDbType.Int, _IdCategoria);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 1, "@Nombre", SqlDbType.VarChar, _Nombre);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 2, "@IdOrganizacion", SqlDbType.Int, _IdOrganizacion);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 3, "@Activo", SqlDbType.Bit, _Activa);
                    cls_DAL.conectar(conexion, ref mensaje_error, ref numero_error);
                    cls_DAL.ejecuta_sqlcommand_editar_agregar(conexion, sql, true, parametros, ref mensaje_error, ref numero_error);
                    if (numero_error != 0)
                    {
                        string error = "Ha ocurrido un error al editar en la BD. Favor contacte al administrador.";
                        HttpContext.Current.Response.Redirect("~/Errores.aspx?Error" + error, false);
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return false;
                    }
                    else
                    {
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        /// <summary>
        /// elimina una categoría de la organización logueada
        /// </summary>
        /// <param name="IdCategoria"></param>
        /// <returns></returns>
        public bool Eliminar_Categoria(int IdCategoria)
        {
            try
            {
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref numero_error);
                if (conexion == null)
                {
                    string error = "Ha ocurrido un error en la conexión con la BD. Favor contacte al administrador";
                    HttpContext.Current.Response.Redirect("~/Errores.aspx?Error" + error, false);
                    return false;
                }
                else
                {
                    sql = "RHPLUS_Categoria_Eliminar";
                    paramStruct[] parametros = new paramStruct[1];
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 0, "@IdCategoria", SqlDbType.Int, IdCategoria);
                    cls_DAL.conectar(conexion, ref mensaje_error, ref numero_error);
                    cls_DAL.ejecuta_sqlcommand_editar_agregar(conexion, sql, true, parametros, ref mensaje_error, ref numero_error);
                    if (numero_error != 0)
                    {
                        string error = "Ha ocurrido un error al eliminar en la BD. Favor contacte al administrador.";
                        HttpContext.Current.Response.Redirect("Errores.aspx?Error" + error, false);
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return false;
                    }
                    else
                    {
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        /// <summary>
        /// carga las categorías de la organización logueada.
        /// </summary>
        /// <returns></returns>
        public DataSet Categoria_Organizacion()
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref numero_error);
                cmd = new SqlCommand("RHPLUS_Categoria_Consulta", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@IdOrganizacion", SqlDbType.Int).Value = _IdOrganizacion;
                cls_DAL.conectar(conexion, ref mensaje_error, ref numero_error);

                da.SelectCommand = cmd;
                da.Fill(ds);
                da.Dispose();
                cmd.Dispose();
                cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);

                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }
}
