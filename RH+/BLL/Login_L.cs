﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Login_L
    {
        #region propiedades

        private string _IdUsuario;
        public string IdUsuario
        {
            get { return _IdUsuario; }
            set { _IdUsuario = value; }
        }

        private string _Clave;
        public string Clave
        {
            get { return _Clave; }
            set { _Clave = value; }
        }

        private int _IdRol;
        public int IdRol
        {
            get { return _IdRol; }
            set { _IdRol = value; }
        }

        #endregion

        #region variables pivadas

        SqlConnection conexion;
        string mensaje_error;
        int numero_error;
        string sql;
        DataSet ds;
        DataTable dt;

        #endregion

        #region Metodos

        public DataTable Ingresar(string IdUsuario, string Clave)
        {
            try
            {
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref numero_error);
                if (conexion == null)
                {
                    return null;
                }
                else
                {
                    sql = "RHPLUS_Login";
                    cls_DAL.conectar(conexion, ref mensaje_error, ref numero_error);

                    dt = cls_DAL.Login(conexion, sql, true, IdUsuario, Encriptar_Clave.generateHashDigest(Clave, Encriptar_Clave.HashMethod.MD5));

                    if (dt.Rows.Count >= 0)
                    {
                        //if (dt.Rows[0]["IdUsuario"].ToString() == IdUsuario && dt.Rows[0]["Clave"].ToString() == Encriptar_Clave.generateHashDigest(Clave, Encriptar_Clave.HashMethod.MD5))
                        //{
                        //    cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        //    return null;
                        //}
                        //else
                        //{
                        //    cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        //    return dt;
                        //}
                        return dt;
                    }
                    else
                    {
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return null;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool SuperAdmin()
        {
            try
            {
                if (_IdRol == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public bool Admin()
        {
            try
            {
                if (_IdRol == 2)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool Editor()
        {
            try
            {
                if (_IdRol == 3)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool Lector()
        {
            try
            {
                if (_IdRol == 4)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion
    }
}
