﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using System.Data.SqlClient;
using System.Data;
using System.Web;

namespace BLL
{
    public class PerfilesPuesto_L
    {
        /// <summary>
        /// variables globales de Perfiles de Puesto
        /// </summary>
        #region Variables privadas

        //estas variables son de la tabla PerfilPuesto
        private int _IdPerfil;
        public int IdPerfil
        {
            get { return _IdPerfil; }
            set { _IdPerfil = value; }
        }

        private string _Codigo;
        public string Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        private int _Version;
        public int Version
        {
            get { return _Version; }
            set { _Version = value; }
        }

        private string _Titulo;
        public string Titulo
        {
            get { return _Titulo; }
            set { _Titulo = value; }
        }

        private string _Descripcion;
        public string Descripcion
        {
            get { return _Descripcion; }
            set { _Descripcion = value; }
        }

        private int _Administrador;
        public int Administrador
        {
            get { return _Administrador; }
            set { _Administrador = value; }
        }

        private int _Responsable;
        public int Responsable
        {
            get { return _Responsable; }
            set { _Responsable = value; }
        }

        private DateTime _FechaVigencia;
        public DateTime FechaVigencia
        {
            get { return _FechaVigencia; }
            set { _FechaVigencia = value; }
        }

        private int _Seccion;
        public int Seccion
        {
            get { return _Seccion; }
            set { _Seccion = value; }
        }

        private int _SubCategoria;
        public int SubCategoria
        {
            get { return _SubCategoria; }
            set { _SubCategoria = value; }
        }

        private int _IdArchivo;
        public int IdArchivo
        {
            get { return _IdArchivo; }
            set { _IdArchivo = value; }
        }

        private int _IdEstado;
        public int IdEstado
        {
            get { return _IdEstado; }
            set { _IdEstado = value; }
        }

        private int _IdCategoria;
        public int IdCategoria
        {
            get { return _IdCategoria; }
            set { _IdCategoria = value; }
        }

        //a partir de aqui las variables son de la tabla Archivo
        private byte[] _Archivo;
        public byte[] Archivo
        {
            get { return _Archivo; }
            set { _Archivo = value; }
        }

        private string _Nombre;
        public string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }

        private string _Extension;
        public string Extension
        {
            get { return _Extension; }
            set { _Extension = value; }
        }

        //Variable de IdOrganizacion
        private int _IdOrganizacion;
        public int IdOrganizacion
        {
            get { return _IdOrganizacion; }
            set { _IdOrganizacion = value; }
        }

        #endregion

        #region Variables privadas

        SqlConnection conexion;
        string mensaje_error;
        int numero_error;
        string sql;
        DataSet ds;

        #endregion

        #region Metodos

        /// <summary>
        /// carga la lista de todos los perfiles que están en el sistema
        /// </summary>
        /// <returns></returns>
        public DataSet Carga_Lista_Perfiles()
        {
            try
            {
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref numero_error);
                if (conexion == null)
                {
                    string error = "Ha ocurrido un error en la conexión con la BD. Favor contacte al administrador";
                    HttpContext.Current.Response.Redirect("Errores.aspx?Error" + error, false);
                    return null;
                }
                else
                {
                    sql = "RHPLUS_PerfilesPuesto_Consulta";

                    ds = cls_DAL.ejecuta_dataset_consulta(conexion, sql, true, ref mensaje_error, ref numero_error);

                    if (numero_error != 0)
                    {
                        string error = "Ha ocurrido un error al consultar con la BD. Favor contacte al administrador.";
                        HttpContext.Current.Response.Redirect("Errores.aspx?Error" + error, false);
                        return null;
                    }
                    else
                    {
                        return ds;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// agrega un nuevo perfil de la organización logueada
        /// </summary>
        /// <returns></returns>
        public bool Agregar_Perfil()
        {
            try
            {
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref numero_error);
                if (conexion == null)
                {
                    string error = "Ha ocurrido un error en la conexión con la BD. Favor contacte al administrador";
                    HttpContext.Current.Response.Redirect("Errores.aspx?Error" + error, false);
                    return false;
                }
                else
                {
                    sql = "RHPLUS_PerfilesPuesto_Nuevo";
                    paramStruct[] parametros = new paramStruct[15];
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 0, "@Codigo", SqlDbType.VarChar, _Codigo);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 1, "@Version", SqlDbType.Int, _Version);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 2, "@Titulo", SqlDbType.VarChar, _Titulo);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 3, "@Descripcion", SqlDbType.VarChar, _Descripcion);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 4, "Administrador", SqlDbType.Int, _Administrador);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 5, "@Responsable", SqlDbType.Int, _Responsable);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 6, "@FechaVigencia", SqlDbType.Date, _FechaVigencia);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 7, "@Seccion", SqlDbType.Int, _Seccion);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 8, "@SubCategoria", SqlDbType.Int, _SubCategoria);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 9, "@IdArchivo", SqlDbType.Int, _IdArchivo);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 10, "@IdEstado", SqlDbType.Int, _IdEstado);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 11, "@Archivo", SqlDbType.Image, _Archivo);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 12, "@Nombre", SqlDbType.VarChar, _Nombre);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 13, "@Extension", SqlDbType.VarChar, _Extension);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 14, "@IdCategoria", SqlDbType.Int, _IdCategoria);

                    cls_DAL.conectar(conexion, ref mensaje_error, ref numero_error);
                    cls_DAL.ejecuta_sqlcommand_editar_agregar_imagen(conexion, sql, true, parametros, ref mensaje_error, ref numero_error);

                    if (numero_error != 0)
                    {
                        string error = "Ha ocurrido un error al insertar en la BD. Favor contacte al administrador.";
                        HttpContext.Current.Response.Redirect("Errores.aspx?Error" + error, false);
                        return false;
                    }
                    else
                    {
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// cambia el estado del perfil a aprobado
        /// </summary>
        /// <returns></returns>
        public bool Aprobar_Perfil()
        {
            try
            {
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref numero_error);
                if (conexion == null)
                {
                    string error = "Ha ocurrido un error en la conexión con la BD. Favor contacte al administrador";
                    HttpContext.Current.Response.Redirect("Errores.aspx?Error" + error, false);
                    return false;
                }
                else
                {
                    sql = "RHPLUS_PerfilesPuesto_Aprobar";
                    paramStruct[] parametros = new paramStruct[1];
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 0, "@IdPerfilPuesto", SqlDbType.Int, _IdPerfil);
                    cls_DAL.conectar(conexion, ref mensaje_error, ref numero_error);
                    cls_DAL.ejecuta_sqlcommand_editar_agregar(conexion, sql, true, parametros, ref mensaje_error, ref numero_error);
                    if (numero_error != 0)
                    {
                        string error = "Ha ocurrido un error al cambiar el estado en la BD. Favor contacte al administrador.";
                        HttpContext.Current.Response.Redirect("Errores.aspx?Error" + error, false);
                        return false;
                    }
                    else
                    {
                        cls_DAL.desconectar(conexion,ref mensaje_error,ref numero_error);
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// cambia el estado del perfil a obsoleto
        /// </summary>
        /// <returns></returns>
        public bool Obsoleto_Perfil()
        {
            try
            {
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref numero_error);
                if (conexion == null)
                {
                    string error = "Ha ocurrido un error en la conexión con la BD. Favor contacte al administrador";
                    HttpContext.Current.Response.Redirect("Errores.aspx?Error" + error, false);
                    return false;
                }
                else
                {
                    sql = "RHPLUS_PerfilesPuesto_Obsoleto";
                    paramStruct[] parametros = new paramStruct[1];
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 0, "@IdPerfilPuesto", SqlDbType.Int, _IdPerfil);
                    cls_DAL.conectar(conexion, ref mensaje_error, ref numero_error);
                    cls_DAL.ejecuta_sqlcommand_editar_agregar(conexion, sql, true, parametros, ref mensaje_error, ref numero_error);
                    if (numero_error != 0)
                    {
                        string error = "Ha ocurrido un error al cambiar el estado en la BD. Favor contacte al administrador.";
                        HttpContext.Current.Response.Redirect("Errores.aspx?Error" + error, false);
                        return false;
                    }
                    else
                    {
                        cls_DAL.desconectar(conexion,ref mensaje_error,ref numero_error);
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// modifica el perfil seleccionado
        /// </summary>
        /// <returns></returns>
        public bool Editar_Perfil()
        {
            try
            {
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref numero_error);
                if (conexion == null)
                {
                    string error = "Ha ocurrido un error en la conexión con la BD. Favor contacte al administrador";
                    HttpContext.Current.Response.Redirect("Errores.aspx?Error=" + error, false);
                    return false;
                }
                else
                {
                    sql = "RHPLUS_PerfilPuesto_Editar";
                    paramStruct[] parametros = new paramStruct[11];
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 0, "@IdPerfil", SqlDbType.Int, _IdPerfil);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 1, "@Codigo", SqlDbType.VarChar, _Codigo);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 2, "@Version", SqlDbType.Int, _Version);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 3, "@Nombre", SqlDbType.VarChar, _Titulo);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 4, "@Descripcion", SqlDbType.VarChar, _Descripcion);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 5, "@Administrador", SqlDbType.Int, _Administrador);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 6, "@Responsable", SqlDbType.Int, _Responsable);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 7, "@FechaVigencia", SqlDbType.Date, _FechaVigencia);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 8, "@Seccion", SqlDbType.Int, _Seccion);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 9, "@SubCategoria", SqlDbType.Int, _SubCategoria);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 10, "@IdCategoria", SqlDbType.Int, _IdCategoria);
                    cls_DAL.conectar(conexion, ref mensaje_error, ref numero_error);
                    cls_DAL.ejecuta_sqlcommand_editar_agregar(conexion, sql, true, parametros, ref mensaje_error, ref numero_error);

                    if (numero_error != 0)
                    {
                        string error = "Ha ocurrido un error al editar en la BD. Favor contacte al administrador.";
                        HttpContext.Current.Response.Redirect("Errores.aspx?Error=" + error, false);
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return false;
                    }
                    else
                    {
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// elimina el perfil seleccionado
        /// </summary>
        /// <param name="IdPerfil"></param>
        /// <returns></returns>
        public bool Eliminar_Perfil(int IdPerfil)
        {
            try
            {
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref numero_error);
                if (conexion == null)
                {
                    string error = "Ha ocurrido un error en la conexión con la BD. Favor contacte al administrador";
                    HttpContext.Current.Response.Redirect("Errores.aspx?Error=" + error, false);
                    return false;
                }
                else
                {
                    sql = "RHPLUS_Perfiles_Elimina";
                    paramStruct[] parametros = new paramStruct[1];
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 0, "@IdPerfil", SqlDbType.Int, IdPerfil);
                    cls_DAL.conectar(conexion, ref mensaje_error, ref numero_error);
                    cls_DAL.ejecuta_sqlcommand_editar_agregar(conexion, sql, true, parametros, ref mensaje_error, ref numero_error);
                    if (numero_error != 0)
                    {
                        string error = "Ha ocurrido un error al eliminar en la BD. Favor contacte al administrador.";
                        HttpContext.Current.Response.Redirect("Errores.aspx?Error=" + error, false);
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return false;
                    }
                    else
                    {
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// carga los perfiles de la organización logueda.
        /// </summary>
        /// <returns></returns>
        public DataSet PerfilesPuesto_Organizacion()
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref numero_error);
                cmd = new SqlCommand("RHPLUS_PerfilesPuesto_Consulta", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@IdOrganizacion", SqlDbType.Int).Value = _IdOrganizacion;
                cls_DAL.conectar(conexion, ref mensaje_error, ref numero_error);

                da.SelectCommand = cmd;
                da.Fill(ds);
                da.Dispose();
                cmd.Dispose();
                cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);

                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }
}
