﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using System.Web;

namespace BLL
{
    public class Correo_L
    {
        #region propiedades

        private int _IdCorreo;
        public int IdCorreo
        {
            get { return _IdCorreo; }
            set { _IdCorreo = value; }
        }

        private string _ServidorSMTP;
        public string ServidorSMTP
        {
            get { return _ServidorSMTP; }
            set { _ServidorSMTP = value; }
        }

        private string _CorreoNotificaciones;
        public string CorreoNotificaciones
        {
            get { return _CorreoNotificaciones; }
            set { _CorreoNotificaciones = value; }
        }

        private string _ContrasenaCorreo;
        public string ContrasenaCorreo
        {
            get { return _ContrasenaCorreo; }
            set { _ContrasenaCorreo = value; }
        }

        private bool _SSL;
        public bool SSL
        {
            get { return _SSL; }
            set { _SSL = value; }
        }

        private int _Puerto;
        public int Puerto
        {
            get { return _Puerto; }
            set { _Puerto = value; }
        }

        private string _Observaciones;
        public string Observaciones
        {
            get { return _Observaciones; }
            set { _Observaciones = value; }
        }

        private int _IdOrganizacion;
        public int IdOrganizacion
        {
            get { return _IdOrganizacion; }
            set { _IdOrganizacion = value; }
        }

        private string _POP3;
        public string POP3
        {
            get { return _POP3; }
            set { _POP3 = value; }
        }

        private bool _Autenticacion;
        public bool Autenticacion
        {
            get { return _Autenticacion; }
            set { _Autenticacion = value; }
        }

        private string _Usuario;
        public string Usuario
        {
            get { return _Usuario; }
            set { _Usuario = value; }
        }

        #endregion

        #region variables Privadas

        SqlConnection conexion;
        string mensaje_error;
        int numero_error;
        string sql;
        DataSet ds;

        #endregion

        #region Metodos

        public DataSet Carga_Lista_Correo()
        {
            try
            {
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref numero_error);
                if (conexion == null)
                {
                    string error = "Ha ocurrido un error en la conexión con la BD. Favor contacte al administrador";
                    HttpContext.Current.Response.Redirect("~/Errores.aspx?Error" + error, false);
                    return null;
                }
                else
                {
                    sql = "RHPLUS_Correo_Consulta";
                    ds = cls_DAL.ejecuta_dataset_consulta(conexion, sql, true, ref mensaje_error, ref numero_error);

                    if (numero_error != 0)
                    {
                        string error = "Ha ocurrido un error al consultar con la BD. Favor contacte al administrador.";
                        HttpContext.Current.Response.Redirect("Errores.aspx?Error" + error, false);
                        return null;
                    }
                    else
                    {
                        return ds;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Nuevo_Correo()
        {
            try
            {
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref numero_error);

                if (conexion  == null)
                {
                    string error = "Ha ocurrido un error en la conexión con la BD. Favor contacte al administrador";
                    HttpContext.Current.Response.Redirect("~/Errores.aspx?Error" + error, false);
                    return false;
                }
                else
                {
                    sql = "RHPLUS_Correo_Nuevo";
                    paramStruct[] parametros = new paramStruct[10];

                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 0, "@ServidorSMTP", SqlDbType.VarChar, _ServidorSMTP);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 1, "@CorreoNotificaciones", SqlDbType.VarChar, _CorreoNotificaciones);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 2, "@ContrasenaCorreo", SqlDbType.VarChar, _ContrasenaCorreo);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 3, "@SSL", SqlDbType.Bit, _SSL);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 4, "@Puerto", SqlDbType.Int, _Puerto);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 5, "@Observaciones", SqlDbType.VarChar, _Observaciones);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 6, "@IdOrganizacion", SqlDbType.Int, _IdOrganizacion);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 7, "@POP3", SqlDbType.VarChar, _POP3);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 8, "@Autenticacion", SqlDbType.Bit, _Autenticacion);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 9, "@Usuario", SqlDbType.VarChar, _Usuario);

                    cls_DAL.conectar(conexion, ref mensaje_error, ref numero_error);
                    cls_DAL.ejecuta_sqlcommand_editar_agregar(conexion, sql, true, parametros, ref mensaje_error, ref numero_error);

                    if (numero_error != 0)
                    {
                        string error = "Ha ocurrido un error al insertar en la BD. Favor contacte al administrador.";
                        HttpContext.Current.Response.Redirect("~/Errores.aspx?Error" + error, false);
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return false;
                    }
                    else
                    {
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Editar_Correo()
        {
            try
            {
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref  numero_error);
                if (conexion == null)
                {
                    string error = "Ha ocurrido un error en la conexión con la BD. Favor contacte al administrador";
                    HttpContext.Current.Response.Redirect("~/Errores.aspx?Error" + error, false);
                    return false;
                }
                else
                {
                    sql = "RHPLUS_Correo_Editar";
                    paramStruct[] parametros = new paramStruct[11];
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 0, "@IdCorreo", SqlDbType.Int, _IdCorreo);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 1, "@ServidorSMTP", SqlDbType.VarChar, _ServidorSMTP);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 2, "@CorreoNotificaciones", SqlDbType.VarChar, _CorreoNotificaciones);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 3, "@ContrasenaCorreo", SqlDbType.VarChar, _ContrasenaCorreo);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 4, "@SSL", SqlDbType.VarChar, _SSL);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 5, "@Puerto", SqlDbType.Int, _Puerto);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 6, "@Observaciones", SqlDbType.VarChar, _Observaciones);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 7, "@IdOrganizacion", SqlDbType.Int, _IdOrganizacion);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 8, "@POP3", SqlDbType.VarChar, _POP3);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 9, "@Autenticacion", SqlDbType.Bit, _Autenticacion);
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 10, "@Usuario", SqlDbType.VarChar, _Usuario);
                    cls_DAL.conectar(conexion, ref mensaje_error, ref numero_error);
                    cls_DAL.ejecuta_sqlcommand_editar_agregar(conexion, sql, true, parametros, ref mensaje_error, ref numero_error);
                    if (numero_error != 0)
                    {
                        string error = "Ha ocurrido un error al editar en la BD. Favor contacte al administrador.";
                        HttpContext.Current.Response.Redirect("~/Errores.aspx?Error" + error, false);
                        return false;
                    }
                    else
                    {
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public bool Elimina_Correo(int IdCorreo)
        {
            try
            {
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref numero_error);
                if (conexion == null)
                {
                    string error = "Ha ocurrido un error en la conexión con la BD. Favor contacte al administrador";
                    HttpContext.Current.Response.Redirect("~/Errores.aspx?Error" + error, false);
                    return false;
                }
                else
                {
                    sql = "RHPLUS_Correo_Eliminar";
                    paramStruct[] parametros = new paramStruct[1];
                    cls_DAL.agregar_datos_estructura_parametros(ref parametros, 0, "@IdCorreo", SqlDbType.Int, IdCorreo);
                    cls_DAL.conectar(conexion, ref mensaje_error, ref numero_error);
                    cls_DAL.ejecuta_sqlcommand_editar_agregar(conexion, sql, true, parametros, ref mensaje_error, ref numero_error);

                    if (numero_error != 0)
                    {
                        string error = "Ha ocurrido un error al eliminar en la BD. Favor contacte al administrador.";
                        HttpContext.Current.Response.Redirect("~/Errores.aspx?Error" + error, false);
                        return false;
                    }
                    else
                    {
                        cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataSet Correo_Organizacion()
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                conexion = cls_DAL.trae_conexion("RH+", ref mensaje_error, ref numero_error);
                cmd = new SqlCommand("RHPLUS_Correo_Consulta", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@IdOrganizacion", SqlDbType.Int).Value = _IdOrganizacion;
                cls_DAL.conectar(conexion, ref mensaje_error, ref numero_error);

                da.SelectCommand = cmd;
                da.Fill(ds);
                da.Dispose();
                cmd.Dispose();
                cls_DAL.desconectar(conexion, ref mensaje_error, ref numero_error);

                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }
}
