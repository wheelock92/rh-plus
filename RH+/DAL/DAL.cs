﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace DAL
{
    public class DAL
    {
    }

    public struct paramStruct
    {
        /// <summary>
        /// Variables para enviar datos a BD.
        /// </summary>
        public string Nombre_Parametro;
        public SqlDbType Tipo_Dato;
        public Object Valor_Parametro;
    }

    public static class cls_DAL
    {
        public static SqlConnection trae_conexion(string nombre_Conexion, ref string mensaje_error, ref int num_error)
        {
            SqlConnection cnn;
            try 
	        {	        
		        string cadena_conexion = string.Empty;

                cadena_conexion = ConfigurationManager.ConnectionStrings[nombre_Conexion].ToString();

                cnn = new SqlConnection(cadena_conexion);
                mensaje_error = string.Empty;
                num_error = 0;
                return cnn;
	        }
	        catch (NullReferenceException ex)
	        {
		        mensaje_error = "No se encontro la cadena de conexion: " + nombre_Conexion + ". Informacion adicional: " + ex.Message;
                num_error = -1;
                return null;
	        }
            catch (ConfigurationException ex)
            {
                mensaje_error = "Error. Informacion adicional: " + ex.Message;
                num_error = -2;
                return null;
            }
        }

        public static void conectar(SqlConnection conexion, ref string mensaje_error, ref int num_error)
        {
            try 
	        {	        
		        conexion.Open();
                mensaje_error = "ok";
                num_error = 0;
	        }
	        catch (Exception)
	        {
        		throw;
	        }
        }

        public static void desconectar(SqlConnection conexion, ref string mensaje_error, ref int num_error)
        {
            try 
	        {	        
		        if (conexion.State == ConnectionState.Closed)
	            {
		            mensaje_error = "conexion cerrada";
                    num_error = 0;
	            }
                else
	            {
                    conexion.Close();
                    mensaje_error = "ok";
	            }
	        }
	        catch (Exception)
	        {
		        throw;
	        }
        }

        /// <summary>
        /// realiza las consultas a la BD. no tiene parametros de consulta.
        /// </summary>
        /// <param name="conexion"></param>
        /// <param name="sql"></param>
        /// <param name="proc_almacenado"></param>
        /// <param name="mensaje_error"></param>
        /// <param name="num_error"></param>
        /// <returns></returns>
        public static DataSet ejecuta_dataset_consulta(SqlConnection conexion, string sql, bool proc_almacenado, ref string mensaje_error, ref int num_error)
        {
            SqlDataAdapter sql_data_adapter;
            DataSet dataset= new DataSet();

			try 
	        {	        
		        sql_data_adapter= new SqlDataAdapter(sql, conexion);
                if (proc_almacenado)
            	{
		            sql_data_adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
	            }
                sql_data_adapter.Fill(dataset);
                num_error = 0;
                mensaje_error = "ok";
                return dataset;
	        }
	        catch (Exception)
	        {
		        throw;
	        }
        }

        /// <summary>
        /// agregar los parametros en una estructura para enviarlos a la BD
        /// </summary>
        /// <param name="parametros"></param>
        /// <param name="posicion"></param>
        /// <param name="nombre_parametro"></param>
        /// <param name="tipo_dato"></param>
        /// <param name="valor_parametros"></param>
        public static void agregar_datos_estructura_parametros(ref paramStruct[] parametros, int posicion, string nombre_parametro, SqlDbType tipo_dato, object valor_parametros)
        {
            parametros[posicion].Nombre_Parametro = nombre_parametro.ToString();
            parametros[posicion].Tipo_Dato = tipo_dato;
            parametros[posicion].Valor_Parametro = valor_parametros;
        }

        /// <summary>
        /// ejecuta las funciones de agregar y editar.
        /// </summary>
        /// <param name="conexion"></param>
        /// <param name="sql"></param>
        /// <param name="proc_almacenado"></param>
        /// <param name="parametros"></param>
        /// <param name="mensaje_error"></param>
        /// <param name="num_error"></param>
        public static void ejecuta_sqlcommand_editar_agregar(SqlConnection conexion, string sql, bool proc_almacenado, paramStruct[] parametros, ref string mensaje_error, ref int num_error)
        {
            SqlCommand sql_command;
            try
            {
                int resultado = 0;
                sql_command = new SqlCommand(sql, conexion);
                if (proc_almacenado)
                {
                    sql_command.CommandType = CommandType.StoredProcedure;
                }
                foreach (paramStruct var in parametros)
                {
                    Agrega_parametro_sqlcommand(ref sql_command, var.Nombre_Parametro, var.Valor_Parametro.ToString(), var.Tipo_Dato);
                }
                resultado = sql_command.ExecuteNonQuery();
                num_error = 0;
                mensaje_error = "ok";
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// agrega los datos a la estructura de parametros
        /// </summary>
        /// <param name="sql_command"></param>
        /// <param name="nombre_parametro"></param>
        /// <param name="valor_parametro"></param>
        /// <param name="tipo_dato"></param>
        public static void Agrega_parametro_sqlcommand(ref SqlCommand sql_command, string nombre_parametro, string valor_parametro, SqlDbType tipo_dato)
        {
            SqlParameter param = new SqlParameter();
            param.ParameterName = nombre_parametro;
            param.Value = valor_parametro;
            param.SqlDbType = tipo_dato;
            sql_command.Parameters.Add(param);
        }

        public static void ejecuta_sqlcommand_editar_agregar_imagen(SqlConnection conexion, string sql, bool es_proc_almacenado, paramStruct[] parametros, ref string mensaje_error, ref  int num_error)
        {
            SqlCommand sql_command;
            try
            {
                int resultado = 0;
                sql_command = new SqlCommand(sql, conexion);
                if (es_proc_almacenado)
                {
                    sql_command.CommandType = CommandType.StoredProcedure;
                }
                foreach (paramStruct var in parametros)
                {
                    Agrega_parametro_sqlcommand_imagen(ref sql_command, var.Nombre_Parametro, var.Valor_Parametro, var.Tipo_Dato);
                }
                resultado = sql_command.ExecuteNonQuery();
                num_error = 0;
                mensaje_error = "ok";
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static void Agrega_parametro_sqlcommand_imagen(ref SqlCommand sql_command, string nombre_parametro, object valor_parametro, SqlDbType tipo_dato)
        {
            SqlParameter param = new SqlParameter();
            param.ParameterName = nombre_parametro;
            param.Value = valor_parametro;
            param.SqlDbType = tipo_dato;
            sql_command.Parameters.Add(param);
        }

        public static DataTable Login(SqlConnection conexion, string sql, bool es_proc_almacenado, string IdUsuario, string Clave)
        {
            SqlCommand sql_command;
            try
            {
                sql_command = new SqlCommand(sql, conexion);
                if (es_proc_almacenado)
                {
                    sql_command.CommandType = CommandType.StoredProcedure;
                }

                //sql_command.Parameters.AddWithValue("@IdUsuario", IdUsuario);
                //sql_command.Parameters.AddWithValue("@clave", Clave);

                SqlDataAdapter da = new SqlDataAdapter(sql_command);

                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
