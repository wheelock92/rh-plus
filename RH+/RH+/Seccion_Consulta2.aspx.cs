﻿using BLL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Seccion_Consulta2 : System.Web.UI.Page
{
    Secciones_L objSeccion = new Secciones_L();
    Login_L objLogin = new Login_L();

    protected void Page_Load(object sender, EventArgs e)
    {
        objLogin.IdRol = Convert.ToInt32(Session["IdRol"]);
        if (objLogin.SuperAdmin() || objLogin.Admin() || objLogin.Editor() || objLogin.Lector())
        {
            carga_lista_seccion();
        }
        else
        {
            Session["Error"] = "El usuario no posee los permisos para acceder.";
            Response.Redirect("Errores.aspx");
        }
    }

    private void carga_lista_seccion()
    {
        try
        {
            objSeccion.IdOrganizacion = Convert.ToInt32(Session["IdOrganizacion"]);
            gv_Seccion.DataSource = objSeccion.Carga_Secciones_Organizacion();
            gv_Seccion.DataBind();
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("Errores.aspx", false);
        }
    }

    protected void gv_Seccion_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton db = (LinkButton)e.Row.Cells[5].Controls[0];
                string Seccion = e.Row.Cells[1].Text;
                db.OnClientClick = string.Format("return confirm ('¿Desea eliminar la sección: {0}?');", Seccion);
            }
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("Errores.aspx", false);
        }
    }

    protected void gv_Seccion_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Eliminar")
            {
                int fila = Convert.ToInt32(e.CommandArgument);
                int IdSeccion = Convert.ToInt32(gv_Seccion.DataKeys[fila].Value);
                if (objSeccion.Eliminar_Seccion(IdSeccion))
                {
                    this.carga_lista_seccion();
                }
            }
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("Errores.aspx", false);
        }
    }

    protected void ExportarExcel(object sender, EventArgs e)
    {
        Response.Clear();
        Response.Buffer = true;
        Response.ClearContent();
        Response.ClearHeaders();
        Response.Charset = "";
        string NombreArchivo = "Secciones" + DateTime.Now + ".xls";
        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.AddHeader("Content-Disposition", "attachment;filename=" + NombreArchivo);
        gv_Seccion.GridLines = GridLines.Both;
        gv_Seccion.HeaderStyle.Font.Bold = true;
        gv_Seccion.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();

        /*Response.AddHeader("content-disposition", "attachment; filename=gvtoexcel.xls");
        Response.ContentType = "application/vnd.ms-excel";*/
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        //base.VerifyRenderingInServerForm(control);
        return;
    }

    protected void gv_Seccion_Sorting(object sender, GridViewSortEventArgs e)
    {

    }

    protected void gv_Seccion_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gv_Seccion.PageIndex = e.NewPageIndex;
        carga_lista_seccion();
    }
}