﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PerfilesPuesto_Consulta.aspx.cs" Inherits="PerfilesPuesto_Consulta" EnableEventValidation="false" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h2>Perfiles de Puesto</h2>
    <div class="table-responsive" style="overflow:auto">
        <br />
        <asp:Button ID="btnNUevo" PostBackUrl="~/PerfilesPuesto_Nuevo.aspx" runat="server" Text="Nuevo" class="btn btn-primary" OnClick="ExportarExcel" />
        &nbsp;
        <asp:Button ID="btn_Excel" runat="server" Text="Excel" class="btn btn-success" />
        <br /><br />

        <style>
            .Hide {
                display: none;
            }
        </style>

        <asp:GridView ID="gv_PerfilesPuesto" CssClass="table table-striped table-hover table-bordered"  runat="server" AutoGenerateColumns="False" DataKeyNames="IdPerfil" OnRowCommand="gv_PerfilesPuesto_RowCommand1" AllowPaging="True" AllowSorting="True" OnPageIndexChanging="gv_PerfilesPuesto_PageIndexChanging">
            <Columns>
                <asp:BoundField DataField="IdPerfil" HeaderText="Id Perfil" Visible="False" />
                <asp:BoundField DataField="Codigo" HeaderText="Codigo" />
                <asp:BoundField DataField="Version" HeaderText="Version" />
                <asp:BoundField DataField="NombrePuesto" HeaderText="Titulo" />
                <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" />
                <asp:BoundField DataField="FechaModificacion" HeaderText="Fecha Vigencia" DataFormatString="{0:dd/MM/yyy}"/>
                <asp:BoundField DataField="Id" HeaderText="IdUsuario" Visible="true" HeaderStyle-CssClass="Hide" ItemStyle-CssClass="hide" />
                <asp:BoundField DataField="Administrador" HeaderText="Administrador" />
                <asp:BoundField DataField="IdResp" HeaderText="IdResponsable"  HeaderStyle-CssClass="Hide" ItemStyle-CssClass="hide" />
                <asp:BoundField DataField="Responsable" HeaderText="Responsable" />
                <asp:BoundField DataField="IdSeccion" HeaderText="IdSeccion"  HeaderStyle-CssClass="Hide" ItemStyle-CssClass="hide" />
                <asp:BoundField DataField="Seccion" HeaderText="Seccion" />
                <asp:BoundField DataField="IdCategoria" HeaderText="IdCategoría" HeaderStyle-CssClass="Hide" ItemStyle-CssClass="hide" />
                <asp:BoundField DataField="Categoria" HeaderText="Categoría" />
                <asp:BoundField DataField="IdSubcategoria" HeaderText="IdSubcategoria" HeaderStyle-CssClass="Hide" ItemStyle-CssClass="hide" />
                <asp:BoundField DataField="SubCategoria" HeaderText="SubCategoria" />
                <asp:BoundField DataField="IdArchivo" HeaderText="IdArchivo" HeaderStyle-CssClass="Hide" ItemStyle-CssClass="hide" />
                <asp:BoundField DataField="Archivo" HeaderText="Archivo" />
                <asp:BoundField DataField="IdEstado" HeaderText="IdEstado" HeaderStyle-CssClass="Hide" ItemStyle-CssClass="hide" />
                <asp:BoundField DataField="Estado" HeaderText="Estado" />
                <asp:ButtonField CommandName="Aprobado" HeaderText="Aprobado" Text="Aprobado" ButtonType="Button" />
                <asp:ButtonField CommandName="Obsoleto" HeaderText="Obsoleto" Text="Obsoleto" ButtonType="Button" />
                <asp:ButtonField CommandName="Descargar" HeaderText="Archivo" Text="Descargar" ButtonType="Button" />
                <asp:ButtonField CommandName="Editar" Text="Editar" HeaderText="Editar" />
                <asp:ButtonField CommandName="Eliminar" Text="Eliminar" HeaderText="Eliminar" />
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>

