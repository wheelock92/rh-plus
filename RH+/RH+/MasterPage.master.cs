﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        MenuRoles();
    }

    private void MenuRoles()
    {
        int idRol = Convert.ToInt32(Session["IdRol"]);

        if (idRol == 1)
        {
            Seccion.Visible = false;
            Usuarios.Visible = false;
            Correo.Visible = false;
            Categoria.Visible = false;
            SubCategoria.Visible = false;
            PerfilesPuesto.Visible = false;
        }

        if (idRol != 1)
        {
            GrupoEmpresarial.Visible = false;
            Organizacion.Visible = false;
        }

        if (idRol != 1 && idRol != 2)
        {
            Seccion.Visible = false;
            Usuarios.Visible = false;
            Correo.Visible = false;
            Categoria.Visible = false;
            SubCategoria.Visible = false;
        }
    }

    private void Cerrar()
    {
        Session.Clear();
        Session.RemoveAll();
        Session.Abandon();
    }

    public void CerrarSesion_Click(Object sender, EventArgs e)
    {
        Session.Clear();
        Session.RemoveAll();
        Session.Abandon();
        Response.Redirect("~/Login.aspx");
    }
}
