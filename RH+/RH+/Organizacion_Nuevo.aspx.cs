﻿using BLL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Organizacion_Nuevo : System.Web.UI.Page
{
    Organizacion_L objOrganizacion = new Organizacion_L();
    Login_L objLogin = new Login_L();
    Secciones_L objSeccion = new Secciones_L();

    protected void Page_Load(object sender, EventArgs e)
    {
        int idRol;
        idRol = Convert.ToInt32(Session["IdRol"]);
        objLogin.IdRol = idRol;

        if (!objLogin.SuperAdmin())
        {
            Session["Error"] = "El usuario no posee los permisos para acceder.";
            Response.Redirect("~/ErroresSU.aspx", false);
        }

        //Carga_Secciones();

        if (!Page.IsPostBack)
        {
            if (Request.QueryString["IdOrganizacion"] != null)
            {
                txt_NombreEmpresa.Text = Request.QueryString["NombreEmpresa"];
                if (Request.QueryString["IdGrupo"] != string.Empty)
                {
                    chk_Grupo_Empresarial.Visible = true;
                    chk_Grupo_Empresarial.Checked = true;
                    ddl_GrupoEmpresarial.Visible = true;
                    ddl_GrupoEmpresarial.SelectedValue = Request.QueryString["IdGrupo"];
                }
                else
                {
                    //chk_Grupo_Empresarial.Checked = false;
                }
                if (Request.QueryString["Activa"] == "True")
                {
                    chk_Activa.Checked = true;
                }
                else
                {
                    chk_Activa.Checked = false;
                }
                txt_Cantidad_Usuarios.Text = Request.QueryString["CantidadUsuario"];
                txt_Observaciones.Text = Request.QueryString["Observaciones"];
                txt_Apellido1.Text = Request.QueryString["Apellido1"];
                txt_Apellido2.Text = Request.QueryString["Apellido2"];
                txt_Correo.Text = Request.QueryString["Correo"];
                txt_Login.Text = Request.QueryString["IdUsuario"];
                txt_NombreAdmin.Text = Request.QueryString["Name"];
                
            }
        }
    }

    private void GuardarNuevo()
    {
        try
        {
            if (Request.QueryString["IdOrganizacion"] != null)
            {
                if (chk_Activa.Checked)
                {
                    objOrganizacion.Activa = true;
                }
                else
                {
                    objOrganizacion.Activa = false;
                }
                objOrganizacion.CantidadUsuarios = Convert.ToInt32(txt_Cantidad_Usuarios.Text);
                if (chk_Grupo_Empresarial.Checked)
                {
                    objOrganizacion.GrupoEmpresarial = Convert.ToInt32(ddl_GrupoEmpresarial.SelectedValue);
                }
                objOrganizacion.IdOrganizacion = Convert.ToInt32(Request.QueryString["IdOrganizacion"]);
                objOrganizacion.NombreEmpresa = txt_NombreEmpresa.Text;
                objOrganizacion.Observaciones = txt_Observaciones.Text;
                objOrganizacion.IdUsuarioAdmin = Convert.ToInt32(Request.QueryString["Id"]);

                if (objOrganizacion.Editar_Organizacion())
                {
                    Response.Redirect("~/Organizacion_Consulta.aspx", false);
                }
                else
                {
                    string error = "Error al editar la información. Contacte al administrador.";
                    Response.Redirect("~/ErroresSU.aspx?Error=" + error, false);
                }
            }
            else
            {
                //Datos organizacion
                objOrganizacion.CantidadUsuarios = Convert.ToInt32(txt_Cantidad_Usuarios.Text);
                objOrganizacion.NombreEmpresa = txt_NombreEmpresa.Text;
                objOrganizacion.Observaciones = txt_Observaciones.Text;
                if (chk_Activa.Checked)
                {
                    objOrganizacion.Activa = true;
                }
                else
                {
                    objOrganizacion.Activa = false;
                }
                if (chk_Grupo_Empresarial.Checked)
                {
                    objOrganizacion.GrupoEmpresarial = Convert.ToInt32(ddl_GrupoEmpresarial.SelectedValue);
                }
                else
                {
                    objOrganizacion.GrupoEmpresarial = Convert.ToInt32(null);
                }

                byte[] imagesize = new byte[fu_Logo.PostedFile.ContentLength];
                HttpPostedFile uploadimage = fu_Logo.PostedFile;

                uploadimage.InputStream.Read(imagesize, 0, (int)fu_Logo.PostedFile.ContentLength);
                objOrganizacion.Logo = imagesize;


                //datos usuario
                objOrganizacion.Apellido1 = txt_Apellido1.Text;
                objOrganizacion.Apellido2 = txt_Apellido2.Text;
                objOrganizacion.Clave = "123456";
                objOrganizacion.Correo = txt_Correo.Text;
                objOrganizacion.Estado = true;
                if (chk_Grupo_Empresarial.Checked)
                {
                    objOrganizacion.GrupoEmpresarial = Convert.ToInt32(ddl_GrupoEmpresarial.SelectedValue);
                }
                objOrganizacion.IdRol = 2;
                objOrganizacion.IdSeccion = 4;
                objOrganizacion.Nombre = txt_NombreAdmin.Text;
                objOrganizacion.Login = txt_Login.Text;

                if (objOrganizacion.Agregar_Organizacion())
                {
                    Response.Redirect("Organizacion_Consulta.aspx", false);
                }
            }
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("~/ErroresSU.aspx", false);
        }
    }

    private void EditarOrganizacion()
    {
        if (chk_Activa.Checked)
        {
            objOrganizacion.Activa = true;
        }
        else
        {
            objOrganizacion.Activa = false;
        }
        objOrganizacion.CantidadUsuarios = Convert.ToInt32(txt_Cantidad_Usuarios.Text);
        if (chk_Grupo_Empresarial.Checked)
        {
            objOrganizacion.GrupoEmpresarial = Convert.ToInt32(ddl_GrupoEmpresarial.SelectedValue);
        }
        objOrganizacion.IdOrganizacion = Convert.ToInt32(Request.QueryString["IdOrganizacion"]);
        objOrganizacion.NombreEmpresa = txt_NombreEmpresa.Text;
        objOrganizacion.Observaciones = txt_Observaciones.Text;

        try
        {
            if (objOrganizacion.Editar_Organizacion())
            {
                Response.Redirect("~/Organizacion_Consulta.aspx", false);
            }
            else
            {
                Session["Error"] = "Error al editar la información. Contacte al administrador.";
                Response.Redirect("~/ErroresSU.aspx", false);
            }
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("~/ErroresSU.aspx", false);
        }
    }

    private void Editar_UsuarioAdmin_Organizacion()
    {
        try
        {
            objOrganizacion.Apellido1 = txt_Apellido1.Text;
            objOrganizacion.Apellido2 = txt_Apellido2.Text;
            objOrganizacion.Correo = txt_Correo.Text;
            objOrganizacion.Login = txt_Login.Text;
            objOrganizacion.Nombre = txt_NombreAdmin.Text;
            objOrganizacion.IdUsuarioAdmin = Convert.ToInt32(Request.QueryString["Id"]);

            if (objOrganizacion.Editar_UsuarioAdmin_Organizacion())
            {
                Response.Redirect("~/Organizacion_Consulta.aspx", false);
            }
            else
            {
                Session["Error"] = "Error al cambiar al nuevo usuario administrador.";
                Response.Redirect("~/ErroresSU.aspx", false);
            }
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("~/Errores.aspx", false);
        }
    }

    protected void btn_Guardar_Click(object sender, EventArgs e)
    {
        if (Request.QueryString["IdOrganizacion"] == null)
        {
            GuardarNuevo();
        }
        else
        {
            EditarOrganizacion();
            if (txt_Login.Text == Request.QueryString["IdUsuario"])
            {
                Editar_UsuarioAdmin_Organizacion();
            }
            else
            {
                val_Apellido1.Enabled = false;
                val_Apellido2.Enabled = false;
                val_Correo.Enabled = false;
                val_Login.Enabled = false;
                val_NombreAdmin.Enabled = false;
            }
        }
    }

    protected void chk_Grupo_Empresarial_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_Grupo_Empresarial.Checked)
        {
            ddl_GrupoEmpresarial.Visible = true;
            ddl_GrupoEmpresarial.Enabled = true;
        }
        else
        {
            ddl_GrupoEmpresarial.Enabled = false;
            ddl_GrupoEmpresarial.Visible = false;
        }
    }

    protected void Btn_Reset_Click(object sender, EventArgs e)
    {
        try
        {
            objOrganizacion.IdUsuarioAdmin = Convert.ToInt32(Request.QueryString["Id"]);
            objOrganizacion.Clave = "123456";
            if (objOrganizacion.Reset_Clave_Administrador())
            {
                Response.Write("<script LANGUAGE='JavaScript' >alert('Se reseteó la contraseña exitosamente.')</script>");
            }
            else
            {
                Session["Error"] = "Error al resetear la contraseña.";
                Response.Redirect("~/Errores.aspx", false);
            }
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("~/ErroresSU.aspx", false);
        }
    }
}