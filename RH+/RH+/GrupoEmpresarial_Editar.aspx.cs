﻿using BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class GrupoEmpresarial_Editar : System.Web.UI.Page
{
    GrupoEmpresarial_L objGrupo = new GrupoEmpresarial_L();
    Login_L objLogin = new Login_L();

    protected void Page_Load(object sender, EventArgs e)
    {
        objLogin.IdRol = Convert.ToInt32(Session["IdRol"]);
        if (objLogin.SuperAdmin())
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["IdGrupo"] != null)
                {
                    txt_Descripcion.Text = Request.QueryString["Descripcion"];
                    txt_IdGrupo.Text = Request.QueryString["IdGrupo"];
                    txt_Nombre.Text = Request.QueryString["Nombre"];
                    if (Request.QueryString["Activo"] == "True")
                    {
                        chk_Activo.Checked = true;
                    }
                    else
                    {
                        chk_Activo.Checked = false;
                    }
                }
            }
        }
        else
        {
            Session["Error"] = "El usuario no posee los permisos para acceder.";
            Response.Redirect("~/ErroresSU.aspx", false);
        }
    }

    private void Guardar()
    {
        try
        {
            if (chk_Activo.Checked)
            {
                objGrupo.Activo = true;
            }
            else
            {
                objGrupo.Activo = false;
            }
            objGrupo.Descripcion = txt_Descripcion.Text;
            objGrupo.Nombre = txt_Nombre.Text;

            if (txt_IdGrupo.Text != string.Empty)
            {
                objGrupo.IdGrupo = Convert.ToInt32(txt_IdGrupo.Text);
                if (objGrupo.Editar_Grupo())
                {
                    Response.Redirect("GrupoEmpresarial_Consulta.aspx", false);
                }
                else
                {
                    string error = "Ha ocurrido un error al editar en la BD. Favor contacte al administrador.";
                    Response.Redirect("~/ErroresSU.aspx?Error=" + error, false);
                }
            }
            else
            {
                if (objGrupo.Agregar_Grupo())
                {
                    Response.Redirect("GrupoEmpresarial_Consulta.aspx", false);
                }
                else
                {
                    string error = "Ha ocurrido un error al guardar en la BD. Favor contacte al administrador.";
                    Response.Redirect("~/ErroresSU.aspx?Error=" + error, false);
                }
            }

            
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("~/ErroreSUs.aspx", false);
        }
    }
    protected void btn_Guardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }
}