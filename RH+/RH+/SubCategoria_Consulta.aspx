﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SubCategoria_Consulta.aspx.cs" Inherits="SubCategoria_Consulta" EnableEventValidation="false" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h2>Subcategorías</h2>
    <br />
    <asp:Button ID="btn_Nuevo" Text="Nuevo" class="btn btn-primary" PostBackUrl="~/SubCategoria_Nuevo.aspx" runat="server" />
    &nbsp;
    <asp:Button ID="btn_Excel" runat="server" Text="Excel" class="btn btn-success" OnClick="ExportarExcel" />
    <br /><br />
    <asp:GridView ID="gv_SubCategoria" CssClass="table table-striped table-hover table-bordered" runat="server" AutoGenerateColumns="False" DataKeyNames="IdSubCategoria" OnRowCommand="gv_SubCategoria_RowCommand" OnRowDataBound="gv_SubCategoria_RowDataBound" AllowPaging="true" PageSize="10" AllowSorting="true" OnPageIndexChanging="gv_SubCategoria_PageIndexChanging">
        <Columns>
            <asp:BoundField DataField="IdSubCategoria" HeaderText="IdSubCategoria" Visible="False" />
            <asp:BoundField DataField="Nombre" HeaderText="SubCategoria" />
            <asp:BoundField DataField="IdOrganizacion" HeaderText="IdOrganizacion" Visible="False" />
            <asp:CheckBoxField DataField="Activo" HeaderText="Activo" />
            <asp:HyperLinkField DataNavigateUrlFields="IdSubCategoria,Nombre,IdOrganizacion,Activo" DataNavigateUrlFormatString="SubCategoria_Nuevo.aspx?IdSubCategoria={0}&amp;Nombre={1}&amp;IdOrganizacion={2}&amp;Activo={3}" HeaderText="Editar" Text="Editar" />
            <asp:ButtonField CommandName="Eliminar" HeaderText="Eliminar" Text="Eliminar" />
        </Columns>
    </asp:GridView>
    <br />
</asp:Content>

