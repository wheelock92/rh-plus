﻿using BLL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Categoria_Consulta : System.Web.UI.Page
{
    Categoria_L objCategoria = new Categoria_L();
    Login_L objLogin = new Login_L();

    protected void Page_Load(object sender, EventArgs e)
    {
        int idRol = Convert.ToInt32(Session["IdRol"]);
        objLogin.IdRol = idRol;

        if (objLogin.SuperAdmin() || objLogin.Admin() || objLogin.Editor() || objLogin.Lector())
        {
            Carga_Lista_Categoria();
        }
        else
        {
            Session["Error"] = "El usuario no posee los permisos para acceder.";
            Response.Redirect("~/Errores.aspx");
        }
    }

    /// <summary>
    /// carga las categorías de la organización logueada.
    /// </summary>
    private void Carga_Lista_Categoria()
    {
        try
        {
            objCategoria.IdOrganizacion = Convert.ToInt32(Session["IdOrganizacion"]);
            gv_Categoria.DataSource = objCategoria.Categoria_Organizacion();
            gv_Categoria.DataBind();
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("~/Errores.aspx", false);
        }
    }
    protected void gv_Categoria_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        
    }
    protected void gv_Categoria_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        
    }

    /// <summary>
    /// exporta a excel la información del gridview
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ExportarExcel(object sender, EventArgs e)
    {
        Response.Clear();
        Response.Buffer = true;
        Response.ClearContent();
        Response.ClearHeaders();
        Response.Charset = "";
        string NombreArchivo = "Categorias" + DateTime.Now + ".xls";
        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.AddHeader("Content-Disposition", "attachment;filename=" + NombreArchivo);
        gv_Categoria.GridLines = GridLines.Both;
        gv_Categoria.HeaderStyle.Font.Bold = true;
        gv_Categoria.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();

        /*Response.AddHeader("content-disposition", "attachment; filename=gvtoexcel.xls");
        Response.ContentType = "application/vnd.ms-excel";*/
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        //base.VerifyRenderingInServerForm(control);
        return;
    }

    protected void gv_Categoria_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gv_Categoria.PageIndex = e.NewPageIndex;
        Carga_Lista_Categoria();
    }

    /// <summary>
    /// elimina la categoría seleccionada en el gridview
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gv_Categoria_RowCommand1(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Eliminar")
        {
            int fila = Convert.ToInt32(e.CommandArgument);
            int IdCategoria = Convert.ToInt32(gv_Categoria.DataKeys[fila].Value);
            try
            {
                if (objCategoria.Eliminar_Categoria(IdCategoria))
                {
                    this.Carga_Lista_Categoria();
                }
                else
                {
                    string error = "No se ha podido eliminar la Categoria seleccionada. Favor contacte al administrador";
                    Response.Redirect("~/Errores.aspx?Error" + error, false);
                }
            }
            catch (Exception ex)
            {
                Session["Error"] = ex.Message;
                Response.Redirect("~/Errores.aspx", false);
            }
        }
    }

    protected void gv_Categoria_RowDataBound1(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton db = (LinkButton)e.Row.Cells[5].Controls[0];
                string categoria = e.Row.Cells[1].Text;
                db.OnClientClick = string.Format("return confirm ('¿Desea eliminar la Categoría: {0}');", categoria);
            }
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("~/Errores.aspx", false);
        }
    }
}