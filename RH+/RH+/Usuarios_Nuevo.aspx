﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Usuarios_Nuevo.aspx.cs" Inherits="Usuarios_Nuevo" EnableEventValidation="false" %>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form role="form" id="templatemo-preferences-form">
        <div class="row">
            <div class="col-md-6 margin-bottom-15">
                <label for="Username" class="control-label">Username</label>
                <asp:TextBox ID="txt_IdUsuario" runat="server" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ID="val_Username" ErrorMessage="Ingrese el Username del usuario" runat="server" ControlToValidate="txt_IdUsuario"
                    Display="None"></asp:RequiredFieldValidator>
            </div>
            <div class="col-md-6 margin-bottom-15">
                <label for="Nombre" class="control-label">Nombre</label>
                <asp:TextBox ID="txt_Nombre" runat="server" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ID="val_Nombre" ErrorMessage="Ingrese el Nombre del usuario" runat="server" ControlToValidate="txt_Nombre" Display="None"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 margin-bottom-15">
                <label for="Apellido1" class="control-label">Primer Apellido</label>
                <asp:TextBox ID="txt_Apellido1" runat="server" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ID="val_Apellido1" ErrorMessage="Ingrese el primer apellido del usuario" runat="server" ControlToValidate="txt_Apellido1"
                    Display="None"></asp:RequiredFieldValidator>
            </div>
            <div class="col-md-6 margin-bottom-15">
                <label for="Apellido2" class="control-label">Segundo Apellido</label>
                <asp:TextBox ID="txt_Apellido2" runat="server" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ID="val_Apellido2" ErrorMessage="Ingrese el segundo apellido del usuario" runat="server" ControlToValidate="txt_Apellido2"
                    Display="None"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 margin-bottom-15">
                <label for="Correo" class="control-label">Correo</label>
                <asp:TextBox ID="txt_Correo" TextMode="Email" ToolTip="ejemplo@dominio.com" runat="server" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ID="val_Correo" ErrorMessage="Ingrese el correo del usuario." ControlToValidate="txt_Correo" runat="server" Display="None"></asp:RequiredFieldValidator>
            </div>
            <div class="col-md-6 margin-bottom-15">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" value=""/>
                        <asp:CheckBox ID="chk_Activa" Text="Activo" Checked="true" runat="server" />
                    </label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 margin-bottom-15">
                <label for="Rol" class="control-label">Rol</label>
                <asp:DropDownList ID="ddl_Rol" runat="server" DataSourceID="DS_Roles" DataTextField="Nombre" DataValueField="IdRol"></asp:DropDownList>
                <asp:SqlDataSource ID="DS_Roles" runat="server" ConnectionString="<%$ ConnectionStrings:RH+ %>" SelectCommand="SELECT * FROM [RHPLUS_Roles] WHERE ([IdRol] &lt;&gt; @IdRol)">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="1" Name="IdRol" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </div>
            <div class="col-md-6 margin-bottom-15">
                <label for="Seccion" class="control-label">Sección</label>
                <asp:DropDownList ID="ddl_Seccion" runat="server" DataTextField="Nombre" DataValueField="IdSeccion"></asp:DropDownList>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 margin-bottom-15">
                <asp:Button ID="btn_Guardar" Text="Guardar" CssClass="btn btn-primary" runat="server" OnClick="btn_Guardar_Click" />
                <asp:ValidationSummary ID="val_Muestra" runat="server" DisplayMode="BulletList" ShowMessageBox="true" ShowSummary="false" />
            </div>
        </div>
    </form>
</asp:Content>