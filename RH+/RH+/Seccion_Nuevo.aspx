<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Seccion_Nuevo.aspx.cs" Inherits="Seccion_Nuevo" EnableEventValidation="false" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form role="form" id="templatemo-preferences-form">
        <div class="row">
            <div class="col-md-6 margin-bottom-15">
                <label for="firstName" class="control-label">Nombre</label>
                <asp:TextBox  class="form-control" ID="txt_nombre" runat="server"></asp:TextBox>
            </div>
            <div class="col-md-6 margin-bottom-15">
                <label>
                    <asp:CheckBox ID="chk_Activo" Text="Activo" runat="server" Checked="true" />
                </label>
            </div>
        </div>
        <asp:Button ID="btn_Guardar" class="btn btn-primary" Text="Guardar" runat="server" OnClick="btn_Guardar_Click" />
    </form>
</asp:Content>

