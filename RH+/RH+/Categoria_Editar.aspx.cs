﻿using BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Categoria_Editar : System.Web.UI.Page
{
    Categoria_L objCategoria = new Categoria_L();
    Login_L objLogin = new Login_L();
    bool nuevo;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            int idRol = Convert.ToInt32(Session["IdRol"]);
            objLogin.IdRol = idRol;

            if (objLogin.SuperAdmin() || objLogin.Admin() || objLogin.Editor())
            {
                ///si el usuario desea editar una categoría aqui carga los datos del gridview en el formulario
                if (!Page.IsPostBack)
                {
                    if (Request.QueryString["IdCategoria"] != null)
                    {
                        txt_IdCategoria.Text = Request.QueryString["IdCategoria"];
                        txt_Nombre.Text = Request.QueryString["Nombre"];
                        if (Request.QueryString["Activo"] == "True")
                        {
                            chk_Activo.Checked = true;
                        }
                        else
                        {
                            chk_Activo.Checked = false;
                        }
                    }
                }
                else
                {
                    nuevo = true;
                }
            }
            else
            {
                Session["Error"] = "El usuario no posee los permisos para acceder.";
                Response.Redirect("~/Errores.aspx");
            }
        }
        catch (Exception)
        {
            string error = "No se ha podido cargar la información. Favor contacte al administrador.";
            Response.Redirect("~/Errores.aspx?Error" + error, false);
        }
    }

    /// <summary>
    /// guarda la información del formulario.
    /// en caso de ser de edición hace la distinción dependiendo de si el campo de idcategoria esta vacio o no.
    /// </summary>
    private void Guardar()
    {
        try
        {
            objCategoria.Nombre = txt_Nombre.Text;
            objCategoria.IdOrganizacion = Convert.ToInt32(Session["IdOrganizacion"]);
            if (chk_Activo.Checked)
            {
                objCategoria.Activa = true;
            }
            else
            {
                objCategoria.Activa = false;
            }

            if (txt_IdCategoria.Text != string.Empty)
            {
                objCategoria.IdCategoria = Convert.ToInt32(txt_IdCategoria.Text);
                if (objCategoria.Editar_Categoria())
                {
                    Response.Redirect("~/Categoria_Consulta.aspx", false);
                }
            }
            else
            {
                if (objCategoria.Agregar_Categoria())
                {
                    Response.Redirect("~/Categoria_Consulta.aspx", false);
                }
            }
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("~/Errores.aspx", false);
        }
    }

    protected void txt_Guardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }
}