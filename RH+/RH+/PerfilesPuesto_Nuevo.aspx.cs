﻿using BLL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PerfilesPuesto_Nuevo : System.Web.UI.Page
{
    PerfilesPuesto_L objPerfilesPuesto = new PerfilesPuesto_L();
    Login_L objLogin = new Login_L();
    Usuarios_L objUsuarios = new Usuarios_L();
    SubCategoria_L objSubcategoria = new SubCategoria_L();
    Secciones_L objSeccion = new Secciones_L();
    Categoria_L objCategoria = new Categoria_L();

    protected void Page_Load(object sender, EventArgs e)
    {
        objLogin.IdRol=Convert.ToInt32(Session["IdRol"]);

        objUsuarios.IdOrganizacion = Convert.ToInt32(Session["IdOrganizacion"]);
        ddl_Administrador.DataSource = objUsuarios.Usuarios_Organizacion();
        ddl_Administrador.DataBind();

        objSubcategoria.IdOrganizacion = Convert.ToInt32(Session["IdOrganizacion"]);
        ddl_SubCategoria.DataSource = objSubcategoria.Subcategoria_Organizacion();
        ddl_SubCategoria.DataBind();

        objSeccion.IdOrganizacion = Convert.ToInt32(Session["IdOrganizacion"]);
        ddl_Seccion.DataSource = objSeccion.Carga_Secciones_Organizacion();
        ddl_Seccion.DataBind();

        objCategoria.IdOrganizacion = Convert.ToInt32(Session["IdOrganizacion"]);
        ddl_Categoria.DataSource = objCategoria.Categoria_Organizacion();
        ddl_Categoria.DataBind();

        if (!objLogin.SuperAdmin() && !objLogin.Admin() && !objLogin.Editor())
        {
            Session["Error"] = "El usuario no posee los permisos para acceder.";
            Response.Redirect("~/Errores.aspx", false);
        }

        if (!Page.IsPostBack)
        {
            txt_Codigo.Text = Request.QueryString["Codigo"];
            txt_Descripcion.Text = Request.QueryString["Descripcion"];
            txt_Titulo.Text = Request.QueryString["Titulo"];
            txt_Version.Text = Request.QueryString["Version"];
            ddl_Administrador.SelectedValue = Request.QueryString["Id"];
            ddl_Categoria.SelectedValue = Request.QueryString["IdCategoria"];
            ddl_Seccion.SelectedValue = Request.QueryString["IdSeccion"];
            ddl_SubCategoria.SelectedValue = Request.QueryString["IdSubcategoria"];
            cal_Vigencia.SelectedDate = Convert.ToDateTime(Request.QueryString["FechaVigencia"]);
        }
    }

    private void Guardar()
    {
        try
        {
            objPerfilesPuesto.Administrador = Convert.ToInt32(ddl_Administrador.SelectedValue);
            objPerfilesPuesto.Codigo = txt_Codigo.Text;
            objPerfilesPuesto.Descripcion = txt_Descripcion.Text;
            objPerfilesPuesto.FechaVigencia = cal_Vigencia.SelectedDate;
            objPerfilesPuesto.IdEstado = 0;
            objPerfilesPuesto.Responsable = Convert.ToInt32(Session["Id"]);
            objPerfilesPuesto.Seccion = Convert.ToInt32(ddl_Seccion.SelectedValue);
            objPerfilesPuesto.SubCategoria = Convert.ToInt32(ddl_SubCategoria.SelectedValue);
            objPerfilesPuesto.Titulo = txt_Titulo.Text;
            objPerfilesPuesto.IdCategoria = Convert.ToInt32(ddl_Categoria.SelectedValue);
            objPerfilesPuesto.Version = Convert.ToInt32(txt_Version.Text);

            if (fu_Archivo.HasFile)
            {
                string NombreArchivo = Path.GetFileName(fu_Archivo.FileName);
                fu_Archivo.SaveAs(Server.MapPath("~/" + NombreArchivo));
                objPerfilesPuesto.Nombre = NombreArchivo;
                objPerfilesPuesto.Extension = NombreArchivo.Substring(NombreArchivo.LastIndexOf(".") + 1);
            }

            byte[] filesize = new byte[fu_Archivo.PostedFile.ContentLength];
            HttpPostedFile uploadFile = fu_Archivo.PostedFile;

            uploadFile.InputStream.Read(filesize, 0, (int)fu_Archivo.PostedFile.ContentLength);
            objPerfilesPuesto.Archivo = filesize;

            string IdPerfil = Request.QueryString["IdPerfil"];
            if (Request.QueryString["IdPerfil"] == null)
            {
                if (objPerfilesPuesto.Agregar_Perfil())
                {
                    Response.Redirect("PerfilesPuesto_Consulta.aspx", false);
                }
                else
                {
                    string error = "Ha ocurrido un error al insertar en la BD. Favor contacte al administrador.";
                    Response.Redirect("Errores.aspx?Error" + error, false);
                }
            }
            else
            {
                objPerfilesPuesto.IdPerfil = Convert.ToInt32(Request.QueryString["IdPerfil"]);
                if (objPerfilesPuesto.Editar_Perfil())
                {
                    Response.Redirect("PerfilesPuesto_Consulta.aspx", false);
                }
                else
                {
                    string error = "Ha ocurrido un error al insertar en la BD. Favor contacte al administrador.";
                    Response.Redirect("Errores.aspx?Error" + error, false);
                }
            }
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("Errores.aspx");
        }
    }
    protected void btn_Guardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void val_Vigencia_ServerValidate(object source, ServerValidateEventArgs args)
    {
        try
        {
            //if (cal_Vigencia.SelectedDate.Date <= DateTime.Now)
            //{
            //    args.IsValid = true;
            //}
            //else
            //{
            //    args.IsValid = false;
            //}
            if (cal_Vigencia.SelectedDate == null || cal_Vigencia.SelectedDate == new DateTime(0001, 1, 1, 0, 0, 0))
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("~/Errores.aspx", false);
        }
    }
}