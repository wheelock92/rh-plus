﻿using BLL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PerfilesPuesto_Consulta : System.Web.UI.Page
{
    PerfilesPuesto_L objPerfiles = new PerfilesPuesto_L();
    Login_L objLogin = new Login_L();

    protected void Page_Load(object sender, EventArgs e)
    {
        objLogin.IdRol = Convert.ToInt32(Session["IdRol"]);

        if (objLogin.SuperAdmin() || objLogin.Admin() || objLogin.Editor() || objLogin.Lector())
        {
            Carga_Lista_Perfiles();
        }
        else
        {
            Session["Error"] = "El usuario no posee los permisos para acceder.";
            Response.Redirect("~/Errores.aspx");
        }
    }

    private void Carga_Lista_Perfiles()
    {
        try
        {
            objPerfiles.IdOrganizacion = Convert.ToInt32(Session["IdOrganizacion"]);
            gv_PerfilesPuesto.DataSource = objPerfiles.PerfilesPuesto_Organizacion();
            gv_PerfilesPuesto.DataBind();
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("~/Errores.aspx", false);
        }
    }

    protected void gv_PerfilesPuesto_RowCommand1(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            switch (e.CommandName)
            {
                case "Obsoleto":
                    int fila = Convert.ToInt32(e.CommandArgument);
                    objPerfiles.IdPerfil = Convert.ToInt32(gv_PerfilesPuesto.DataKeys[fila].Value);

                    if (objPerfiles.Obsoleto_Perfil())
                    {
                        this.Carga_Lista_Perfiles();
                    }
                    break;

                case "Aprobado":
                    fila = Convert.ToInt32(e.CommandArgument);
                    objPerfiles.IdPerfil = Convert.ToInt32(gv_PerfilesPuesto.DataKeys[fila].Value);

                    if (objPerfiles.Aprobar_Perfil())
                    {
                        this.Carga_Lista_Perfiles();
                    }
                    break;

                //case "Descargar":
                //    string NombreArchivo = "";
                    
                //    Response.ClearHeaders();
                //    Response.ClearContent();

                //    if (NombreArchivo.Contains(".doc"))
                //    {
                //        Response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                //    }
                //    else
                //    {
                //        Response.ContentType = "application/octet-stream";
                //    }
                //    Response.AddHeader("Content-Disposition", "attachment; filename=" + nombreArchivo.Replace(' ', '_'));
                //    byte[] outbyte;

                //    break;

                case "Eliminar":
                    int filas = Convert.ToInt32(e.CommandArgument);
                    int IdPerfil = Convert.ToInt32(gv_PerfilesPuesto.DataKeys[filas].Value);
                    if (objPerfiles.Eliminar_Perfil(IdPerfil))
                    {
                        this.Carga_Lista_Perfiles();
                    }

                    break;

                case "Editar":
                    fila = Convert.ToInt32(e.CommandArgument);
                    string Perfil = gv_PerfilesPuesto.DataKeys[fila].Value.ToString();
                    string Codigo = gv_PerfilesPuesto.Rows[fila].Cells[1].Text.ToString();
                    string Version = gv_PerfilesPuesto.Rows[fila].Cells[2].Text.ToString();
                    string Titulo = gv_PerfilesPuesto.Rows[fila].Cells[3].Text.ToString();
                    string Descripcion = gv_PerfilesPuesto.Rows[fila].Cells[4].Text.ToString();
                    string FechaVigencia = gv_PerfilesPuesto.Rows[fila].Cells[5].Text.ToString();
                    string Id = gv_PerfilesPuesto.Rows[fila].Cells[6].Text.ToString();
                    string IdResp = gv_PerfilesPuesto.Rows[fila].Cells[8].Text.ToString();
                    string IdSeccion = gv_PerfilesPuesto.Rows[fila].Cells[10].Text.ToString();
                    string IdCategoria = gv_PerfilesPuesto.Rows[fila].Cells[11].Text.ToString();
                    string IdSubcategoria = gv_PerfilesPuesto.Rows[fila].Cells[14].Text.ToString();
                    string IdEstado = gv_PerfilesPuesto.Rows[fila].Cells[18].Text.ToString();

                    Response.Redirect("~/PerfilesPuesto_Nuevo.aspx?IdPerfil=" + Perfil + "&Codigo=" + Codigo + "&Version=" + Version + "&Titulo=" + Titulo 
                        + "&Descripcion=" + Descripcion + "&FechaVigencia=" + FechaVigencia + "&Id=" + Id + "&IdResp=" + IdResp + "&IdSeccion=" + IdSeccion 
                        + "&IdSubcategoria=" + IdSubcategoria + "&IdEstado=" + IdEstado + "&IdCategoria=" + IdCategoria, false);


                    break;

                default:
                    string errores = "La acción solicitada no está funcionando. Favor seleccione uno valido.";
                    Response.Redirect("~/Errores.aspx?Error" + errores, false);
                    break;
            }
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("~/Errores.aspx", false);
        }
    }
    protected void gv_PerfilPuesto_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton db = (LinkButton)e.Row.Cells[24].Controls[0];
                string Perfil = e.Row.Cells[3].Text;
                db.OnClientClick = string.Format("return confirm ('¿Desea eliminar el perfil: {0}?'", Perfil);
            }
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("~/Errores.aspx", false);
        }
    }

    protected void ExportarExcel(object sender, EventArgs e)
    {
        Response.Clear();
        Response.Buffer = true;
        Response.ClearContent();
        Response.ClearHeaders();
        Response.Charset = "";
        string NombreArchivo = "Perfiles Puesto" + DateTime.Now + ".xls";
        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.AddHeader("Content-Disposition", "attachment;filename=" + NombreArchivo);
        gv_PerfilesPuesto.GridLines = GridLines.Both;
        gv_PerfilesPuesto.HeaderStyle.Font.Bold = true;
        gv_PerfilesPuesto.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();

        /*Response.AddHeader("content-disposition", "attachment; filename=gvtoexcel.xls");
        Response.ContentType = "application/vnd.ms-excel";*/
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        //base.VerifyRenderingInServerForm(control);
        return;
    }

    protected void gv_PerfilesPuesto_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gv_PerfilesPuesto.PageIndex = e.NewPageIndex;
        Carga_Lista_Perfiles();
    }
}