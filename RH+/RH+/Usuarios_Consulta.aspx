﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Usuarios_Consulta.aspx.cs" Inherits="Usuarios_Consulta" EnableEventValidation="false" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>Usuarios</h2>
    <br />
    <asp:Button ID="btn_Nuevo" class="btn btn-primary" runat="server" Text="Nuevo" PostBackUrl="~/Usuarios_Nuevo.aspx" />
    &nbsp;
    <asp:Button ID="btn_Excel" runat="server" Text="Excel" class="btn btn-success" OnClick="ExportarExcel" />
    <br /><br />
    <asp:GridView ID="gv_Usuarios" CssClass="table table-striped table-hover table-bordered" runat="server" AutoGenerateColumns="False" AllowPaging="true" PageSize="10" AllowSorting="true" OnRowCommand="gv_Usuarios_RowCommand" OnRowDataBound="gv_Usuarios_RowDataBound" DataKeyNames="Id" OnPageIndexChanging="gv_Usuarios_PageIndexChanging">
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="Id" Visible="False" />
            <asp:BoundField DataField="IdUsuario" HeaderText="IdUsuario" SortExpression="Nombre" />
            <asp:BoundField DataField="Nombre" HeaderText="Nombre" />
            <asp:BoundField DataField="Apellido1" HeaderText="Apellido1" />
            <asp:BoundField DataField="Apellido2" HeaderText="Apellido2" />
            <asp:BoundField DataField="Correo" HeaderText="Correo" />
            <asp:BoundField DataField="IdRol" HeaderText="IdRol" Visible="False" />
            <asp:BoundField DataField="Rol" HeaderText="Rol"></asp:BoundField>
            <asp:BoundField DataField="IdSeccion" HeaderText="IdSeccion" Visible="False" />
            <asp:BoundField DataField="Seccion" HeaderText="Seccion" />
            <asp:CheckBoxField DataField="Estado" HeaderText="Estado" ReadOnly="True" Text="Activo" />
            <asp:HyperLinkField DataNavigateUrlFields="Id,IdUsuario,Nombre,Apellido1,Apellido2,Correo,IdRol,IdSeccion,Estado" DataNavigateUrlFormatString="Usuarios_Nuevo.aspx?Id={0}&amp;IdUsuario={1}&amp;Nombre={2}&amp;Apellido1={3}&amp;Apellido2={4}&amp;Correo={5}&amp;IdRol={6}&amp;IdSeccion={7}&amp;Estado={8}" HeaderText="Editar" Text="Editar" />
            <asp:ButtonField CommandName="Eliminar" HeaderText="Eliminar" Text="Eliminar" />
        </Columns>
    </asp:GridView>
    <br />
</asp:Content>