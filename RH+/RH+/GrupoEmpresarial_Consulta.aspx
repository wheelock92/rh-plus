﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSU.master" AutoEventWireup="true" CodeFile="GrupoEmpresarial_Consulta.aspx.cs" Inherits="GrupoEmpresarial_Consulta" EnableEventValidation="false" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h2>Grupos Empresariales</h2>
    <br />
    <style type="text/css">
        .GridHeader th{text-align:center}
    </style>
    <div class="row margin-bottom-30">
        <div class="col-md-12">
            <div class="table-responsive">
                <asp:Button ID="btn_Nuevo" Text="Nuevo" PostBackUrl="~/GrupoEmpresarial_Editar.aspx" runat="server" class="btn btn-primary" />
                &nbsp;
                <asp:Button ID="btn_Excel" Text="Excel" class="btn btn-success" runat="server" OnClick="ExportarExcel"/>
                <br /><br />
                <asp:GridView ID="gv_GrupoEmpresarial" runat="server"  CssClass="table table-striped table-hover table-bordered GridHeader" AutoGenerateColumns="False" DataKeyNames="IdGrupo" OnRowCommand="gv_GrupoEmpresarial_RowCommand" OnRowDataBound="gv_GrupoEmpresarial_RowDataBound" AllowPaging="true" PageSize="10" AllowSorting="true" OnPageIndexChanging="gv_GrupoEmpresarial_PageIndexChanging">
                    <Columns>
                        <asp:BoundField DataField="IdGrupo" HeaderText="IdGrupo" Visible="False" ItemStyle-Width="100px" />
                        <asp:BoundField DataField="Nombre" HeaderText="Nombre" >
                            <ItemStyle Width="5px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Descripcion" HeaderText="Descripción" >
                            <HeaderStyle Width="50px" />
                        </asp:BoundField>
                        <asp:CheckBoxField DataField="Activo" Text="Activo" >
                            <ItemStyle Width="50px" />
                        </asp:CheckBoxField>
                        <asp:HyperLinkField DataNavigateUrlFields="IdGrupo,Nombre,Descripcion,Activo" Text="Editar" DataNavigateUrlFormatString="GrupoEmpresarial_Editar.aspx?IdGrupo={0}&amp;Nombre={1}&amp;Descripcion={2}&amp;Activo={3}" >
                            <HeaderStyle Width="50px" />
                        </asp:HyperLinkField>
                        <asp:ButtonField CommandName="Eliminar"  Text="Eliminar" >
                            <HeaderStyle Width="50px" />
                        </asp:ButtonField>
                    </Columns>
                    <HeaderStyle HorizontalAlign="Center" />
                </asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>

