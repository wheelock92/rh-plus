﻿using BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SubCategoria_Nuevo : System.Web.UI.Page
{
    SubCategoria_L objSubcategoria = new SubCategoria_L();
    Login_L objLogin = new Login_L();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            int idRol = Convert.ToInt32(Session["IdRol"]);
            objLogin.IdRol = idRol;

            if (objLogin.SuperAdmin() || objLogin.Admin() || objLogin.Editor())
            {
                //si se va a editar aquicarga la informacion en el formulario
                if (!Page.IsPostBack)
                {
                    if (Request.QueryString["IdSubCategoria"] != null)
                    {
                        txt_IdSubCategoria.Text = Request.QueryString["IdSubCategoria"];
                        txt_Nombre.Text = Request.QueryString["Nombre"];
                        if (Request.QueryString["Activo"] == "True")
                        {
                            chk_Activo.Checked = true;
                        }
                        else
                        {
                            chk_Activo.Checked = false;
                        }
                    }
                }
            }
            else
            {
                Session["Error"] = "El usuario no posee los permisos para acceder.";
                Response.Redirect("~/Errores.aspx", false);
            }
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("~/Errores.aspx", false);
        }
    }

    //aqui guarda la información. distingue si es editar o agregar dependiendo de si el campo de Id esta lleno o no.
    private void Guardar()
    {
        try
        {
            objSubcategoria.IdOrganizacion = Convert.ToInt32(Session["IdOrganizacion"]);
            objSubcategoria.Nombre = txt_Nombre.Text;
            if (chk_Activo.Checked)
            {
                objSubcategoria.Activo = true;
            }
            else
            {
                objSubcategoria.Activo = false;
            }
            if (txt_IdSubCategoria.Text != string.Empty)
            {
                objSubcategoria.IdSubCategoria = Convert.ToInt32(txt_IdSubCategoria.Text);
                if (objSubcategoria.Editar_SubCategoria())
                {
                    Response.Redirect("~/SubCategoria_Consulta.aspx", false);
                }
                else
                {
                    Session["Error"] = "No se pudo editar la información. Favor contacte al administrador";
                    Response.Redirect("~/Errores.aspx", false);
                }
            }
            else
            {
                if (objSubcategoria.Agrega_SubCategoria())
                {
                    Response.Redirect("~/SubCategoria_Consulta.aspx", false);
                }
                else
                {
                    Session["Error"] = "No se pudo guardar la información. Favor contacte al administrador";
                    Response.Redirect("~/Errores.aspx", false);
                }
            }
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("~/Errores.aspx", false);
        }
    }

    protected void btn_Guardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }
}