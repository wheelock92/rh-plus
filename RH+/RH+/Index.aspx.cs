﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using System.IO;
using System.Configuration;

public partial class _Default : System.Web.UI.Page
{
    Logo_L objLogo = new Logo_L();
    byte[] Logo_Empresa;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Logo.ImageUrl = "~/ShowImage.cs?IdOrganizacion" + Request.QueryString["IdOrganizacion"];
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("Errores.aspx");
        }
    }
}