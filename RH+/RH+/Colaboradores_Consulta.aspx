﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Colaboradores_Consulta.aspx.cs" Inherits="Colaboradores_Consulta" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head2" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <style type="text/css">
        .GridHeader th{text-align:center}
        .HideCol {display:none}
    </style>

    <h2>Colaboradores</h2>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <asp:Button ID="btn_Nuevo" runat="server" CssClass="btn btn-primary" PostBackUrl="~/Colaboradores_Nuevo.aspx" Text="Nuevo" />
                &nbsp;
                <asp:Button ID="btn_Excel" runat="server" CssClass="btn btn-success" PostBackUrl="#" Text="Excel" />
                <br /><br />
                <asp:GridView ID="gv_Colaboradores" CssClass="table table-striped table-hover table-bordered" AutoGenerateColumns="false" DataKeyNames="Identificacion" AllowPaging="true" runat="server" OnRowCommand="gv_Colaboradores_RowCommand" OnRowDataBound="gv_Colaboradores_RowDataBound">
                    <Columns>
                        <asp:BoundField DataField="Identificacion" HeaderText="Identificación" />
                        <asp:BoundField DataField="Nombre" HeaderText="Nombre" ItemStyle-CssClass="HideCol" HeaderStyle-CssClass="HideCol" />
                        <asp:TemplateField HeaderText="Nombre">
                            <ItemTemplate>
                                <asp:Label ID="lbl_Nombre" runat="server" Text='<%#Eval("Nombre")+" "+Eval("Apellido1")+" "+Eval("Apellido2") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="FechaNacimiento" HeaderText="Fecha de Nacimiento" DataFormatString="{0:dd/MM/yyy}" />
                        <asp:BoundField DataField="Codigo" HeaderText="Código" />
                        <asp:BoundField DataField="Puesto" HeaderText="Puesto" ItemStyle-CssClass="HideCol" HeaderStyle-CssClass="HideCol" />
                        <asp:BoundField DataField="NombrePuesto" HeaderText="Puesto" />
                        <asp:BoundField DataField="FechaContratacion" HeaderText="Fecha de Contratación" DataFormatString="{0:dd/MM/yyy}" />
                        <asp:BoundField DataField="FechaVinculacion" HeaderText="Vinculacion Al Puesto" DataFormatString="{0:dd/MM/yyy}" />
                        <asp:CheckBoxField DataField="Estado" HeaderText="Activo" />
                        <asp:BoundField DataField="IdOrganizacion" HeaderText="IdOrganizacion" ItemStyle-CssClass="HideCol" HeaderStyle-CssClass="HideCol" />
                        <asp:BoundField DataField="Apellido1" HeaderText="Apellido1" ItemStyle-CssClass="HideCol" HeaderStyle-CssClass="HideCol" />
                        <asp:BoundField DataField="Apellido2" HeaderText="Apellido1" ItemStyle-CssClass="HideCol" HeaderStyle-CssClass="HideCol" />
                        <asp:ButtonField CommandName="Editar" HeaderText="Editar" Text="Editar" />
                        <asp:ButtonField CommandName="Eliminar" HeaderText="Eliminar" Text="Eliminar" />
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>

