﻿using BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Correo_Editar : System.Web.UI.Page
{
    Correo_L objCorreo = new Correo_L();
    Login_L objLogin = new Login_L();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            chk_Autenticación.Checked = true;

            int idRol = Convert.ToInt32(Session["IdRol"]);
            objLogin.IdRol = idRol;

            if (objLogin.SuperAdmin() || objLogin.Editor() || objLogin.Admin())
            {
                if (!Page.IsPostBack)
                {
                    txt_Contrasena.Text = Request.QueryString["ContrasenaCorreo"];
                    txt_CorreoNotificaciones.Text = Request.QueryString["CorreoNotificaciones"];
                    txt_IdCorreo.Text = Request.QueryString["IdCorreo"];
                    txt_Observaciones.Text = Request.QueryString["Observaciones"];
                    txt_Puerto.Text = Request.QueryString["Puerto"];
                    txt_ServidorSMTP.Text = Request.QueryString["ServidorSMTP"];
                    txt_POP3.Text = Request.QueryString["POP3"];
                    txt_Usuario.Text = Request.QueryString["Usuario"];
                    if (Request.QueryString["SSL"] == "True")
                    {
                        chk_SSL.Checked = true;
                    }
                    else
                    {
                        chk_SSL.Checked = false;
                    }

                    if (Request.QueryString["Autenticacion"] == "True")
                    {
                        chk_Autenticación.Checked = true;
                    }
                    else
                    {
                        chk_Autenticación.Checked = false;
                    }
                }
            }
            else
            {
                Session["Error"] = "El usuario no posee los permisos para acceder.";
                Response.Redirect("Errores.aspx");
            }
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("Errores.aspx", false);
        }
    }

    private void Guardar()
    {
        try
        {
            objCorreo.ContrasenaCorreo = txt_Contrasena.Text;
            objCorreo.CorreoNotificaciones = txt_CorreoNotificaciones.Text;
            objCorreo.IdOrganizacion = Convert.ToInt32(Session["IdOrganizacion"]);
            objCorreo.Observaciones = txt_Observaciones.Text;
            objCorreo.Puerto = Convert.ToInt32(txt_Puerto.Text);
            objCorreo.ServidorSMTP = txt_ServidorSMTP.Text;
            objCorreo.POP3 = txt_POP3.Text;
            objCorreo.Usuario = txt_Usuario.Text;

            if (chk_SSL.Checked)
            {
                objCorreo.SSL = true;
            }
            else
            {
                objCorreo.SSL = false;
            }
            if (chk_Autenticación.Checked)
            {
                objCorreo.Autenticacion = true;
            }
            else
            {
                objCorreo.Autenticacion = false;
            }

            if (txt_IdCorreo.Text != string.Empty)
            {
                objCorreo.IdCorreo = Convert.ToInt32(txt_IdCorreo.Text);
                if (objCorreo.Editar_Correo())
                {
                    Response.Redirect("~/Correo_Consulta.aspx", false);
                }
                else
                {
                    string error = "Ha ocurrido un error al editar en la BD. Favor contacte al administrador.";
                    Response.Redirect("~/Errores.aspx?Error" + error, false);
                }
            }
            else
            {
                if (objCorreo.Nuevo_Correo())
                {
                    Response.Redirect("~/Correo_Consulta.aspx", false);
                }
                else
                {
                    string error = "Ha ocurrido un error al editar en la BD. Favor contacte al administrador.";
                    Response.Redirect("~/Errores.aspx?Error" + error, false);
                }
            }
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("Errores.aspx", false);
        }
    }
    protected void btn_Guardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }
}