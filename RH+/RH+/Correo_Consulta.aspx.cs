﻿using BLL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Correo_Consulta : System.Web.UI.Page
{
    Correo_L objCorreo = new Correo_L();
    Login_L objLogin = new Login_L();

    protected void Page_Load(object sender, EventArgs e)
    {
        int idRol = Convert.ToInt32(Session["IdRol"]);
        objLogin.IdRol = idRol;

        if (objLogin.SuperAdmin() || objLogin.Admin() || objLogin.Editor() || objLogin.Lector())
        {
            Carga_Lista_Correo();
        }
        else
        {
            Session["Error"] = "El usuario no posee los permisos para acceder.";
            Response.Redirect("Errores.aspx");
        }
    }

    private void Carga_Lista_Correo()
    {
        try
        {
            objCorreo.IdOrganizacion = Convert.ToInt32(Session["IdOrganizacion"]);
            gv_Correo.DataSource = objCorreo.Correo_Organizacion();
            gv_Correo.DataBind();
        }
        catch (Exception ex)
        {
            Session["Error"] = ex;
            Response.Redirect("Errores.aspx", false);
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        
    }
    protected void gv_Correo_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton db = (LinkButton)e.Row.Cells[12].Controls[0];
                db.OnClientClick = string.Format("return confirm ('¿Desea eliminar el correo seleccionado?');");
            }
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("Errores.aspx", false);
        }
    }
    protected void gv_Correo_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Eliminar")
            {
                int fila = Convert.ToInt32(e.CommandArgument);
                int IdCorreo = Convert.ToInt32(gv_Correo.DataKeys[fila].Value);
                if (objCorreo.Elimina_Correo(IdCorreo))
                {
                    this.Carga_Lista_Correo();
                }
            }
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("Errores.aspx");
        }
    }

    protected void ExportarExcel(object sender, EventArgs e)
    {
        Response.Clear();
        Response.Buffer = true;
        Response.ClearContent();
        Response.ClearHeaders();
        Response.Charset = "";
        string NombreArchivo = "Servidores Correo" + DateTime.Now + ".xls";
        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.AddHeader("Content-Disposition", "attachment;filename=" + NombreArchivo);
        gv_Correo.GridLines = GridLines.Both;
        gv_Correo.HeaderStyle.Font.Bold = true;
        gv_Correo.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();

        /*Response.AddHeader("content-disposition", "attachment; filename=gvtoexcel.xls");
        Response.ContentType = "application/vnd.ms-excel";*/
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        //base.VerifyRenderingInServerForm(control);
        return;
    }

    protected void gv_Correo_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gv_Correo.PageIndex = e.NewPageIndex;
        Carga_Lista_Correo();
    }
}