﻿using BLL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Organizacion_Consulta : System.Web.UI.Page
{
    Organizacion_L objOrganizacion = new Organizacion_L();
    Login_L objLogin = new Login_L();

    protected void Page_Load(object sender, EventArgs e)
    {
        int idRol;
        
        idRol = Convert.ToInt32(Session["IdRol"]);
        objLogin.IdRol = idRol;

        if (objLogin.SuperAdmin())
        {
            Carga_Lista_Organizacion();
        }
        else
        {
            Session["Error"] = "El usuario no posee los permisos para acceder.";
            Response.Redirect("~/ErroresSU.aspx", false);
        }
    }

    private void Carga_Lista_Organizacion()
    {
        try
        {
            gv_Organizacion.DataSource = objOrganizacion.Carga_Lista_Organizacion();
            gv_Organizacion.DataBind();
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("~/ErroresSU.aspx", false);
        }
    }
    protected void gv_Organizacion_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton db = (LinkButton)e.Row.Cells[16].Controls[0];
                string Organizacion = e.Row.Cells[1].Text;
                db.OnClientClick = string.Format("return confirm('¿Desea eliminar la organizacion: {0}';", Organizacion);
            }
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("~/ErroresSU.aspx", false);
        }
    }
    protected void gv_Organizacion_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            int fila = Convert.ToInt32(e.CommandArgument);
            if (e.CommandName == "Eliminar")
            {
                int IdOrganizacion = Convert.ToInt32(gv_Organizacion.DataKeys[fila].Value);
                if (objOrganizacion.Eliminar_Organizacion(IdOrganizacion))
                {
                    this.Carga_Lista_Organizacion();
                }
            }
            else
            {
                if (e.CommandName == "Reset")
                {
                    objOrganizacion.IdUsuarioAdmin = Convert.ToInt32(gv_Organizacion.Rows[fila].Cells[6].Text);
                    objOrganizacion.Clave = "123456";
                    if (objOrganizacion.ResetClave())
                    {
                        this.Carga_Lista_Organizacion();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("~/ErroresSU.aspx", false);
        }
    }

    protected void ExportarExcel(object sender, EventArgs e)
    {
        Response.Clear();
        Response.Buffer = true;
        Response.ClearContent();
        Response.ClearHeaders();
        Response.Charset = "";
        string NombreArchivo = "Organizaciones" + DateTime.Now + ".xls";
        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.AddHeader("Content-Disposition", "attachment;filename=" + NombreArchivo);
        gv_Organizacion.GridLines = GridLines.Both;
        gv_Organizacion.HeaderStyle.Font.Bold = true;
        gv_Organizacion.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();

        /*Response.AddHeader("content-disposition", "attachment; filename=gvtoexcel.xls");
        Response.ContentType = "application/vnd.ms-excel";*/
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        //base.VerifyRenderingInServerForm(control);
        return;
    }

    protected void gv_Organizacion_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gv_Organizacion.PageIndex = e.NewPageIndex;
        Carga_Lista_Organizacion();
    }
}