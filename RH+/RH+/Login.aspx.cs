﻿using BLL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Login : System.Web.UI.Page
{
    Login_L objLogin = new Login_L();
    Encriptar_Clave objClave = new Encriptar_Clave();

    string IdUsuario;
    string Clave;
    int IdRol;
    int IdOrganizacion;

    protected void Page_Load(object sender, EventArgs e)
    {
        txt_Login.Focus();
    }

    protected void VerificaUsuario()
    {
        try
        {
            DataTable dt = new DataTable();
            DataTable rol = new DataTable();

            IdUsuario = txt_Login.Text;
            Clave = txt_Clave.Text;
            string clave_encriptada = BLL.Encriptar_Clave.generateHashDigest(Clave, Encriptar_Clave.HashMethod.MD5);

            if (objLogin.Ingresar(IdUsuario, Clave) != null)
            {
                bool EncuentraUsuario = true;

                if (IdUsuario == "superadmin" && clave_encriptada == "F8RSD2z9GrU9h0XoRoHrSQ==")
                {
                    Session["IdRol"] = 1;
                    Response.Redirect("~/Organizacion_Consulta.aspx", false);
                }
                else
                {
                    dt = objLogin.Ingresar(IdUsuario, Clave);

                    for (int rows = 0; rows < dt.Rows.Count; rows++)
                    {
                        if (IdUsuario == dt.Rows[rows][0].ToString() && BLL.Encriptar_Clave.generateHashDigest(Clave, Encriptar_Clave.HashMethod.MD5) == dt.Rows[rows][1].ToString())
                        {
                            if (Convert.ToBoolean(dt.Rows[rows][6]))
                            {
                                if (Convert.ToBoolean(dt.Rows[rows][5]))
                                {
                                    IdRol = Convert.ToInt32(dt.Rows[rows][2].ToString());
                                    Session["IdRol"] = IdRol;
                                    IdOrganizacion = Convert.ToInt32(dt.Rows[rows][4].ToString());
                                    Session["IdOrganizacion"] = IdOrganizacion;
                                    Session["Usuario"] = dt.Rows[rows][3].ToString();
                                    int Id = Convert.ToInt32(dt.Rows[rows][7].ToString());
                                    Session["Id"] = Id;
                                    //Session["Link"] = dt.Rows[rows][4].ToString();
                                    //int idOrganizacion = Convert.ToInt32(dt.Rows[rows][3].ToString());
                                    //string link = dt.Rows[rows][4].ToString();
                                    if (IdRol == 1)
                                    {
                                        Response.Redirect("~/Organizacion_Consulta.aspx", false);
                                        EncuentraUsuario = true;
                                        break;
                                    }
                                    else
                                    {
                                        Response.Redirect("~/Index.aspx", false);
                                        EncuentraUsuario = true;
                                        break;
                                    }
                                }
                                else
                                {
                                    ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Usuario inactivo. Contacte al administrador')</script>");
                                    break;
                                }
                            }
                            else
                            {
                                ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Organización inactiva. Favor contacte al administrador.')</script>");
                                break;
                            }
                        }
                        else
                        {
                            EncuentraUsuario = false;
                        }
                    }

                    if (!EncuentraUsuario)
                    {
                        ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Usuario y Contraseña no válidos.')</script>");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("~/Errores.aspx", false);
        }
    }
    protected void btn_Aceptar_Click(object sender, EventArgs e)
    {
        VerificaUsuario();
    }
}