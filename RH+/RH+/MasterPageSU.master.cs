﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MasterPageSU : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        MenuRoles();
    }

    private void MenuRoles()
    {
        int idRol = Convert.ToInt32(Session["IdRol"]);

        if (idRol != 1)
        {
            Session["Error"] = "El usuario no posee los permisos para acceder.";
            Response.Redirect("Errores.aspx", false);
        }

    }

    private void Cerrar()
    {
        Session.Clear();
        Session.RemoveAll();
        Session.Abandon();
    }

    public void CerrarSesion_Click(Object sender, EventArgs e)
    {
        Session.Clear();
        Session.RemoveAll();
        Session.Abandon();
        Response.Redirect("~/Login.aspx");
    }
}
