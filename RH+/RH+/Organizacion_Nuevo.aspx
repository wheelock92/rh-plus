<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSU.master" AutoEventWireup="true" CodeFile="Organizacion_Nuevo.aspx.cs" Inherits="Organizacion_Nuevo" EnableEventValidation="false" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div class="row">
        <div class="col-md-12">
            <form role="form" id="templatemo-preferences-form">
                <div class="row">
                    <div class="col-md-12 margin-bottom-15">
                        <label for="Organización">Nueva Organización</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 margin-bottom-15">
                        <label for="firstName" class="control-label">Nombre</label>
                        <asp:TextBox  class="form-control" ID="txt_NombreEmpresa" runat="server" MaxLength="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="val_NombreOrganizacion" ErrorMessage="Ingrese el nombre de la Organización" SetFocusOnError="true"
                            ControlToValidate="txt_NombreEmpresa" runat="server" Display="None"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-6 margin-bottom-15">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value=""/>
                                <asp:CheckBox ID="chk_Activa" Text="Activa" Checked="true" runat="server" />
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 margin-bottom-15">
                        <div class="dropdownlist">
                            <label>
                                <asp:CheckBox ID="chk_Grupo_Empresarial" Text="Grupo empresarial" Checked="false" runat="server" OnCheckedChanged="chk_Grupo_Empresarial_CheckedChanged" AutoPostBack="true"/>
                            </label>
                            <asp:DropDownList ID="ddl_GrupoEmpresarial" runat="server" DataSourceID="DS_GrupoEmpresarial" DataTextField="Nombre" DataValueField="IdGrupo" Visible="false"></asp:DropDownList>
                            <asp:SqlDataSource ID="DS_GrupoEmpresarial" runat="server" ConnectionString="<%$ ConnectionStrings:RH+ %>" SelectCommand="SELECT [IdGrupo], [Nombre] FROM [GrupoEmpresarial]"></asp:SqlDataSource>
                        </div>
                    </div>
                    <div class="col-md-6 margin-bottom-15">
                        <label for="cantidad usuarios" class="control-label">Cantidad de usuarios</label>
                        <asp:TextBox  class="form-control" ID="txt_Cantidad_Usuarios" TextMode="Number" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="val_CantidadUsuarios" runat="server" ErrorMessage="Favor ingrese la cantidad de usuarios." SetFocusOnError="true"
                            Display="None" ControlToValidate="txt_Cantidad_Usuarios"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="val_CantidadUsuariosNum" runat="server" ErrorMessage="La cantidad debe ser ingresada en números." SetFocusOnError="true"
                            ControlToValidate="txt_Cantidad_Usuarios" Type="Integer" Operator="DataTypeCheck"></asp:CompareValidator>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 margin-bottom-15">
                        <label for="lastName" class="control-label">Logo</label>
                        <asp:FileUpload  class="form-control" ID="fu_Logo" runat="server" />
                    </div>
                    <div class="col-md-6 margin-bottom-15">
                        <label for="notes">Observaciones</label>
                        <asp:TextBox  class="form-control" ID="txt_Observaciones" runat="server" Rows="3" MaxLength="100"></asp:TextBox>
                    </div>
                </div>

                <br />
                <!--Formulario del usuario administrador-->
                <div class="row">
                    <div class="col-md-12 margin-bottom-15">
                        <asp:Button CssClass="btn btn-danger" runat="server" Text="Reset Clave Administrador" OnClick="Btn_Reset_Click" ID="btn_Reset" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 margin-bottom-15">
                        <label for="Usuario Administrador">Usuario Administrador</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 margin-bottom-15">
                        <label for="Nombre" class="control-label">Nombre</label>
                        <asp:TextBox ID="txt_NombreAdmin" class="form-control" runat="server" MaxLength="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="val_NombreAdmin" ControlToValidate="txt_NombreAdmin" runat="server" ErrorMessage="Ingrese el nombre del Usuario"
                            Display="None" Enabled="false" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-6 margin-bottom-15">
                        <label for="Apellido 1" class="control-label">Primer Apellido</label>
                        <asp:TextBox ID="txt_Apellido1" class="form-control" runat="server" MaxLength="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="val_Apellido1" ControlToValidate="txt_Apellido1" runat="server" ErrorMessage="Ingrese el primer apellido del usuario."
                            Display="None" Enabled="false" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 margin-bottom-15">
                        <label for="Apellido 2" class="control-label">Segundo Apellido</label>
                        <asp:TextBox ID="txt_Apellido2" class="form-control" runat="server" MaxLength="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="val_Apellido2" ControlToValidate="txt_Apellido2" runat="server" ErrorMessage="Ingrese el segundo apellido del usuario."
                            Display="None" Enabled="false" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-6 margin-bottom-15">
                        <label class="control-label" for="Login">Username</label>
                        <asp:TextBox ID="txt_Login" class="form-control" runat="server" MaxLength="100"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="val_Login" ControlToValidate="txt_Login" runat="server" ErrorMessage="Ingrese el Username" Display="None"
                            Enabled="false" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 margin-bottom-15">
                        <label for="Correo" class="control-label">Correo electrónico</label>
                        <asp:TextBox ID="txt_Correo" TextMode="Email" class="form-control" runat="server" MaxLength="100"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="val_Correo" ControlToValidate="txt_Correo" ErrorMessage="Ingrese el correo del usuario" Display="None"
                            Enabled="false" runat="server" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <asp:Button ID="btn_Guardar" Text="Guardar" class="btn btn-primary" runat="server" OnClick="btn_Guardar_Click" />
    &nbsp;
    <%--<asp:Button ID="btn_Editar" Text="Editar" CssClass="btn btn-success" runat="server" />--%>
    <asp:ValidationSummary ID="val_Muestra" DisplayMode="BulletList" runat="server" ShowMessageBox="true" ShowSummary="false" />
</asp:Content>