﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;

public partial class Usuarios_Consulta : System.Web.UI.Page
{
    Usuarios_L objUsuarios = new Usuarios_L();
    int IdOrganizacion = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        IdOrganizacion = Convert.ToInt32(Session["IdOrganizacion"]);
        Carga_Lista_Usuarios();
    }

    /// <summary>
    /// carga la información de los usuarios de la organizacion en el gridview.
    /// </summary>
    private void Carga_Lista_Usuarios()
    {
        try
        {
            objUsuarios.IdOrganizacion = IdOrganizacion;
            gv_Usuarios.DataSource = objUsuarios.Usuarios_Organizacion();
            gv_Usuarios.DataBind();
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("~/Errores.aspx", false);
        }
    }

    /// <summary>
    /// código que exporta la información del gridview a un documento de excel que se descarga desde el navegador.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ExportarExcel(object sender, EventArgs e)
    {
        Response.Clear();
        Response.Buffer = true;
        Response.ClearContent();
        Response.ClearHeaders();
        Response.Charset = "";
        string NombreArchivo = "Usuarios" + DateTime.Now + ".xls";
        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.AddHeader("Content-Disposition", "attachment;filename=" + NombreArchivo);
        gv_Usuarios.GridLines = GridLines.Both;
        gv_Usuarios.HeaderStyle.Font.Bold = true;
        gv_Usuarios.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();

        /*Response.AddHeader("content-disposition", "attachment; filename=gvtoexcel.xls");
        Response.ContentType = "application/vnd.ms-excel";*/
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        //base.VerifyRenderingInServerForm(control);
        return;
    }


    /// <summary>
    /// accion para seleccionar en el gridview un usuario que se quiere eliminar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gv_Usuarios_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton db = (LinkButton)e.Row.Cells[12].Controls[0];
                string usuario = e.Row.Cells[1].Text;
                db.OnClientClick = string.Format("return confirm ('¿Desea eliminar El usuario: {0}');", usuario);
            }
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("~/Errores.aspx", false);
        }
    }

    /// <summary>
    /// ejecuta el codigo para eliminar el usuario seleccionado en el gridview
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gv_Usuarios_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Eliminar")
            {
                int fila = Convert.ToInt32(e.CommandArgument);
                int IdUsuario = Convert.ToInt32(gv_Usuarios.DataKeys[fila].Value);
                if (objUsuarios.Eliminar_Usuario(IdUsuario))
                {
                    this.Carga_Lista_Usuarios();
                }
            }
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("~/Errores.aspx", false);
        }
    }

    /// <summary>
    /// genera un índice de paginación en el gridview.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gv_Usuarios_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gv_Usuarios.PageIndex = e.NewPageIndex;
        Carga_Lista_Usuarios();
    }
}