﻿using BLL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SubCategoria_Consulta : System.Web.UI.Page
{
    SubCategoria_L objSubcategoria = new SubCategoria_L();

    protected void Page_Load(object sender, EventArgs e)
    {
        Carga_Lista_Subcategoria();
    }

    /// <summary>
    /// realiza la consulta y carga la información al grid view de subcategoria
    /// </summary>
    private void Carga_Lista_Subcategoria()
    {
        try
        {
            objSubcategoria.IdOrganizacion = Convert.ToInt32(Session["IdOrganizacion"]);
            gv_SubCategoria.DataSource = objSubcategoria.Subcategoria_Organizacion();
            gv_SubCategoria.DataBind();
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("Errores.aspx", false);
        }
    }

    protected void gv_SubCategoria_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton db = (LinkButton)e.Row.Cells[5].Controls[0];
                string SubCategoria = e.Row.Cells[1].Text;
                db.OnClientClick = string.Format("return confirm ('¿Desea eliminar la SubCategoria: {0}');", SubCategoria);
            }
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("~/Errores.aspx", false);
        }
    }

    protected void gv_SubCategoria_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Eliminar")
            {
                int fila = Convert.ToInt32(e.CommandArgument);
                int IdSubCategoria = Convert.ToInt32(gv_SubCategoria.DataKeys[fila].Value);
                if (objSubcategoria.Eliminar_SubCategoria(IdSubCategoria))
                {
                    this.Carga_Lista_Subcategoria();
                }
            }
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("~/Errores.aspx", false);
        }
    }

    /// <summary>
    /// exporta la información del gridview a un excel
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ExportarExcel(object sender, EventArgs e)
    {
        Response.Clear();
        Response.Buffer = true;
        Response.ClearContent();
        Response.ClearHeaders();
        Response.Charset = "";
        string NombreArchivo = "Subcategorias" + DateTime.Now + ".xls";
        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.AddHeader("Content-Disposition", "attachment;filename=" + NombreArchivo);
        gv_SubCategoria.GridLines = GridLines.Both;
        gv_SubCategoria.HeaderStyle.Font.Bold = true;
        gv_SubCategoria.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();

        /*Response.AddHeader("content-disposition", "attachment; filename=gvtoexcel.xls");
        Response.ContentType = "application/vnd.ms-excel";*/
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        //base.VerifyRenderingInServerForm(control);
        return;
    }

    protected void gv_SubCategoria_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gv_SubCategoria.PageIndex = e.NewPageIndex;
        Carga_Lista_Subcategoria();
    }
}