﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Errores_aspx : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //lblMensaje.Text = Convert.ToString(Request.QueryString["men"]);
        if (Session["Error"] != null)
        {
            lblMensaje.Text = "Ocurrio un error:   " + Session["Error"];
            lblNumero.Text = "";
        }

        if (Request.QueryString["Error"] != null)
        {
            lblNumero.Text = "Ocurrio un error:    " + Request.QueryString["Error"].ToString();
        }
    }
}