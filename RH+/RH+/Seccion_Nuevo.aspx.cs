﻿using BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Seccion_Nuevo : System.Web.UI.Page
{
    Secciones_L objSeccion = new Secciones_L();
    Login_L objLogin = new Login_L();
    string Accion = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        objLogin.IdRol=Convert.ToInt32(Session["IdRol"]);
        
        if (!objLogin.SuperAdmin() && !objLogin.Admin() && !objLogin.Editor())
	    {
            Session["Error"] = "El usuario no posee los permisos para acceder.";
            Response.Redirect("Errores.aspx");
	    }

        if (!Page.IsPostBack)
        {
            if (Request.QueryString["IdSeccion"] != null)
            {
                txt_nombre.Text = Request.QueryString["Nombre"];
                Accion = "Editar";
                if (Request.QueryString["Activo"] == "True")
                {
                    chk_Activo.Checked = true;
                }
                else
                {
                    chk_Activo.Checked = false;
                }
            }
        }
        else
        {
            Accion = "Nuevo";
        }
    }

    private void Guardar()
    {
        try
        {
            int idSeccion = Convert.ToInt32(Request.QueryString["IdSeccion"]);
            objSeccion.Nombre = txt_nombre.Text;
            if (chk_Activo.Checked)
            {
                objSeccion.Activo = true;
            }
            else
            {
                objSeccion.Activo = false;
            }
            objSeccion.IdOrganizacion = Convert.ToInt32(Session["IdOrganizacion"]);
            if (idSeccion != 0)
            {
                objSeccion.IdSeccion = Convert.ToInt32(Request.QueryString["IdSeccion"]);

                if (objSeccion.Editar_Seccion())
                {
                    Response.Redirect("Seccion_Consulta2.aspx", false);
                }
                else
                {
                    Session["Error"] = "Error al actualizar la información. Favor contacte al administrador.";
                    Response.Redirect("~/Errores.aspx", false);
                }
            }
            else
            {
                if (objSeccion.agrega_seccion())
                {
                    Response.Redirect("Seccion_Consulta2.aspx", false);
                }
                else
                {
                    Session["Error"] = "Error al insertar la información. Favor contacte al administrador.";
                    Response.Redirect("~/Errores.aspx", false);
                }
            }
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("~/Errores.aspx", false);
        }
    }
    protected void btn_Guardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }
}