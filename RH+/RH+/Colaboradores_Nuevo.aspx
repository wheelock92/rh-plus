﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Colaboradores_Nuevo.aspx.cs" Inherits="Colaboradores_Nuevo" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head2" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form role="form" id="templatemo-preferences-form">
        <div class="row">
            <div class="col-md-6 margin-bottom-15">
                <label for="Identificacion" class="control-label">Identificaci&oacute;n</label>
                <asp:TextBox CssClass="form-control" ID="txt_Identificacion" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="val_Identicacion" ControlToValidate="txt_Identificacion" Display="None" runat="server" ErrorMessage="Ingrese la identificacion del colaborador"></asp:RequiredFieldValidator>
            </div>
            <div class="col-md-6 margin-bottom-15">
                <label for="Nombre" class="control-label">Nombre</label>
                <asp:TextBox CssClass="form-control" ID="txt_Nombre" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="val_Nombre" ControlToValidate="txt_Nombre" Display="None" runat="server" ErrorMessage="Ingrese el nombre del colaborador."></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 margin-bottom-15">
                <label for="Apellido1" class="control-label">Primer Apellido</label>
                <asp:TextBox ID="txt_Apellido1" CssClass="form-control" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="val_Apellido1" ControlToValidate="txt_Apellido1" Display="None" runat="server" ErrorMessage="Ingrese el primer apellido del colaborador." CssClass="alert-danger"></asp:RequiredFieldValidator>
            </div>
            <div class="col-md-6 margin-bottom-15">
                <label for="Apellido2" class="control-label">Segundo Apellido</label>
                <asp:TextBox ID="txt_Apellido2" CssClass="form-control" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="val_Apellido2" ControlToValidate="txt_Apellido2" Display="None" runat="server" ErrorMessage="Ingrese el segundo apellido del colaborador." CssClass="alert-danger"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 margin-bottom-15">
                <label for="Puesto" class="control-label">Puesto</label>
                <asp:DropDownList ID="ddl_Puesto" runat="server" CssClass="form-control margin-bottom-15" DataTextField="NombrePuesto" DataValueField="IdPerfil"></asp:DropDownList>
            </div>
            <div class="col-md-6 margin-bottom-15">
                <label for="Codigo" class="control-label">C&oacute;digo</label>
                <asp:TextBox ID="txt_Codigo" runat="server" TextMode="Number" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ID="val_Codigo" ControlToValidate="txt_Codigo" runat="server" Display="None" ErrorMessage="Ingrese el codigo del colaborador"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 margin-bottom-15">
                <label for="FechaContratacion" class="control-label">Fecha de Contrataci&oacute;n</label>
                <asp:Calendar ID="cal_FechaContratacion" runat="server"></asp:Calendar>
            </div>
            <div class="col-md-6 margin-bottom-15">
                <label for="FechaNacimiento" class="control-label">Fecha de Nacimiento</label>
                <asp:Calendar ID="cal_FechaNacimiento" runat="server"></asp:Calendar>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 margin-bottom-15">
                <label for="FechaVinculacion" class="control-label">Fecha de Vinculaci&oacute;n al Puesto</label>
                <asp:Calendar ID="cal_FechaVinculacion" runat="server"></asp:Calendar>
            </div>
            <div class="col-md-6 margin-bottom-15">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" value="" />
                        <asp:CheckBox ID="chk_Estado" Text="Activo" Checked="true" runat="server" />
                    </label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 margin-bottom-15">
                <asp:Button ID="btn_Guardar" Text="Guardar" runat="server" OnClick="btn_Guardar_Click" CssClass="btn btn-primary" />
            </div>
        </div>
    </form>
</asp:Content>

