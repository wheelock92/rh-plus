﻿using BLL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class GrupoEmpresarial_Consulta : System.Web.UI.Page
{
    GrupoEmpresarial_L objGrupo = new GrupoEmpresarial_L();
    Login_L objLogin = new Login_L();

    protected void Page_Load(object sender, EventArgs e)
    {
        objLogin.IdRol = Convert.ToInt32(Session["IdRol"]);
        if (objLogin.SuperAdmin())
        {
            Carga_Lista_Grupos();
        }
        else
        {
            Session["Error"] = "El usuario no posee los permisos para accceder.";
            Response.Redirect("~/ErroresSU.aspx", false);
        }
    }

    private void Carga_Lista_Grupos()
    {
        try
        {
            gv_GrupoEmpresarial.DataSource = objGrupo.Carga_Lista_Grupos();
            gv_GrupoEmpresarial.DataBind();
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("~/ErroresSU.aspx", false);
        }
    }
    protected void gv_GrupoEmpresarial_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton db = (LinkButton)e.Row.Cells[5].Controls[0];
                string grupo = e.Row.Cells[1].Text;
                db.OnClientClick = string.Format("return confirm ('¿Desea eliminar el Grupo Empresarial: {0}?');", grupo);
            }
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("~/ErroresSU.aspx", false);
        }
    }
    protected void gv_GrupoEmpresarial_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Eliminar")
            {
                int fila = Convert.ToInt32(e.CommandArgument);
                int IdGrupo = Convert.ToInt32(gv_GrupoEmpresarial.DataKeys[fila].Value);

                if (objGrupo.Eliminar_Grupo(IdGrupo))
                {
                    this.Carga_Lista_Grupos();
                }
                else
                {
                    string error = "No se ha podido eliminar el grupo seleccionada. Favor contacte al administrador";
                    Response.Redirect("~/ErroresSU.aspx?Error" + error, false);
                }
            }
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("~/ErroresSU.aspx", false);
        }
    }

    protected void ExportarExcel(object sender, EventArgs e)
    {
        Response.Clear();
        Response.Buffer = true;
        Response.ClearContent();
        Response.ClearHeaders();
        Response.Charset = "";
        string NombreArchivo = "Grupos Empresariales" + DateTime.Now + ".xls";
        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.AddHeader("Content-Disposition", "attachment;filename=" + NombreArchivo);
        gv_GrupoEmpresarial.GridLines = GridLines.Both;
        gv_GrupoEmpresarial.HeaderStyle.Font.Bold = true;
        gv_GrupoEmpresarial.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();

        /*Response.AddHeader("content-disposition", "attachment; filename=gvtoexcel.xls");
        Response.ContentType = "application/vnd.ms-excel";*/
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        //base.VerifyRenderingInServerForm(control);
        return;
    }

    protected void gv_GrupoEmpresarial_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gv_GrupoEmpresarial.PageIndex = e.NewPageIndex;
        Carga_Lista_Grupos();
    }
}