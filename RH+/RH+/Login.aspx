﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" EnableEventValidation="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>RH Plus</title>
    <link href="css/templatemo_main.css" rel="stylesheet" />
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <link href="css/templatemo_main.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server" role="form">
        <div id="main-wrapper">
            <div class="navbar navbar-inverse" role="navigation">
                <div class="navbar-header">
                    <div class="logo"><h1>RH Plus</h1></div>
                </div>   
            </div>
            <table style="margin-top:150px; margin-left:550px">
                <tr>
                    <td><label for="login" class="control-label">Login</label></td>
                    <td><asp:TextBox ID="txt_Login" class="form-control" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td><label for="clave" class="control-label">Clave</label></td>
                    <td><asp:TextBox ID="txt_Clave" class="form-control" TextMode="Password" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align:center; padding:10px"><asp:Button ID="btn_Aceptar" Text="Aceptar" runat="server" CssClass="btn btn-primary" OnClick="btn_Aceptar_Click" /></td>
                </tr>
                <!--<tr>
                    <td colspan="2"></td>
                </tr>-->
                <tr>
                    <td colspan="2" style="text-align:center"><asp:CheckBox ID="chk_Recordarme" Text="Recordarme" runat="server" /></td>
                </tr>
            </table>
        </div>
    </form>
    <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
    <footer class="templatemo-footer">
        <div class="templatemo-copyright">
          <p>Copyright © 2018 Soluciones DS. V. 1.5</p>
        </div>
      </footer>
</body>
</html>
