﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Correo_Editar.aspx.cs" Inherits="Correo_Editar" EnableEventValidation="false" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="row">
        <div class="col-md-12">
            <form role="form" id="templatemo-preferences-form">
                <div class="row">
                    <div class="col-md-6 margin-bottom-15">
                        <label for="IdCorreo" class="control-label" id="lbl_IdCorreo" runat="server">Id Correo</label>
                        <asp:TextBox ID="txt_IdCorreo" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-6 margin-bottom-15">
                        <label for="SMTP" class="control-label">Servidor SMTP</label>
                        <asp:TextBox ID="txt_ServidorSMTP" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="val_SMTP" runat="server" ErrorMessage="Ingrese el servidor SMTP" Display="None" SetFocusOnError="true"
                            ControlToValidate="txt_ServidorSMTP"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 margin-bottom-15">
                        <label for="POP3" class="control-label">Servidor POP3</label>
                        <asp:TextBox ID="txt_POP3" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="val_POP3" ControlToValidate="txt_POP3" runat="server" ErrorMessage="Ingrese un servidor POP3" Display="None"
                            SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-6 margin-bottom-15">
                        <label for="CorreoNoficaciones" class="control-label">Correo Notificaciones</label>
                        <asp:TextBox ID="txt_CorreoNotificaciones" runat="server" MaxLength="50" CssClass="form-control" TextMode="Email"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="val_CorreoNotificaciones" runat="server" ControlToValidate="txt_CorreoNotificaciones" ErrorMessage="Ingrese el correo de notificaciones"
                            Display="None" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 margin-bottom-15">
                        <label for="CorreoNoficaciones" class="control-label">Usuario</label>
                        <asp:TextBox ID="txt_Usuario" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="val_Usuario" runat="server" ControlToValidate="txt_Usuario" ErrorMessage="Ingrese el usuerio del servidor"
                            Display="None" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-6 margin-bottom-15">
                        <label for="ClaveCorreo" class="control-label">Contrase&ntilde;a Correo</label>
                        <asp:TextBox ID="txt_Contrasena" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="val_Contrasena" ControlToValidate="txt_Contrasena" runat="server" ErrorMessage="Ingrese la contraseña del servidor"
                            Display="None" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 margin-bottom-15">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value=""/>
                                <asp:CheckBox ID="chk_Autenticación" Text="Autenticación" Checked="true" runat="server" />
                            </label>
                        </div>
                    </div>
                    <div class="col-md-3 margin-bottom-15">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value=""/>
                                <asp:CheckBox ID="chk_SSL" Text="SSL" Checked="true" runat="server" />
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6 margin-bottom-15">
                        <label for="Puerto" class="control-label">Puerto</label>
                        <asp:TextBox ID="txt_Puerto" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="val_Puerto" ControlToValidate="txt_Puerto" runat="server" ErrorMessage="Ingrese el Puerto del Servidor" 
                            Display="None" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 margin-bottom-15">
                        <label for="Observaciones" class="control-label">Observaciones</label>
                        <asp:TextBox ID="txt_Observaciones" runat="server" MaxLength="100" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 margin-bottom-15">
                        <asp:Button ID="btn_Guardar" Text="Guardar" runat="server" OnClick="btn_Guardar_Click" CssClass="btn btn-primary" />
                        &nbsp;
                        <asp:Button ID="btn_Cancelar" Text="Cancelar" PostBackUrl="~/Correo_Consulta.aspx" runat="server" CssClass="btn btn-danger" />
                    </div>
                </div>
            </form>
            <asp:ValidationSummary ID="val_Muestra" DisplayMode="BulletList" ShowMessageBox="true" runat="server" ShowSummary="false" />
        </div>
    </div>
</asp:Content>

