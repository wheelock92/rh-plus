﻿using BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Colaboradores_Nuevo : System.Web.UI.Page
{
    Colaboradores_L objColaboradores = new Colaboradores_L();
    PerfilesPuesto_L objPerfilesPuesto = new PerfilesPuesto_L();

    int IdOrganizacion = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        IdOrganizacion = Convert.ToInt32(Session["IdOrganizacion"]);
        CargaPuestos();

        if (!Page.IsPostBack)
        {
            if (Request.QueryString["Identificacion"] != null)
            {
                txt_Apellido1.Text = Request.QueryString["Apellido1"];
                txt_Apellido2.Text = Request.QueryString["Apellido2"];
                txt_Codigo.Text = Request.QueryString["Codigo"];
                txt_Identificacion.Text = Request.QueryString["Identificacion"];
                txt_Nombre.Text = Request.QueryString["Nombre"];
                ddl_Puesto.SelectedValue = Request.QueryString["Puesto"];
                //cal_FechaContratacion.SelectedDate = Convert.ToDateTime(Request.QueryString["FechaContratacion"].ToString());
                //cal_FechaNacimiento.SelectedDate = Convert.ToDateTime(Request.QueryString["FechaNacimiento"]);
                //cal_FechaVinculacion.SelectedDate = Convert.ToDateTime(Request.QueryString["FechaContratacion"]);
                string estado = Request.QueryString["Estado"];
                if (Request.QueryString["Estado"] == "True")
                {
                    chk_Estado.Checked = true;
                }
                else
                {
                    chk_Estado.Checked = false;
                }
            }
        }
    }

    private void Guardar()
    {
        try
        {
            objColaboradores.Apellido1 = txt_Apellido1.Text;
            objColaboradores.Apellido2 = txt_Apellido2.Text;
            objColaboradores.Codigo = Convert.ToInt32(txt_Codigo.Text);
            if (chk_Estado.Checked)
            {
                objColaboradores.Estado = true;
            }
            else
            {
                objColaboradores.Estado = false;
            }
            objColaboradores.FechaContratacion = cal_FechaContratacion.SelectedDate;
            objColaboradores.FechaNacimiento = cal_FechaNacimiento.SelectedDate;
            objColaboradores.FechaVinculacion = cal_FechaVinculacion.SelectedDate;
            objColaboradores.Identificacion = txt_Identificacion.Text;
            objColaboradores.IdOrganizacion = IdOrganizacion;
            objColaboradores.Nombre = txt_Nombre.Text;
            objColaboradores.Puesto = Convert.ToInt32(ddl_Puesto.SelectedValue);

            if (Request.QueryString["Identificacion"] == null)
            {
                if (objColaboradores.Colaboradores_Agregar())
                {
                    Response.Redirect("~/Colaboradores_Consulta.aspx", false);
                }
                else
                {
                    Session["Error"] = "Ocurrio un error al guardar la información. Favor contacte al administrador.";
                    Response.Redirect("~/Errores.aspx", false);
                }
            }
            else
            {
                if (objColaboradores.Colaboradores_Editar())
                {
                    Response.Redirect("~/Colaboradores_Consulta.aspx", false);
                }
                else
                {
                    Session["Error"] = "Ocurrio un error al guardar la información. Favor contacte al administrador.";
                    Response.Redirect("~/Errores.aspx", false);
                }
            }
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("~/Errores.aspx", false);
        }
    }

    private void CargaPuestos()
    {
        try
        {
            objPerfilesPuesto.IdOrganizacion = IdOrganizacion;
            ddl_Puesto.DataSource = objPerfilesPuesto.PerfilesPuesto_Organizacion();
            ddl_Puesto.DataBind();
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("~/Errores.aspx");
        }
    }

    protected void btn_Guardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }
}