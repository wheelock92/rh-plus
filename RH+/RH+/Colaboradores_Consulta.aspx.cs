﻿using BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Colaboradores_Consulta : System.Web.UI.Page
{
    Colaboradores_L objColaboradores = new Colaboradores_L();

    protected void Page_Load(object sender, EventArgs e)
    {
        Carga_Colaboradores();
    }

    private void Carga_Colaboradores()
    {
        try
        {
            objColaboradores.IdOrganizacion = Convert.ToInt32(Session["IdOrganizacion"]);
            gv_Colaboradores.DataSource = objColaboradores.Consulta_Colaboradores_Organizacion();
            gv_Colaboradores.DataBind();
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("~/Errores.aspx", false);
        }
    }

    protected void gv_Colaboradores_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            switch (e.CommandName)
            {
                case "Editar":
                    int fila = Convert.ToInt32(e.CommandArgument);
                    string Identificacion = gv_Colaboradores.DataKeys[fila].Value.ToString();
                    string Nombre = gv_Colaboradores.Rows[fila].Cells[1].Text.ToString();
                    string FechaNacimiento = gv_Colaboradores.Rows[fila].Cells[3].Text.ToString();
                    string Codigo = gv_Colaboradores.Rows[fila].Cells[4].Text.ToString();
                    string Puesto = gv_Colaboradores.Rows[fila].Cells[5].Text.ToString();
                    string FechaContratacion = gv_Colaboradores.Rows[fila].Cells[7].Text.ToString();
                    string FechaVinculacion = gv_Colaboradores.Rows[fila].Cells[8].Text.ToString();
                    string Estado = gv_Colaboradores.Rows[fila].Cells[9].Text.ToString();
                    string Apellido1 = gv_Colaboradores.Rows[fila].Cells[11].Text.ToString();
                    string Apellido2 = gv_Colaboradores.Rows[fila].Cells[12].Text.ToString();

                    Response.Redirect("~/Colaboradores_Nuevo.aspx?Identificacion=" + Identificacion + "&Nombre=" + Nombre + "&FechaNacimiento="
                        + FechaNacimiento + "&Puesto=" + Puesto + "&FechaContratacion=" + FechaContratacion + "FechaVinculaion=" + FechaVinculacion
                        + "&Estado=" + Estado + "&Apellido1=" + Apellido1 + "&Apellido2=" + Apellido2 + "&Codigo=" + Codigo, false);

                    break;

                case "Eliminar":
                    fila = Convert.ToInt32(e.CommandArgument);
                    string Id = gv_Colaboradores.DataKeys[fila].Value.ToString();
                    try
                    {
                        objColaboradores.Identificacion = Id;
                        if (objColaboradores.Colaboradores_Eliminar())
                        {
                            this.Carga_Colaboradores();
                        }
                        else
                        {
                            string errores = "No se ha podido eliminar el colaborador seleccionado. Favor contacte al administrador";
                            Response.Redirect("~/Errores.aspx?Error" + errores, false);
                        }
                    }
                    catch (Exception ex)
                    {
                        Session["Error"] = ex.Message;
                        Response.Redirect("~/Errores.aspx", false);
                    }

                    break;

                default:
                    string error = "La opcion seleccionada no esta funcional. Contacte al administrador";
                    Response.Redirect("~/Errores.aspx?Error" + error, false);
                    break;
            }
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("~/Errores.aspx", false);
        }
    }

    protected void gv_Colaboradores_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton db = (LinkButton)e.Row.Cells[14].Controls[0];
                string Nombre = e.Row.Cells[2].Text;
                db.OnClientClick = string.Format("return confirm ('Desea eliminar el colaborador: {0}?');", Nombre);
            }
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("~/Errores.aspx", false);
        }
    }
}