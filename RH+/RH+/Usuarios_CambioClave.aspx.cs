﻿using BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Usuarios_CambioClave : System.Web.UI.Page
{
    Usuarios_L objUsuarios = new Usuarios_L();

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// guarda la nueva clave ingresada por el usuario
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_Guardar_Click(object sender, EventArgs e)
    {
        try
        {
            if (txt_Clave.Text == txt_Clave2.Text)
            {
                objUsuarios.Clave = txt_Clave.Text;
                objUsuarios.Id = Convert.ToInt32(Session["Id"]);
                if (objUsuarios.Cambio_Clave())
                {
                    Response.Redirect("~/Index.aspx", false);
                }
                else
                {
                    Session["Error"] = "Error al cambiar la clave. Contacte al administrador";
                    Response.Redirect("~/Errores.aspx", false);
                }
            }
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("~/Errores.aspx", false);
        }
    }
}