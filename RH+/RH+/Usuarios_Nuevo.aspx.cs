﻿using BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;

public partial class Usuarios_Nuevo : System.Web.UI.Page
{
    Usuarios_L objUsuarios = new Usuarios_L();
    Encriptar_Clave Encriptar = new Encriptar_Clave();
    Secciones_L objSeccion = new Secciones_L();
    Login_L objLogin = new Login_L();

    protected void Page_Load(object sender, EventArgs e)
    {
        objLogin.IdRol = Convert.ToInt32(Session["IdRol"]);

        //Ejecuta una consulta a nivel lógico para verificar el rol del usuario para acceder al formulario.
        if (!objLogin.Admin() && !objLogin.Editor())
        {
            Session["Error"] = "El Usuario no posee los permisos para acceder.";
            Response.Redirect("~/Errores.aspx", false);
        }

        Carga_Secciones();

        //verifica si la página es postback para definir si es para editar o agregar. si es editar llena los diferentes campos con la información correcta
        if (!Page.IsPostBack)
        {
            if (Request.QueryString["Nombre"] != null)
            {
                txt_Apellido1.Text = Request.QueryString["Apellido1"];
                txt_Apellido2.Text = Request.QueryString["Apellido2"];
                txt_Correo.Text = Request.QueryString["Correo"];
                txt_IdUsuario.Text = Request.QueryString["IdUsuario"];
                txt_Nombre.Text = Request.QueryString["Nombre"];
                if (Request.QueryString["Estado"] == "True")
                {
                    chk_Activa.Checked = true;
                }
                else
                {
                    chk_Activa.Checked = false;
                }
                ddl_Rol.SelectedValue = Request.QueryString["IdRol"];
                ddl_Seccion.SelectedValue = Request.QueryString["IdSeccion"];
            }
        }
    }

    /// <summary>
    /// Guarda la información. Hace la verificación de si es para editar o agregar buscando en el parametro de IdUsuario si contiene datos o esta vacio.
    /// </summary>
    private void Guardar()
    {
        try
        {
            objUsuarios.Apellido1 = txt_Apellido1.Text;
            objUsuarios.Apellido2 = txt_Apellido2.Text;
            objUsuarios.Correo = txt_Correo.Text;
            if (chk_Activa.Checked)
            {
                objUsuarios.Estado = true;
            }
            else
            {
                objUsuarios.Estado = false;
            }
            objUsuarios.IdRol = Convert.ToInt32(ddl_Rol.SelectedValue);
            objUsuarios.IdSeccion = Convert.ToInt32(ddl_Seccion.SelectedValue);
            objUsuarios.IdUsuario = txt_IdUsuario.Text;
            objUsuarios.Nombre = txt_Nombre.Text;
            objUsuarios.IdOrganizacion = Convert.ToInt32(Session["IdOrganizacion"]);

            if (Request.QueryString["IdUsuario"] == null)
            {
                objUsuarios.Clave = "123456";

                if (objUsuarios.Nuevo())
                {
                    Response.Redirect("~/Usuarios_Consulta.aspx", false);
                }
            }
            else
            {
                objUsuarios.Id = Convert.ToInt32(Request.QueryString["Id"]);
                if (objUsuarios.Editar_Usuario())
                {
                    Response.Redirect("~/Usuarios_Consulta.aspx", false);
                }
            }
            
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("~/Errores.aspx", false);
        }
        
        
    }

    /*public string Cifrar_Clave(string claveP)
    {
        String modoValidacion;
        String claveCifrada;

        claveCifrada = "";
        modoValidacion = System.Configuration.ConfigurationManager.AppSettings.Get("metodoEncriptacion");
        claveCifrada = BusinessObjectsFramework.utiles.Authenticate.CifrarCadena(claveP, "SMARTFLOWFORDOCS", false);
    }*/
    protected void btn_Guardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// lleva el dropdwon list de las secciones de la organización a la que pertenece el usuario.
    /// </summary>
    private void Carga_Secciones()
    {
        try
        {
            objSeccion.IdOrganizacion = Convert.ToInt32(Session["IdOrganizacion"]);
            ddl_Seccion.DataSource = objSeccion.Carga_Secciones_Organizacion();
            ddl_Seccion.DataBind();
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.Message;
            Response.Redirect("~/Errores.aspx", false);
        }
    }
}