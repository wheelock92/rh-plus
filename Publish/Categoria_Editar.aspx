﻿<%@ page title="" language="C#" masterpagefile="~/MasterPage.master" autoeventwireup="true" inherits="Categoria_Editar, App_Web_0h4t3dmx" enableeventvalidation="false" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="row">
        <div class="col-md-12">
            <formview role="form" id="templatemo-preferences-form">
                <div class="row">
                    <div class="col-md-6 margin-bottom-15">
                        <label for="IdCategoria" class="control-label">Id Categor&iacute;a</label>
                        <asp:TextBox ID="txt_IdCategoria" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-6 margin-bottom-15">
                        <label for="Nombre" class="control-label">Nombre</label>
                        <asp:TextBox ID="txt_Nombre" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="val_Nombre" runat="server" ErrorMessage="Ingrese el nombre de la categoría." Display="None"
                            ControlToValidate="txt_Nombre" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 margin-bottom-15">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value=""/>
                                <asp:CheckBox ID="chk_Activo" Text="Activo" Checked="true" runat="server" />
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 margin-bottom-15">
                        <asp:Button ID="txt_Guardar" runat="server" Text="Guardar" OnClick="txt_Guardar_Click" CssClass="btn btn-primary" />
                        &nbsp;
                        <asp:Button ID="txt_Cancelar" Text="Cancelar" runat="server" CssClass="btn btn-danger" PostBackUrl="~/Categoria_Consulta.aspx" />
                    </div>
                </div>
            </formview>
            <asp:ValidationSummary ID="val_Muestra" runat="server" DisplayMode="BulletList" ShowMessageBox="true" ShowSummary="false" />
        </div>
    </div>
</asp:Content>

