﻿<%@ page title="" language="C#" masterpagefile="~/MasterPage.master" autoeventwireup="true" inherits="Correo_Consulta, App_Web_0h4t3dmx" enableeventvalidation="false" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h2>Correos</h2>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <asp:Button ID="btn_Nuevo" runat="server" PostBackUrl="~/Correo_Editar.aspx" class="btn btn-primary" Text="Nuevo" />
                &nbsp;
                <asp:Button ID="btn_Excel" runat="server" class="btn btn-success" Text="Excel" OnClick="ExportarExcel" />
                <br /><br />
                <asp:GridView ID="gv_Correo" CssClass="table table-striped table-hover table-bordered"  runat="server" AutoGenerateColumns="False" DataKeyNames="IdCorreo" AllowPaging="true" PageSize="10" AllowSorting="true" OnPageIndexChanging="gv_Correo_PageIndexChanging" OnRowDataBound="gv_Correo_RowDataBound" OnRowCommand="gv_Correo_RowCommand">
                    <Columns>
                        <asp:BoundField DataField="IdCorreo" HeaderText="IdCorreo" Visible="False" />
                        <asp:BoundField DataField="ServidorSMTP" HeaderText="Servidor SMTP" />
                        <asp:BoundField DataField="POP3" HeaderText="Servidor POP3" />
                        <asp:BoundField DataField="CorreoNotificaciones" HeaderText="Correo Notificaciones" />
                        <asp:BoundField DataField="ContrasenaCorreo" HeaderText="Contraseña Correo" />
                        <asp:CheckBoxField DataField="Autenticacion" HeaderText="Autenticación" />
                        <asp:BoundField DataField="Usuario" HeaderText="Usuario" />
                        <asp:CheckBoxField DataField="SSL" HeaderText="SSL" />
                        <asp:BoundField DataField="Puerto" HeaderText="Puerto" />
                        <asp:BoundField DataField="Observaciones" HeaderText="Observaciones" />
                        <asp:BoundField DataField="IdOrganizacion" HeaderText="IdOrganizacion" Visible="False" />
                        <asp:HyperLinkField DataNavigateUrlFields="IdCorreo,ServidorSMTP,POP3,CorreoNotificaciones,ContrasenaCorreo,Autenticacion,Usuario,SSL,Puerto,Observaciones,IdOrganizacion" 
                            DataNavigateUrlFormatString="Correo_Editar.aspx?IdCorreo={0}&amp;ServidorSMTP={1}&amp;POP3={2}&amp;CorreoNotificaciones={3}&amp;ContrasenaCorreo={4}&amp;Autenticacion={5}&amp;Usuario={6}&amp;SSL={7}&amp;Puerto={8}&amp;Observaciones={9}&amp;IdOrganizacion={10}" Text="Editar" />
                        <asp:ButtonField CommandName="Eliminar" Text="Eliminar" />
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/templatemo_script.js"></script>   
    <br />
</asp:Content>

