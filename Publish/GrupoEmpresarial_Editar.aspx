﻿<%@ page title="" language="C#" masterpagefile="~/MasterPageSU.master" autoeventwireup="true" inherits="GrupoEmpresarial_Editar, App_Web_0h4t3dmx" enableeventvalidation="false" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="row">
        <div class="col-md-12">
            <form role="form" id="templatemo-preferences-form">
                <div class="row">
                    <div class="col-mg-6 margin-bottom-15">
                        <label for="Id" class="control-label">Id Grupo</label>
                        <asp:TextBox ID="txt_IdGrupo" Enabled="false" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-md-6 margin-bottom-15">
                        <label for="firstName" class="control-label">Nombre</label>
                        <asp:TextBox ID="txt_Nombre"  class="form-control"  runat="server" MaxLength="50" CausesValidation="true"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="valida_Nombre" runat="server" ErrorMessage="Ingrese un nombre para el Grupo" ControlToValidate="txt_Nombre" Display="None"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-6 margin-bottom-15">
                        <label for="lastName" class="control-label">Descripción</label>
                        <asp:TextBox ID="txt_Descripcion" class="form-control" runat="server" MaxLength="100" CausesValidation="true"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="valida_Descripcion" runat="server"  ErrorMessage="Ingrese una descripción del Grupo" 
                            ControlToValidate="txt_Descripcion" Display="None"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-6 margin-bottom-15">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="" />
                                <asp:CheckBox ID="chk_Activo" Text="Activo" runat="server" Checked="true" />
                            </label>
                        </div>
                    </div>
                    <asp:Button ID="btn_Guardar" Text="Guardar" runat="server" class="btn btn-primary" OnClick="btn_Guardar_Click"  />
                </div>
            </form>
            <asp:ValidationSummary ID="ValidationSummary1" DisplayMode="BulletList" runat="server" ShowMessageBox="true" ShowSummary="false" ForeColor="Red" />
        </div>
    </div>
</asp:Content>

