﻿<%@ page title="" language="C#" masterpagefile="~/MasterPage.master" autoeventwireup="true" inherits="PerfilesPuesto_Nuevo, App_Web_0h4t3dmx" enableeventvalidation="false" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6 margin-bottom-15">
                    <label for="firstName" class="control-label">Código</label>
                    <asp:TextBox  class="form-control" ID="txt_Codigo" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="val_Codigo" ControlToValidate="txt_Codigo" runat="server" Display="None" ErrorMessage="Ingrese el código del perfil"></asp:RequiredFieldValidator>
                </div>
                <div class="col-md-6 margin-bottom-15">
                    <label for="lastName" class="control-label">Versión</label>
                        <asp:TextBox  class="form-control" ID="txt_Version" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="val_Version" ControlToValidate="txt_Version" runat="server" Display="None" ErrorMessage="Ingrese la versión del perfil"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 margin-bottom-15">
                    <label for="firstName" class="control-label">Título</label>
                    <asp:TextBox class="form-control" ID="txt_Titulo" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="val_Titulo"  ControlToValidate="txt_Titulo" runat="server" Display="None" ErrorMessage="Ingrese el título del perfil"></asp:RequiredFieldValidator>
                </div>
                <div class="col-md-6 margin-bottom-15">
                    <label for="lastName" class="control-label">Descripción</label>
                    <asp:TextBox class="form-control" ID="txt_Descripcion" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="val_Descripcion" ControlToValidate="txt_Descripcion" runat="server" Display="None"
                          ErrorMessage="Ingrese la descripción del perfil"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 margin-bottom-15">
                    <label for="firstName" class="control-label">Administrador</label>
                    <asp:DropDownList ID="ddl_Administrador"  class="form-control margin-bottom-15" runat="server" DataTextField="Nombre" DataValueField="Id"></asp:DropDownList>
                </div>
                <div class="col-md-6 margin-bottom-15">
                    <label for="firstName" class="control-label">Sección</label>
                    <asp:DropDownList ID="ddl_Seccion" class="form-control margin-bottom-15" runat="server" DataTextField="Nombre" DataValueField="IdSeccion"></asp:DropDownList>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 margin-bottom-15">
                    <label for="Categoria" class="control-label">Categor&iacute;a</label>
                    <asp:DropDownList ID="ddl_Categoria" runat="server" CssClass="form-control margin-bottom-15" DataTextField="Nombre" DataValueField="IdCategoria"></asp:DropDownList>
                </div>
                <div class="col-md-6 margin-bottom-15">
                    <label for="lastName" class="control-label">Subcategoría</label>
                    <asp:DropDownList ID="ddl_SubCategoria" runat="server"  class="form-control margin-bottom-15" DataTextField="Nombre" DataValueField="IdSubcategoria"></asp:DropDownList>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 margin-bottom-15">
                    <label for="firstName" class="control-label">Fecha de vigencia</label>
                    <asp:Calendar ID="cal_Vigencia" runat="server" CssClass="fa-calendar"></asp:Calendar>
                    <%--<asp:CompareValidator ID="val_Vigencia" ControlToValidate="cal_Vigencia" ErrorMessage="Seleccione una fecha posterior a la de hoy."
                        Operator="LessThanEqual" Type="Date" runat="server" ></asp:CompareValidator>--%>
                    <asp:CustomValidator ID="val_Vigencia" runat="server" ErrorMessage="Ingrese una fecha posterior a la de hoy." Display="Dynamic" OnServerValidate="val_Vigencia_ServerValidate"></asp:CustomValidator>
                </div>
                <div class="col-md-6 margin-bottom-15">
                    <label for="lastName" class="control-label">Archivo</label>
                    <asp:FileUpload ID="fu_Archivo" runat="server" />
                </div>
            </div>
        </div>
    </div>
    <asp:Button ID="btn_Guardar" class="btn btn-primary" Text="Guardar" runat="server" OnClick="btn_Guardar_Click" />
    <asp:ValidationSummary ID="val_Muestra" ShowMessageBox="true" DisplayMode="BulletList" ShowSummary="false" runat="server" />
</asp:Content>

