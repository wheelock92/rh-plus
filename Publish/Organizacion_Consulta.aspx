﻿<%@ page language="C#" autoeventwireup="true" masterpagefile="~/MasterPageSU.master" inherits="Organizacion_Consulta, App_Web_0h4t3dmx" enableeventvalidation="false" %>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        .GridHeader th{text-align:center}
        .Hide {display:none}
    </style>
    <h2>Organizaciones</h2>
    <br />
    <asp:Button ID="btn_Nuevo" runat="server" PostBackUrl="~/Organizacion_Nuevo.aspx" Text="Nuevo" class="btn btn-primary" />
    &nbsp;
    <asp:Button ID="btn_Excel" runat="server" class="btn btn-success" Text="Excel" OnClick="ExportarExcel" />
    <br /><br />
    <asp:GridView ID="gv_Organizacion" CssClass="table table-striped table-hover table-bordered GridHeader" runat="server" AutoGenerateColumns="False" DataKeyNames="IdOrganizacion" OnRowCommand="gv_Organizacion_RowCommand" OnRowDataBound="gv_Organizacion_RowDataBound" AllowPaging="True" AllowSorting="True" OnPageIndexChanging="gv_Organizacion_PageIndexChanging">
        <Columns>
            <asp:BoundField DataField="IdOrganizacion" HeaderText="IdOrganizacion" Visible="False" >
                <HeaderStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="NombreEmpresa" HeaderText="Nombre" >
                <HeaderStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="IdGrupo" HeaderText="IdGrupo" NullDisplayText="N/A" Visible="False" >
                <HeaderStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="GrupoEmpresarial" HeaderText="Grupo Empresarial" NullDisplayText="N/A" >
                <HeaderStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="CantidadUsuario" HeaderText="Usuarios" >
                <HeaderStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="Observaciones" HeaderText="Observaciones" >
                <HeaderStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="Id" HeaderText="IdUsuario" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" >
                <HeaderStyle CssClass="Hide"></HeaderStyle>
                <ItemStyle CssClass="Hide"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="Usuario" HeaderText="Administrador" >
                <HeaderStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="IdUsuario" HeaderText="Username">
                <HeaderStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:CheckBoxField DataField="Activa" Text="Activa" >
                <HeaderStyle HorizontalAlign="Center" />
            </asp:CheckBoxField>
            <asp:BoundField DataField="Name" HeaderText="Nombre" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" >
<HeaderStyle CssClass="Hide"></HeaderStyle>

<ItemStyle CssClass="Hide"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="Apellido1" HeaderText="Apellido1" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" >
<HeaderStyle CssClass="Hide"></HeaderStyle>

<ItemStyle CssClass="Hide"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="Apellido2" HeaderText="Apellido2" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" >
<HeaderStyle CssClass="Hide"></HeaderStyle>

<ItemStyle CssClass="Hide"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="Correo" HeaderText="Correo"/>
            <asp:HyperLinkField DataNavigateUrlFields="IdOrganizacion,NombreEmpresa,IdGrupo,Activa,CantidadUsuario,Observaciones,Id,Name,Apellido1,Apellido2,Correo,IdUsuario" DataNavigateUrlFormatString="Organizacion_Nuevo.aspx?IdOrganizacion={0}&amp;NombreEmpresa={1}&amp;IdGrupo={2}&amp;Activa={3}&amp;CantidadUsuario={4}&amp;Observaciones={5}&amp;Id={6}&amp;Name={7}&amp;Apellido1={8}&amp;Apellido2={9}&amp;Correo={10}&amp;IdUsuario={11}" Text="Editar" >
                <HeaderStyle HorizontalAlign="Center" />
            </asp:HyperLinkField>
            <asp:ButtonField CommandName="Eliminar" Text="Eliminar" >
                <HeaderStyle HorizontalAlign="Center" />
            </asp:ButtonField>
            <asp:ButtonField CommandName="Reset" Text="Resetear Clave" >
                <HeaderStyle HorizontalAlign="Center" />
            </asp:ButtonField>
        </Columns>
        <HeaderStyle HorizontalAlign="Center" />
    </asp:GridView>
</asp:Content>