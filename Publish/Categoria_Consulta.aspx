﻿<%@ page title="" language="C#" masterpagefile="~/MasterPage.master" autoeventwireup="true" inherits="Categoria_Consulta, App_Web_0h4t3dmx" enableeventvalidation="false" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h2>Categorías</h2>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <asp:Button ID="btn_Nuevo" runat="server" class="btn btn-primary" PostBackUrl="~/Categoria_Editar.aspx" Text="Nuevo" />
                &nbsp;
                <asp:Button ID="btn_Excel" runat="server" class="btn btn-success" Text="Excel" OnClick="ExportarExcel" />
                <br /><br />
                <asp:GridView ID="gv_Categoria" runat="server"  CssClass="table table-striped table-hover table-bordered" AutoGenerateColumns="False" DataKeyNames="IdCategoria" AllowPaging="True" AllowSorting="True" OnPageIndexChanging="gv_Categoria_PageIndexChanging" OnRowCommand="gv_Categoria_RowCommand1" OnRowDataBound="gv_Categoria_RowDataBound1">
                    <Columns>
                        <asp:BoundField DataField="IdCategoria" HeaderText="IdCategoria" Visible="False" />
                        <asp:BoundField DataField="Nombre" HeaderText="Nombre" />
                        <asp:BoundField DataField="IdOrganizacion" HeaderText="IdOrganizacion" Visible="False" />
                        <asp:CheckBoxField DataField="Activo" HeaderText="Activo" />
                        <asp:HyperLinkField DataNavigateUrlFields="IdCategoria,Nombre,Activo" DataNavigateUrlFormatString="Categoria_Editar.aspx?IdCategoria={0}&amp;Nombre={1}&amp;Activo={2}" Text="Editar" />
                        <asp:ButtonField CommandName="Eliminar"  Text="Eliminar" />
                    </Columns>
                </asp:GridView>
                <br />
            </div>
        </div>
    </div>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/templatemo_script.js"></script>   
</asp:Content>

