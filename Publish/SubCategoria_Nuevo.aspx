﻿<%@ page title="" language="C#" masterpagefile="~/MasterPage.master" autoeventwireup="true" inherits="SubCategoria_Nuevo, App_Web_0h4t3dmx" enableeventvalidation="false" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="row">
        <div class="col-md-12">
            <formview role="form" id="templatemo-preferences-form">
                <div class="row">
                    <div class="col-md-6 margin-bottom-15">
                        <label for="IdSubcategoria" class="control-label">Id Subcategor&iacute;a</label>
                        <asp:TextBox ID="txt_IdSubCategoria" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-6 margin-bottom-15">
                        <label for="Nombre" class="control-label">Nombre</label>
                        <asp:TextBox ID="txt_Nombre" runat="server" CssClass="form-control"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="val_Nombre" ControlToValidate="txt_Nombre" ErrorMessage="Ingrese el nombre de la subcategoria"
                            runat="server" Display="None"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 margin-bottom-15">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value=""/>
                                <asp:CheckBox ID="chk_Activo" Text="Activo" Checked="true" runat="server" />
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 margin-bottom-15">
                        <asp:Button ID="btn_Guardar" Text="Guardar" runat="server" OnClick="btn_Guardar_Click" CssClass="btn btn-primary" />
                        &nbsp;
                        <asp:Button ID="Cancelar" Text="Cancelar" runat="server" PostBackUrl="~/SubCategoria_Consulta.aspx" CssClass="btn btn-danger" />
                    </div>
                </div>
            </formview>
        </div>
    </div>
</asp:Content>

