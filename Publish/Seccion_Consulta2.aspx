﻿<%@ page language="C#" masterpagefile="~/MasterPage.master" autoeventwireup="true" inherits="Seccion_Consulta2, App_Web_0h4t3dmx" %>

<asp:Content ID="content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>Secciones</h2>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <br />
                <asp:Button ID="btn_Nuevo" Text="Nuevo" class="btn btn-primary" PostBackUrl="~/Seccion_Nuevo.aspx" runat="server" />
                &nbsp;
                <asp:Button ID="btn_Excel" Text="Excel" CssClass="btn btn-success" runat="server" OnClick="ExportarExcel" />
                <br /><br />
                <asp:GridView ID="gv_Seccion" runat="server" CssClass="table table-striped table-hover table-bordered" AutoGenerateColumns="false" AllowPaging="true" AllowSorting="true" DataKeyNames="IdSeccion" OnRowDataBound="gv_Seccion_RowDataBound" OnRowCommand="gv_Seccion_RowCommand" OnSorting="gv_Seccion_Sorting" OnPageIndexChanging="gv_Seccion_PageIndexChanging">
                    <Columns>
                        <asp:BoundField DataField="IdSeccion" HeaderText="IdSeccion" Visible="False" />
                        <asp:BoundField DataField="Nombre" HeaderText="Nombre" />
                        <asp:BoundField DataField="IdOrganizacion" HeaderText="IdOrganizacion" Visible="False" />
                        <asp:CheckBoxField DataField="Activo" HeaderText="Activo" ReadOnly="True" />
                        <asp:HyperLinkField DataNavigateUrlFields="IdSeccion,Nombre,IdOrganizacion,Activo" DataNavigateUrlFormatString="Seccion_Nuevo.aspx?IdSeccion={0}&amp;Nombre={1}&amp;IdOrganizacion={2}&amp;Activo={3}" Text="Editar" />
                        <asp:ButtonField CommandName="Eliminar" Text="Eliminar" />
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>