﻿<%@ page title="" language="C#" masterpagefile="~/MasterPage.master" autoeventwireup="true" inherits="Usuarios_CambioClave, App_Web_0h4t3dmx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form role="form" id="templatemo-preferences">
        <div class="row">
            <div class="col-md-6 margin-bottom-15">
                <label for="Clave" class="control-label">Nueva Contrase&ntilde;a</label>
                <asp:TextBox ID="txt_Clave" CssClass="form-control" runat="server" TextMode="Password"></asp:TextBox>
            </div>
            <div class="col-md-6 margin-bottom-15">
                <label for="Clave" class="control-label">Repita Contrase&ntilde;a</label>
                <asp:TextBox ID="txt_Clave2" CssClass="form-control" runat="server" TextMode="Password"></asp:TextBox>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 margin-bottom-15">
                <asp:Button ID="btn_Guardar" Text="Guardar" CssClass="btn btn-success" runat="server" OnClick="btn_Guardar_Click" />
            </div>
        </div>
    </form>
</asp:Content>

